# docsrc=$(shell find doc -iname "*.md")
# dochtml=$(docsrc:%.md=%.html)
# data = m/data/join.json m/data/ttrss.json m/data/events.json m/data/wikimap.json
# agenda = m/p/agenda/index.html m/p/agenda/index.html m/p/agenda/index.html

#skins=$(shell ls m/skins)
#skin_index=$(skins:%=m/skins/%/index.html)
#skin_map=$(skins:%=m/skins/%/map.html)


all: accueil m/index.html m/wiki/index/index.html m/wiki/index.json rewrite_redirects backlinks m/data/main.json

#skins: $(skin_index) $(skin_map)

.PHONY: zapzeros

zapzeros:
	find m/data -size 0 -exec rm {} \;

clean:
	rm m/data/*.json
	rm -rf m/wiki/*
	# rm -rf m/feeds/*

feeds:
	python3 m/scripts/mediawikifeeder.py --user Ergbot --password mettezlesmotsquimanquent feed

site: zapzeros $(data) $(agenda) m/wiki m/menu.html

doc: $(dochtml)

# framing page (js)
m/frame.js: m/src/frame.js m/src/*.js
	cd m && node_modules/.bin/webpack --config src/frame.config.js

# Menu/Index
m/wiki/index/index.html: web.touch m/templates/index.html
	mkdir -p m/wiki/index
	python3 m/scripts/mediawikidump.py template \
		--local-images m/images \
		m/templates/index.html \
		m/wiki/index/index.html

# Index (js)
m/index.js: m/src/index.js
	cd m && node_modules/.bin/webpack --config src/index.config.js

m/worker.js: m/src/worker.js
	cp $< $@

# Dynamic site map (js)
m/map.js: m/src/map.js m/src/*.js m/worker.js
	cd m && node_modules/.bin/webpack --config src/map.config.js

m/anothermap.js: m/src/anothermap.js m/src/pageindex.js
	cd m && node_modules/.bin/webpack --config src/anothermap.config.js
m/anothermap.html: m/src/anothermap.html m/src/map.svg
	python m/scripts/build_html_with_svg.py m/src/anothermap.html m/src/map.svg > $@


m/streetmap.js: m/src/streetmap.js
	cd m && node_modules/.bin/webpack --config src/streetmap.config.js
m/streetmap.html: m/src/streetmap.html m/src/blank.svg
	python m/scripts/build_html_with_svg.py m/src/streetmap.html m/src/blank.svg > $@


# m/%.js: m/src/%.js
# 	cd m && node_modules/.bin/webpack --entry src/$*.js --output $*.js

m/rdfareader.js: m/src/rdfareader.js
	cd m && node_modules/.bin/webpack --config src/rdfareader.config.js

# Dynamic site map (html based on src/map.html which includes src/map.svg)
m/map.html: m/src/map.html m/src/map.svg
	python m/scripts/build_html_with_svg.py m/src/map.html m/src/map.svg > $@

m/index.html: m/src/index.html
	python3 m/scripts/mediawikidump.py template m/src/index.html $@

#m/skins/%/index.html: m/src/index.html
	# python m/scripts/build_html_with_svg.py m/src/map.html m/skins/$*/map.svg > $@

#m/skins/%/map.html: m/src/map.html m/skins/%/map.svg
#	python m/scripts/build_html_with_svg.py m/src/map.html m/skins/$*/map.svg > $@

#m/skins/%/index.html: m/src/index.html
#		python m/scripts/make_index.py --skin $* > $@

################################
# DATA
################################

# m/data/wikidump.json: web.touch
m/wiki/index.json: web.touch m/templates/wikipage.html
	# python3 m/scripts/mediawikidump.py --namespace "Page web" --allpages --subcats Category:Menu --path m/wiki --output $@
	python3 m/scripts/mediawikidump.py dump \
	    --namespace "Page web" \
	    --namespace "Actualités" \
	    --allpages \
	    --allcats --emptycats \
	    --path m/wiki \
		--template m/templates/wikipage.html \
		--local-images m/images

wiki_absurls: web.touch m/templates/wikipage.html
	# python3 m/scripts/mediawikidump.py --namespace "Page web" --allpages --subcats Category:Menu --path m/wiki --output $@
	python3 m/scripts/mediawikidump.py dump \
	    --namespace "Page web" \
	    --namespace "Actualités" \
	    --allpages \
	    --allcats --emptycats \
	    --path m/wiki \
		--template m/templates/wikipage.html


.PHONY: accueil
accueil: web.touch
	python3 m/scripts/mediawikidump.py dump \
		--page "Actualités" \
		--force \
	    --path m/wiki \
		--template m/templates/wikipage.html \
		--local-images m/images
	python3 m/scripts/mediawikidump.py dump \
		--page "`python3 m/scripts/mediawiki_get_startpage.py`" \
		--force \
	    --path m/wiki \
		--template m/templates/wikipage.html \
		--local-images m/images

.PHONY: forcecats
forcecats:
	python3 m/scripts/mediawikidump.py dump \
		--force \
	    --allcats --emptycats \
	    --path m/wiki \
		--template m/templates/wikipage.html \
		--local-images m/images


.PHONY: forcedump
forcedump:
	# python3 m/scripts/mediawikidump.py --namespace "Page web" --allpages --subcats Category:Menu --path m/wiki --output $@
	python3 m/scripts/mediawikidump.py dump \
		--force \
	    --namespace "Page web" \
	    --namespace "Actualités" \
	    --allpages \
	    --allcats --emptycats \
	    --path m/wiki \
		--template m/templates/wikipage.html \
		--local-images m/images

.PHONY: menu
menu: web.touch
	# python m/scripts/menu.py --data m/data/join.json > m/menu.html
	python3 m/scripts/mediawikidump.py dump \
		--page "Template:Menu" \
	    --path m/wiki \
		--template m/templates/menu.html

.PHONY: cats

cats:
	python3 m/scripts/mediawikidump.py dump \
		--force \
		--allcats --emptycats \
		--path m/wiki \
		--template m/templates/wikipage.html

m/data/dates.json: web.touch
	python3 m/scripts/mediawiki_dump_dates.py > $@

m/data/merge.json: m/wiki/index.json m/wiki/Catégorie/index.json m/wiki/Page_web/index.json m/wiki/Actualités/index.json m/scripts/erg.context.json m/data/dates.json
	python3 m/scripts/merge_ld.py m/wiki/index.json m/wiki/Catégorie/index.json m/wiki/Page_web/index.json m/wiki/Actualités/index.json m/data/dates.json --context m/scripts/erg.context.json > $@

.PHONY: backlinks
backlinks: m/data/merge05.json m/templates/backlinks.html
	python3 m/scripts/add_backlinks.py \
		--data m/data/merge05.json \
		m/wiki/*.html \
		m/wiki/Page_web/*.html \
		m/wiki/Actualités/*.html \
		m/wiki/Catégorie/*.html

.PHONY: rewrite_redirects
rewrite_redirects: m/data/merge02.json
	python3 m/scripts/rewrite_redirected_links.py \
		--data m/data/merge02.json \
		m/wiki/*.html \
		m/wiki/Page_web/*.html \
		m/wiki/Actualités/*.html \
		m/wiki/Catégorie/*.html


m/data/merge02.json: m/data/merge.json
	python3 m/scripts/pp_absurls.py $< /m/wiki/ > $@

m/data/merge03.json: m/data/merge02.json
	python3 m/scripts/pp_resolve_redirects.py $< > $@

m/data/merge04.json: m/data/merge03.json m/src/map.svg
	python3 m/scripts/pp_add_symbols.py m/src/map.svg < $< > $@

m/data/merge05.json: m/data/merge04.json
	python3 m/scripts/pp_add_backlinks.py < $< > $@

m/data/main.json: m/data/merge05.json
	python3 m/scripts/pp_export_to_load_indexes.py < $< 

