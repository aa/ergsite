(function () {
var current_doc_href = null;

// alert("map");
// document.getElementById("docclosex").addEventListener("click", function () {
//     document.getElementById("document").classList.toggle("visible");
// });
function classcycler (button, target, modes) {
    var ret = {};
    var i = 0;
    function set_index () {
        var l; // nb changes i in classcycler scope
        for (i=0, l=modes.length; i<l; i++) {
            if (target.classList.contains(modes[i])) { return; }
        }
    }
    button.addEventListener("click", function () {
        set_index();
        console.log("i", i);
        target.classList.replace(modes[i], modes[i = (i+1) % modes.length]);
    });
    ret.set_mode = function (m) {
        set_index();
        target.classList.replace(modes[i], m);
    }
    return ret;
}

function relativize_local_urls (href) {
    // convert hrefs matching window.host
    // used in mouseover code to normalize local URLs (localhtml pages)
    var localbase = window.location.protocol + "//" + window.location.host;
    if (href.indexOf(localbase) == 0) {
        href = href.substring(localbase.length);
    }
    return href;
}

function strip_hostname_from_href (href) {
    return href.replace(/^https?:\/\/.+?\//, "/")
}

function enclosing_link (n) {
    // console.log("enclosing_link n", n, n.nodeName);
    if (n.nodeName && n.nodeName.toUpperCase() == "A") {
        return n;
    } else if (n.parentNode) {
        return enclosing_link(n.parentNode);
    }
}

function enclosing_link_or_title (n) {
    // change to find TITLE H1 as well
    if (n.nodeName && ((n.nodeName.toUpperCase() == "A") || (n.nodeName.toUpperCase() == "H1" && n.classList.contains("title")))) {
        return n
    } else if (n.parentNode) {
        return enclosing_link_or_title(n.parentNode);
    }
}

function strip_hash (href) {
    return href.replace(/#(.+)$/, '');
}

// TODO: filter data items directly (forget d3 selection)
function href_for_link_or_title (elt, basehref) {
    var href;
    if (elt.nodeName.toUpperCase() == "A") {
        return strip_hostname_from_href(elt.href);
    } else if (elt.nodeName.toUpperCase() == "H1") {
        return strip_hash(basehref);
    }
    //else if (elt.nodeName.toUpperCase() == "SECTION") {
    //    return strip_hash(basehref) + "#" + encodeURIComponent(elt.getAttribute("id"));
    //}
}


function mouseover_href (href, src) {
    if (src == "doc") {
        document.getElementById("map").contentWindow.postMessage({msg: "mouseover", href: href, src: src}, "*");
    } else {
        // highlight link in document view
        // document.getElementById("map").contentWindow.postMessage({msg: "mouseover", href: href, src: src}, "*");        
    }
}

function show_external (href) {
    // console.log("show_external **", href);

    document.body.setAttribute('class', 'map');
    window.setTimeout(function () {
        document.getElementById("map").contentWindow.postMessage({msg: "click", href: href}, "*");
    }, 1000)
    // $.fancybox.open({src: href, type: 'iframe'});
}

window.addEventListener("message", function (e) {
    // console.log("top frame.message", e.data);
    if (e.data.msg == "fancyboxopen") {
        // content_cc.set_mode("map");
    } else if (e.data.msg == "fancyboxclose") {
        // content_cc.set_mode("split");
    }
});

function set_current_doc_href (href, force) {
    if (current_doc_href != href || force) {
        current_doc_href = href;
        // console.log("sending message");
        document.getElementById("map")
            .contentWindow
            .postMessage({msg: "docloaded2", href: href}, "*");        
    }
}

function extract_link_gallery (doc) {
    var use_link;
    var glinks = [];
    var glinks_seen = {}; // nodups
    var pagelinks = doc.querySelectorAll("a");
    for (var i=0, l=pagelinks.length; i<l; i++) {
        var pl = pagelinks[i];
        use_link = false;
        if (pl.href) {
            if (glinks_seen[pl.href]) {
                continue;
            }
            glinks_seen[pl.href] = true;
            if (pl.href.indexOf("http") !== 0) {
                continue;
            }

            var glink = {src: pl.href, type: "iframe"};
            if (pl.classList.contains("image")) {
                glink['type'] = "image";
                use_link = true;
            }
            if (pl.href.match(/\.pdf$/)) {
                delete glink['type'];
            }
            if (pl.href.match(/^https?:\/\/vimeo\.com/)) {
                delete glink['type']; // prefer fancybox's auto-detection magic
            }
            if (pl.href.match(/youtube\.com/)) {
                delete glink['type']; // prefer fancybox's auto-detection magic
            }
            if (pl.classList.contains("external") && !pl.classList.contains("wiki")) {
                pl.setAttribute("target", "external");
            //    use_link = true;
            }
            if (pl.hasAttribute("data-caption")) {
                 glink['caption'] = pl.getAttribute("data-caption");
            } else {
                glink['caption'] = "<a target='newtab' href='" + pl.href + "'>" + pl.href + "</a>"
            }
            // console.log("looking for thumbimage", pl);
            if (pl.querySelector("img.thumbimage")) {
                // console.log("found thumbimage")
                use_link=true;
            }


            if (use_link) {
                glinks.push(glink);
            }
        }
    }
    return glinks;
    // console.log("frame:doc_iframe_loaded, glinks", glinks);
}

function fragmentize (href) {
    var m = href.match(/^\/m\/wiki\/(.*?)\.html$/);
    if (m) {
        return m[1];
    }
}

function rev_fragmentize (f) {
    return "/m/wiki/" + f.substring(1) + ".html";
}

window.addEventListener("hashchange", function () {
    // console.log("hashchange", window.location.hash);
    var href = rev_fragmentize(window.location.hash);
    if (current_doc_href != href) {
        // console.log("HASHCHANGE.SETTING HREF", href);
        doc_iframe.src = href;
    }

});

function doc_iframe_loaded () {
    var iframe = this;
    // console.log("map.js: doc_iframe_loaded")
    if (this.contentDocument) {
        // console.log("doc_iframe LOADED", doc_iframe.contentWindow.location);
        var iframe_href = this.contentWindow.location.pathname,
            f = fragmentize(iframe_href);
        if (f) {
            // console.log("frame.js, doc_iframe_loaded", iframe_href, f);
            window.location.hash = f;
            set_current_doc_href(iframe_href);            
        }

        // var glinks = extract_link_gallery(this.contentDocument);
        // document.getElementById("map").contentWindow.postMessage({msg: "linksgallery", links: glinks}, "*");

        this.contentDocument.addEventListener("click", function (e) {
            var a = enclosing_link(e.target),
                target;
            if (a) {
                var glinks = extract_link_gallery(iframe.contentDocument);
                // is a part of this link gallery ?
                var href = a.href || a.getAttribute("xlink:href");
                for (var i=0, l=glinks.length; i<l; i++) {
                    if (glinks[i].src == href) {
                        e.preventDefault();
                        e.stopPropagation();
                        document.getElementById("map").contentWindow.postMessage({msg: "linksgallery", links: glinks, href: href}, "*");
                        return false;
                    }
                }

                // target = a.getAttribute("target");
                // // console.log("iframe.click", e.target, a);
                // if ((a.classList.contains("external") && !target) ||
                //     (target == "external") ||
                //     (a.getAttribute("data-fancybox"))) {
                //     e.preventDefault();
                //     e.stopPropagation();
                //     var href = a.href || a.getAttribute("xlink:href");
                //     // console.log("handling external link", href);
                //     var glinks = extract_link_gallery(iframe.contentDocument);
                //     // show_external(href);
                //     document.getElementById("map").contentWindow.postMessage({msg: "linksgallery", links: glinks, href: href}, "*");

                //     return false;
                // }
            }
        });


        this.contentDocument.addEventListener("mouseover", function (e) {
            var elt = enclosing_link_or_title(e.target),
                href;
            if (elt) {
                href = relativize_local_urls (href_for_link_or_title(elt, iframe_href));
                // console.log("map.js: doc mouseover", href);
                mouseover_href(href, "doc");
            }
        });

        return;
    }

}

function map_iframe_loaded () {
    // console.log("map_iframe_loaded", this);
    var iframe = this;
    if (this.contentDocument) {

        if (current_doc_href) {
            // reset with force to send a docloaded message to the map
            set_current_doc_href(current_doc_href, true);
        }

        var iframe_href = this.contentWindow.location.hostname;
        this.contentDocument.addEventListener("click", function (e) {
            var a = enclosing_link(e.target),
                target;
            if (a) {            
                target = a.getAttribute("target");
                // console.log("iframe.click", e.target, a);
                if ((a.classList.contains("external") && !target) || (target == "external")) {
                    e.preventDefault();
                    var href = a.getAttribute("href") || a.getAttribute("xlink:href");
                    console.log("handling external link", href);
                    show_external(href);
                }
            }
        });

        this.contentDocument.addEventListener("mouseover", function (e) {
            // var elt = enclosing_link_or_section(e.target);
            // // console.log("doc mouseover", elt);
            // if (elt) {
            //     var href = href_for_link_or_section(elt, iframe_href);
            //     if (href) {
            //         console.log("map mouseover", href);
            //         mouseover_href(href, 'map');
            //     }
            // }
        });

    }        
}

var map_iframe = document.getElementById("map");
var doc_iframe = document.getElementById("documentframe");

map_iframe.addEventListener("load", map_iframe_loaded.bind(map_iframe));
map_iframe_loaded.call(map_iframe);

doc_iframe.addEventListener("load", doc_iframe_loaded.bind(doc_iframe));
doc_iframe_loaded.call(doc_iframe);

if (window.location.hash) {
    // console.log("frame.js init", window.location);
    var href = rev_fragmentize(window.location.hash);
    if (href) {
        // console.log("frame.js: SETTING INITIAL doc_iframe.href", href);
        doc_iframe.src = href;
    }
}

/* SPLIT SCREEN SLIDER CODE */
var dragcover;
document.addEventListener("DOMContentLoaded", function () {
    // console.log('DCL, adding dragcovers');
    dragcover = document.createElement("div");
    document.body.appendChild(dragcover);
    dragcover.style.display = "none";
    dragcover.style.position = "absolute";
    dragcover.style.left = "0";
    dragcover.style.top = "0";
    dragcover.style.right = "0";
    dragcover.style.bottom = "0";
    // dragcover.style.background = "pink";
    // dragcover.style.opacity = "0.5";
})
var left = document.getElementById("mapd");
var fb = null;

if (window.matchMedia("(orientation: portrait)").matches) {
	var orientation = "portrait";
} else {
	var orientation = "landscape";
}


d3.select("#drag .area").call(d3.drag().container(function () { return document.getElementById("content"); }).on("start", function () {
    // fb = left.style.flexBasis ? parseInt(left.style.flexBasis) : left.getBoundingClientRect().width;

	if (orientation == "portrait") {
 	   fb = left.getBoundingClientRect().height;
	} else {
	    fb = left.getBoundingClientRect().width;
	}
    // console.log("width at start", fb);
    dragcover.style.display = "block";
}).on("drag", function (e) {
    // console.log("dx", d3.event.dx);
	if (orientation == "portrait") {
	    fb += d3.event.dy;
	} else {
	    fb += d3.event.dx;
	}
    // console.log("new fb", fb);
    left.style.flexBasis = fb+"px";
}).on("end", function (e) {
    // console.log("end");
    dragcover.style.display = "none";
}));


})(); /* root closure */
