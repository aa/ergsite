var URLToolkit = require("url-toolkit");

function index () {
  var NS_CATEGORY = 14,
    ret = {},
    nodes = [],
    // cats = [],
    nodes_by_href = {};

  ret.nodes = nodes;

  function is_cat (d) {
    return d.parent.namespace_id == NS_CATEGORY;
  }

  function resolve_redirects (x) {
    while (x.redirects_to) {
      if (nodes_by_href[x.redirects_to]) {
        x = nodes_by_href[x.redirects_to];
      } else {
        console.log("nodeindex: warning: resolve_redirects, not found", x.redirects_to);
        return x;
      }
    }
    return x;
  }

  function abs_url_path (x, baseurl) {
    return URLToolkit.parseURL(URLToolkit.buildAbsoluteURL(baseurl, x)).path
  }

  function resolve_urls(x, baseurl) {
    if (Array.isArray(x)) {
      var ret = x.filter(function (x) { return x !== null })
        .map(function (x) { return abs_url_path(x, baseurl)});
    } else {
      var ret = abs_url_path(x, baseurl);
    }
    // console.log("resolve_urls", ret);
    return ret;
  }

  function add_nodes (data, baseurl) {
    // correct url (to form: /m/wiki/Catégorie/Site_web.html using data.url)
    // record in nodes_by_href index
    // and add non-redirected nodes to nodes
    // console.log("add_nodes", data, baseurl);

    var base_url = data.url.replace(/^\//, '').replace(/\/$/, ''),
      i, l, d;
    for (i=0, l=data.items.length; i<l; i++) {
      d = data.items[i];
      d.parent = data;
      d.url = abs_url_path(d.url, baseurl);
      nodes_by_href[d.url] = d;
      // d.url = "/m/wiki/" + (base_url ? base_url+"/" : "") + d.url;
      // console.log("*", d.url, dabsurl);
      if (d.redirects_to) {
        // d.redirects_to = "/m/wiki/" + (base_url ? base_url+"/" : "") + d.redirects_to;
        d.redirects_to = resolve_urls(d.redirects_to, baseurl);
      }
      // fix other urls
      if (d.links) { d.links = resolve_urls(d.links, baseurl); }
      if (d.cats) { d.cats = resolve_urls(d.cats, baseurl); }
      if (d.subcats) { d.subcats = resolve_urls(d.subcats, baseurl); }
      if (d.catmembers) { d.catmembers = resolve_urls(d.catmembers, baseurl); }
      if (d.thumbs) { d.thumbs = resolve_urls(d.thumbs, baseurl); }

      if (!d.redirects_to) { nodes.push(d); }

    }
  }
  ret.add_nodes = add_nodes;

  function resolve_link (base, p) {
    var obase = base, op = p;
    // resolve relative links...
    var base = base.split("/");
    base.pop(); // no final path
    while (1) {
      var m = p.match(/^\.\.\/(.+)/);
      if (m) {
        base.pop();
        p = m[1];
      } else {
        break;
      }
    }
    m = p.match(/^\//);
    if (m) { return p; }
    m = p.match(/^https?\:\/\/|mailto\:/);
    if (m) { return p };
    base.push(p);
    var ret = base.join("/");
    // console.log("resolve_link", obase, op, "=>", ret);
    return ret;
  }

  function process_node (d, templates_by_id) {
    // map textual links, cats, catmembers, subcats value to lists of nodes:
    // plus extlinks & backlinks (reverse lookup of links)

    var links = d.links,
      cats = d.cats,
      catmembers = d.catmembers,
      subcats = d.subcats,
      link;
    d.links = [];
    d.cats = [];
    d.subcats = [];
    d.catmembers = [];
    if (d.backlinks === undefined) { d.backlinks = []; }
    d.extlinks = [];
    d.template_name = d.parent.default_template_name || "default";

    if (links) {
      for (var j=0, jl=links.length; j<jl; j++) {
        // link = resolve_link(d.url, links[j]);
        link = links[j];
        // console.log("checking link", link);
        var d2 = node_for_href(link);
        if (d2) {
          // console.log("link.node", d2.title);
          d.links.push(d2);
          if (d2.backlinks === undefined) { d2.backlinks = []; }
          d2.backlinks.push(d);
        } else {
          var m = link.match(/^\/m\/wiki\//);
          if (m) {
            // console.log("warning: ignoring link (node not found)", link);
          } else {
            var m = link.match(/^mailto\:/); // evt exclude everything not https?
            if (m) {
              // console.log("excluding mailto link", m);
            } else {
              // console.log("extlink", link);
              d.extlinks.push(link);        
            }
          }
        }
      }
    }
    if (cats) {
      for (var j=0, jl=cats.length; j<jl; j++) {
        // link = resolve_link(d.url, cats[j]);
        link = cats[j];
        // console.log("checking link", link);
        var d2 = node_for_href(link);
        if (d2) {
          d.cats.push(d2);
          var tname = removeDiacritics(d2.title.replace(/ /g, "_"));
          // console.log("**", d2.title, tname);
          if (templates_by_id[tname]) {
            // console.log("use template_name", tname);
            d.template_name = tname;
          }
        } else {
          // console.log("warning: missing category node", link)
        }
      }
    }
    if (catmembers) {
      for (var j=0, jl=catmembers.length; j<jl; j++) {
        // link = resolve_link(d.url, catmembers[j]);
        link = catmembers[j];
        var d2 = node_for_href(link);
        if (d2) {
          d.catmembers.push(d2);
        } else {
          // console.log("warning: missing category member node", link)
        }
      }
    }
    if (subcats) {
      for (var j=0, jl=subcats.length; j<jl; j++) {
        // link = resolve_link(d.url, subcats[j]);
        link = subcats[j];
        var d2 = node_for_href(link);
        if (d2) {
          d.subcats.push(d2);
        } else {
          // console.log("warning: missing subcat node", link)
        }
      }
    }
  }
  function index (templates_by_id) {
    for (var i=0, l=nodes.length; i<l; i++) {
      var d = nodes[i];
      process_node(d, templates_by_id);
    }
    for (var i=0, l=nodes.length; i<l; i++) {
      var d = nodes[i];
      if (d.catmembers && d.catmembers.length > 0) {
        merge_category_links(d, templates_by_id);

      }
    }
  }
  ret.index = index;

  function merge_category_links (d) {
    /*

    In testing/practice many category pages (such as Page web) have "alternative listings" --
    (cargo) queries that produce tabular results that give a better overview than the default
    alphabetical category listing.
    For this reason, this function:
    1. (Re)moves linked items that are also catmembers.
    2. Follows the order of these items in links (thus following the queried/page order rather than alphabetical/default category order)  

    TODO... FIXUP BACKLINKS!!!

    */
    // create tmp subcat index by url
    var catmembers = d.catmembers,
      links = d.links,
      catmembers_by_url = {},
      n;
    d.links = [];
    d.catmembers = [];
    for (var i=0, l=catmembers.length; i<l; i++) {
      n = catmembers[i];
      catmembers_by_url[n.url] = n;
    }
    for (var i=0, l=links.length; i<l; i++) {
      n = links[i];
      if (catmembers_by_url[n.url]) {
        d.catmembers.push(n);
        delete catmembers_by_url[n.url];
      } else {
        d.links.push(n);
      }
    }
    for (var i=0, l=catmembers.length; i<l; i++) {
      n = catmembers[i];
      if (catmembers_by_url[n.url]) {
        d.catmembers.push(n);
      }
    }
  }










  function node_for_href (href, noredirect) {
      var ret = nodes_by_href[href];
      if (!noredirect && ret) { ret = resolve_redirects(ret) }
      return ret;        
  }
  ret.node_for_href = node_for_href;


  // function parse_document (doc) {
  //   var links1 = doc.querySelectorAll("ul.links1 > li");
  //   console.log("query:nodeindex.parse_document", doc, links1);
  //   for (var i=0, il=links1.length; i<il; i++) {
  //     var li1 = links1[i],
  //       href1 = li1.querySelector("a").getAttribute("href");
  //     // console.log("parse_doc, [1]", href1);
  //     var links2 = li1.querySelectorAll("ul.links2 > li");
  //     for (var j=0, jl=links2.length; j<jl; j++) {
  //       var li2 = links2[j],
  //         href2 = li2.querySelector("a").getAttribute("href");
  //       console.log("parse_doc, [2]", href2);
  //     }
  //   }
  // }
  // ret.parse_document = parse_document;
  
  function add(data) {
    var g = data['@graph'];
    for (var i=0, l=g.length; i<l; i++) {
      var d = g[i];
      // var url = "/m/wiki/" + d.url;
      nodes_by_href[d.url] = d;
      nodes.push(d);
    }
  }
  ret.add = add;

  return ret;
}

module.exports = index;