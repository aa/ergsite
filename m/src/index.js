var search = document.getElementById("searchinput");

window.addEventListener("DOMContentLoaded", function () {
    // console.log("loaded");
    search.focus();
})

var namefilter = (function (opts) {
    var timeout_id = null,
        filter_value = '',
        delay = (opts && opts.delay) || 1000;
    function show_parents (elt) {
        if (elt && elt.style && elt.style.display == "none") {
            elt.style.display = "block";
        }
        if (elt.parentNode) { show_parents(elt.parentNode) }
    }
    function update() {
        // console.log("update", filter_value);
        var pat = new RegExp(filter_value, "i");
        var li = document.querySelectorAll("li");
        for (var i=0, l=li.length; i<l; i++) {
            var elt = li[i],
                a = elt.querySelector("a");
            var n = a.textContent;
            // console.log("n", n);
            if (filter_value == "" || n.match(pat) !== null) {
                show_parents(elt);
                // elt.style.display = "block";
            } else {
                elt.style.display = "none";
            }
        }
    }
    var ret = function (val) {
        filter_value = val;
        if (timeout_id !== null) {
            window.clearTimeout(timeout_id);
            timeout_id = null;
        }
        timeout_id = window.setTimeout(update, delay)
    }
    return ret;
})();

search.addEventListener("keyup", function (e) {
    // console.log("keyup", search.value);
    namefilter(search.value);
}, false);


