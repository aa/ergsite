const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
module.exports = {
  entry: './src/frame.js',
  mode: 'production', // 'development', production
  output: {
    path: path.resolve(__dirname, '..'),
    filename: 'frame.js',
    library: 'frame'
  },
  // plugins: [
  //     new UglifyJSPlugin()
  // ]
};
