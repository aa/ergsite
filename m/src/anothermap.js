var index = require("./pageindex.js")();

var svg = d3.select("#main svg")
  svg_bounds = svg.node().getBoundingClientRect(),
  svg_zoom = d3.zoom()
    .scaleExtent([1 / 16, 4])
    .on("zoom", function () {
      svg_content.attr("transform", d3.event.transform);
    }),
  svg_rect = svg.append("rect")
    .attr("width", svg_bounds.width)
    .attr("height", svg_bounds.height)
    .style("fill", "none")
    .style("pointer-events", "all")
    .call(svg_zoom),
  svg_content = svg.append("g").attr("id", "content"),
  svg_links = svg_content.append("g").attr("class", "links"),
  svg_nodes = svg_content.append("g").attr("class", "nodes");

var simulation = d3.forceSimulation()
    // .force("link", d3.forceLink().id(function(d) { return d.url; }).distance(function (d) { return 30 + (Math.random() * 60) }))
    .force("link", d3.forceLink().id(function(d) { return d.url; }).distance(60))
    .force("x", d3.forceX(0).strength(0.01))
    .force("y", d3.forceY(0).strength(0.01))
    // .force("y", d3.forceY(0).strength(20))
    .force("charge", d3.forceManyBody().strength(-80))
    //.alphaTarget(0.3),
    .on("tick", ticked);

var sim_node = svg.selectAll("a.item"),
  sim_link = svg_links.selectAll("line"),
  current_mouse_item;

function ticked() {
    sim_link
        .attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });
    // node.each(mvmt);
    sim_node
        .attr("transform", function (d) { return "translate("+d.x+","+d.y+")"; });
    // console.log("ticked", node.size());
    // update_active_area();
}

function rebase (url) {
  return "wiki/" + url;
}

function cat_label (x) {
  // console.log("cat_label", x);
  return x.replace(" ", "_");
}

function index_svg_for_templates (svg) {
  var by_id = {};
  // svg.selectAll("title").filter(function (d) {
  //   var t = d3.select(this).text();
  //   return t.indexOf("template") >= 0
  svg.selectAll("*[id]").each(function () {
    // console.log("svg indexing id", this.id);
    by_id[this.getAttribute('id')] = this;
    // this.remove();
    // this.style.visibility = "hidden";
  });
  return by_id;
}

function dragstarted(d) {
  // if (!d3.event.active) simulation.alphaTarget(0.3).restart();
  if (!d3.event.active) simulation.alpha(0.3).restart();
  d.fx = d.x;
  d.fy = d.y;
}

function dragged(d) {
  d.fx = d3.event.x;
  d.fy = d3.event.y;
}

function dragended(d) {
  if (!d3.event.active) simulation.alphaTarget(0);
  d.fx = null;
  d.fy = null;
}

function update_item_state (d) {
  if (d.mouse || d.active) {
    d.fx = d.x;
    d.fy = d.y;
  } else {
    d.fx = null;
    d.fy = null;
  }
}

function setMouseItem (i, center) {
    if (current_mouse_item) {
        d3.select(current_mouse_item).classed("mouse", false).each(function (d) {
            d.mouse = false;
            update_item_state(d);
        });

    }
    current_mouse_item = i;
    if (current_mouse_item) {
        d3.select(current_mouse_item).classed("mouse", true).each(function (d) {
            d.mouse = true;
            update_item_state(d);
        }).raise();
        if (center) {
          var node = d3.select(current_mouse_item).datum();
          function transform() {
            return d3.zoomIdentity
              .translate(svg_bounds.width / 2, svg_bounds.height / 2)
              .scale(1)
              .translate(-node.x, -node.y);
          }
          // console.log("current_mouse_item", node, current_mouse_item);
          // svg_content.call(svg_zoom.transform, d3.zoomIdentity);
          // svg_content.call(svg_zoom.transform, transform);
          svg_content.transition().duration(750).call(svg_zoom.transform, transform);
        }
    }
}

function enter_nodes (sel, templates_by_id) {
  var nodesel = sel.append(function (d,i,nodes) {
    var symbol_id = d.symbol_id || "Main";
    if (!templates_by_id[symbol_id]) { console.log("bad template name", symbol_id); }
    var g0 = document.createElementNS("http://www.w3.org/2000/svg", "a"),
      g1 = document.createElementNS("http://www.w3.org/2000/svg", "g"),
      template = templates_by_id[symbol_id],
      contents = template.cloneNode(true);
    template.style.visibility = "hidden";
    contents.style.visibility = "visible";
    g0.appendChild(g1);
    g1.appendChild(contents);
    g1.setAttribute("class", "contents");
    contents.setAttribute('id', template.getAttribute("id")+i);
    return g0;
  }).each(function (d, i) {
    var bbox = this.getBBox();
    var $this = d3.select(this),
      contents = $this.select("g.contents"),
      title = $this.select("title");
    title.text(d.title);
    // adjust transform to center object at 0, 0
    contents.node().setAttribute("transform", "translate("+ (-bbox.x-(bbox.width/2)) + "," + (-bbox.y-(bbox.height/2)) +")");
  })
  .attr("class", function (d) { return "item " + d.cats.map(cat_label).join(" ") })
  .attr("xlink:href", function (d) { return d.url })
  .attr("xlink:title", function (d) { return d.title })
  .attr("target", "over")
  .on("mouseover", function (d) {
    setMouseItem(this);
  }).on("mouseout", function (d) {
    setMouseItem(null);
  }).call(d3.drag()
      .on("start", dragstarted)
      .on("drag", dragged)
      .on("end", dragended));

  // Add text label
  nodesel.append("text")
    .attr("x", 14)
    .attr("y", 4)
    .text(function (d) { return d.title })
    .call(wrap, 100);


  // var with_pict = nodesel.filter(thumb_for_item);
  // // console.log("with_pict", with_pict.size());
  // with_pict.insert("image", ":first-child")
  //   .attr("class", "thumb")
  //   .attr("x", -24)
  //   .attr("y", -24)
  //   .attr("xlink:href", function (d) { return d.thumb })
  //   .attr("width", 48)
  //   .attr("height", 48);

}

function ensure_array (x) {
  return Array.isArray(x) ? x : [x];
}

// https://bl.ocks.org/mbostock/7555321
// https://stackoverflow.com/questions/24784302/wrapping-text-in-d3#24785497
function wrap(text, width) {
    text.each(function () {
        var text = d3.select(this),
            words = text.text().split(/\s+/).reverse(),
            word,
            line = [],
            lineNumber = 0,
            lineHeight = 1.1, // ems
            x = text.attr("x"),
            y = text.attr("y"),
            dy = 0, //parseFloat(text.attr("dy")),
            tspan = text.text(null)
                        .append("tspan")
                        .attr("x", x)
                        .attr("y", y)
                        .attr("dy", dy + "em");

        while (word = words.pop()) {
            line.push(word);
            tspan.text(line.join(" "));
            if (tspan.node().getComputedTextLength() > width) {
                line.pop();
                tspan.text(line.join(" "));
                line = [word];
                tspan = text.append("tspan")
                            .attr("x", x)
                            .attr("y", y)
                            .attr("dy", ++lineNumber * lineHeight + dy + "em")
                            .text(word);
            }
        }
    });
}

d3.json("data/merge04.json", function (data) {
  console.log("data", svg_nodes);
  index.index_ld(data);
  var templates_by_id = index_svg_for_templates(svg);

  var sel = svg_nodes.selectAll("g").data(data['@graph']).enter();
  enter_nodes(sel, templates_by_id);
  // svg_nodes.selectAll("g").data(data['@graph'])
  //     .enter()
  //     .append("g")
  //     .append("a")
  //   .attr("class", function (d) { return "item" })
  //   .attr("xlink:href", function (d) { return rebase(d.url); })
  //   .attr("href", function (d) { return rebase(d.url); })
  //   .attr("xlink:title", function (d) { return d.title })
  //   .attr("target", "over")
  //   .append("circle")
  //   .attr("r", 10);

  var links = [];

  // DRAW TIMELINE
  var dated = data['@graph'].filter(function (d) {
    var date = d.updated || d.start || d.published;
    if (date) {
      d.date = date;
      return true;
    }
  });
  dated.sort(function (x, y) {
    return (x.date < y.date) ? -1 : ((x.date > y.date) ? 1 : 0);
  });
  for (var i=0, il=dated.length; i<il; i++) {
    if (i+1 < il) {
      links.push({source: dated[i], target: dated[i+1], linktype: 't'})
    }
  }
  console.log("constructed timeline with", dated.length, "items");

  // (ALL) LINKS
  // in order to limit the "arity" of nodes
  // we need to precompute the backlinks...
  // do this in prep stage ?!
  var arity_by_url = {};
  function get_arity (url) {
    return arity_by_url[url] || 0;
  }
  function incr_arity (url) {
    arity_by_url[url] = (arity_by_url[url] || 0) + 1;
  }
  var MAX_NODE_ARITY = 1000;
  data['@graph'].forEach(function (n) {
    if (!n.links) return;
    n.links = ensure_array(n.links);
    for (var li=0, ll = n.links.length; li<ll; li++) {
      var link = n.links[li],
        link_d = index.get(link);
      if (link_d) {
        if (get_arity(n.url) < MAX_NODE_ARITY && get_arity(link_d.url) <=MAX_NODE_ARITY && (!n.date && !link_d.date)) {
        // if (get_arity(n.url) < MAX_NODE_ARITY && get_arity(link_d.url) <=MAX_NODE_ARITY) {
          links.push({source: n, target: link_d, linktype: 'l'});
          incr_arity(n.url);
          incr_arity(link_d.url);
        }
      }
    }
  });

  sim_node = svg.selectAll("a.item"),
  // sim_link = svg_links.selectAll("line");
  simulation.nodes(data['@graph']);

  var link_update = svg_links
    .selectAll("line")
    .data(links, function (d) { return d.linktype+d.source.url+"--"+d.target.url })
  link_update.enter().append("line").attr("class", function (d) { return "link" + d.linktype});
  link_update.exit().remove();

  sim_link = svg_content.select("g.links").selectAll("line");

  // sim_link_l is the selection of all the page links (those of type l, as opposed to c)
  // used to compute the "active_area" hull
  // sim_link_l = svg_content.select("g.links").selectAll("line")
  //   .filter(function (d) { return d.linktype == "l" });

  simulation.force("link").links(links);
  // simulation.alphaTarget(0.3).restart();
  // simulation.alpha(0.3).restart();


});
