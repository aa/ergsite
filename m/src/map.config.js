const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
module.exports = {
  entry: './src/map.js',
  mode: 'development', // 'development', production
  output: {
    path: path.resolve(__dirname, '..'),
    filename: 'map.js',
    library: 'map'
  },
  // plugins: [
  //     new UglifyJSPlugin()
  // ]
};
