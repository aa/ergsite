
function time_spiral (o) {
  // args:
  // o.start: start time
  // o.end: end time
  // o.start_r
  // o.delta_r
  //
  // place elements on a timeline spiral
  // return (date) => {x: , y: }

  var ONE_DAY = 1000 * 60 * 60 * 24,
    start = o.start,
    end = o.end,
    start_r = o.start_radius || 1800,
    delta_r = o.delta_radius || 150,
    start_year = start.getFullYear(),
    end_year = end.getFullYear(),
    dtime = (end - start),
    dy = end_year - start_year,
    dr = delta_r * dy;

  // console.log("time_spiral from", start_year, "to", end_year);

  function dayOfYear (d) {
    return Math.floor( (d - new Date(d.getFullYear(), 0, 0)) / ONE_DAY );
  }

  function position_for_date (d) {
      var year = d.getFullYear(),
        day = dayOfYear(d),
        year_fract = (day/366),
        angle = Math.PI*2 * year_fract,
        year_p = -(d - end) / dtime,
        r = start_r + (year_p * dr);
      
      angle = Math.PI/2 - angle;
      
      var ret = {};
      ret.x = Math.cos(angle) * r;
      ret.y = -Math.sin(angle) * r;
      return ret;
  }
  position_for_date.start = start;
  position_for_date.end = end;

  return position_for_date;
  // var sel = svg.select("g.nodes")
  //   .selectAll("a.xitem")
  //   .data(external_nodes, function (d) { return d.url })
  //   .enter()
  //   .call(enter_nodes2, templates_by_id)
  //   .each (function (d) {
  //     // set x, y based on date
  //     // console.log("d", d.x, d.y);
  //   });
  // var newsel = svg_content.selectAll("a.xitem");
  // console.log("newsel", newsel.size())
  // newsel.attr("transform", function (d) { return "translate("+d.x+","+d.y+")"; });

}
module.exports = time_spiral;