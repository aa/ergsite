const path = require('path');
// const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
module.exports = {
  entry: './src/anothermap.js',
  mode: 'development', // 'development', production
  output: {
    path: path.resolve(__dirname, '..'),
    filename: 'anothermap.js',
    library: 'map'
  },
  // plugins: [
  //     new UglifyJSPlugin()
  // ]
};
