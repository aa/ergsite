
/*

Based on various versions of the class SVG mixin : SVGPan.js
e.g. https://github.com/aleofreddi/svgpan

The idea here is to make a proper class encapsulation that then also 
provides:
* events / callbacks
* element based methods like: centerOnElement
* animation (via css ?!)

*/

function setCTM(element, a, b, c, d, e, f) {
    // CTM: Current Transform Matrix
    if (b === undefined) {
        element.setAttribute("transform","matrix("+a.a+","+a.b+","+a.c+","+a.d+","+a.e+","+a.f+")");
        // element.style.transform = "matrix("+a.a+","+a.b+","+a.c+","+a.d+","+a.e+","+a.f+")";
    } else {
        element.setAttribute("transform","matrix("+a+","+b+","+c+","+d+","+e+","+f+")");
    }
}

function dumpMatrix(matrix) {
    var s = "[ " + matrix.a + ", " + matrix.c + ", " + matrix.e + "\n  " + matrix.b + ", " + matrix.d + ", " + matrix.f + "\n  0, 0, 1 ]";
    return s;
}

function setCTM_animator (vp, m) {
    var a = m.a,
        b = m.b,
        c = m.c,
        d = m.d,
        e = m.e,
        f = m.f;
    // console.log("animate_viewport", a, b, c, d, e, f);
    var step = function () {
        var ctm = vp.getCTM();
        var ad = (a - ctm.a), bd = (b - ctm.b);
        var cd = (c - ctm.c), dd = (d - ctm.d);
        var ed = (e - ctm.e), fd = (f - ctm.f);

        var scaler = 0.05, scaler2 = 0.01;
        var mindelta = 10, mindelta2 = 0.05;
        var an, bn, cn, dn, en, fn;

        var done = true;
        if (Math.abs(ad) < mindelta2) {
            an = a;
        } else {
            an = ctm.a + (scaler2*ad);
            done = false;
        }
        if (Math.abs(bd) < mindelta2) {
            bn = b;
        } else {
            bn = ctm.b + (scaler2*bd);
            done = false;
        }
        if (Math.abs(cd) < mindelta2) {
            cn = c;
        } else {
            cn = ctm.c + (scaler2*cd);
            done = false;
        }
        if (Math.abs(dd) < mindelta2) {
            dn = d;
        } else {
            dn = ctm.d + (scaler2*dd);
            done = false;
        }
        if (Math.abs(ed) < mindelta) {
            en = e;
        } else {
            en = ctm.e + (scaler*ed);
            done = false;
        }
        if (Math.abs(fd) < mindelta) {
            fn = f;
        } else {
            fn = ctm.f + (scaler*fd);
            done = false;
        }
        // console.log("setCTM", vp, an, bn, cn, dn, en, fn);
        setCTM(vp, an, bn, cn, dn, en, fn);
        return done;
    };
    return step;
}

class SVGCamera {
    constructor (svg) {
        this.svg = svg;
        this.g = this.svg.getElementById("viewport");
        this.enablePan = true;
        this.enableZoom = true;
        this.enableDrag = false;
        this.state = '';
        this.stateTf = undefined;
        this.stateOrigin = undefined;
        this.stateTarget = undefined;
        this.setupHandlers();
        this._center_mx = 0.5;
        this._center_my = 0.5;

        this.anim_id = null;
    }

    setupHandlers () {
        // this.svg.setAttributeNS(null, "onmouseup", )
        // setAttributes(this.svg, {
        //     "onmouseup" : "handleMouseUp(evt)",
        //     "onmousedown" : "handleMouseDown(evt)",
        //     "onmousemove" : "handleMouseMove(evt)",
        //     //"onmouseout" : "handleMouseUp(evt)", // Decomment this to stop the pan functionality when dragging out of the SVG element
        // });
        this.svg.addEventListener("mouseup", this.handleMouseUp.bind(this));
        this.svg.addEventListener("mousedown", this.handleMouseDown.bind(this));
        this.svg.addEventListener("mousemove", this.handleMouseMove.bind(this));
        // this.svg.addEventListener("mouseout", this.handleMouseUp.bind(this));

        if(navigator.userAgent.toLowerCase().indexOf('webkit') >= 0)
            window.addEventListener('mousewheel', this.handleMouseWheel.bind(this), false); // Chrome/Safari
        else
            window.addEventListener('DOMMouseScroll', this.handleMouseWheel.bind(this), false); // Others
    }

    getEventPoint(evt) {
        var p = this.svg.createSVGPoint();
        p.x = evt.clientX;
        p.y = evt.clientY;
        return p;
    }

    handleMouseWheel(evt) {
        // console.log("mousewheel", evt);
        if(!this.enableZoom) { return; }

        if(evt.preventDefault) {
            evt.preventDefault();
        }

        evt.returnValue = false;

        var delta;

        // if(evt.wheelDelta)
        //     delta = evt.wheelDelta / 3600; // Chrome/Safari
        // else
        //     delta = evt.detail / -90; // Mozilla
        // var z = 1 + delta; // Zoom factor: 0.9/1.1
        // typical values using FIREFOX -0.01, -0.03 (zoom out) , +0.01-0.03 (zoom in)
        

        if(evt.wheelDelta)
            delta = evt.wheelDelta / 360; // Chrome/Safari
        else
            delta = evt.detail / -9; // Mozilla

        var zoomScale = 0.2; // Zoom sensitivity

        var z = Math.pow(1 + zoomScale, delta);
        // var g = getRoot(svgDoc);
        var p = this.getEventPoint(evt);

        p = p.matrixTransform(this.g.getCTM().inverse());
        // Compute new scale matrix in current mouse position
        var k = this.svg.createSVGMatrix().translate(p.x, p.y).scale(z).translate(-p.x, -p.y);
        setCTM(this.g, this.g.getCTM().multiply(k));

        if (typeof(this.stateTf) == "undefined") {
            this.stateTf = this.g.getCTM().inverse();
        }
        this.stateTf = this.stateTf.multiply(k.inverse());
        // this.unparse();
    }

    unparse () {
        var curCTM = this.g.getCTM();
        var localCurZoom = curCTM.a;
        console.log("camera", curCTM);
    }

    setPositionAndZoom (x, y, z) {
        setCTM(this.g, z, 0, 0, z, x, y);
    }

    handleMouseMove(evt) {
        if(evt.preventDefault) {
            evt.preventDefault();
        }

        evt.returnValue = false;

        if(this.state == 'pan' && this.enablePan) {
            // Pan mode
            var p = this.getEventPoint(evt).matrixTransform(this.stateTf);
            setCTM(this.g, this.stateTf.inverse().translate(p.x - this.stateOrigin.x, p.y - this.stateOrigin.y));
        } else if(this.state == 'drag' && this.enableDrag) {
            // Drag mode
            var p = this.getEventPoint(evt).matrixTransform(this.g.getCTM().inverse());
            setCTM(this.stateTarget, this.svg.createSVGMatrix().translate(p.x - this.stateOrigin.x, p.y - this.stateOrigin.y).multiply(this.g.getCTM().inverse()).multiply(this.stateTarget.getCTM()));
            this.stateOrigin = p;
        }
        // this.unparse();
   }

    handleMouseDown(evt) {
        if(evt.preventDefault) {
            evt.preventDefault();
        }
        evt.returnValue = false;

        if (evt.target.tagName == "svg" || !this.enableDrag ) {// Pan anyway when drag is disabled and the user clicked on an element 
            this.cancel_animation();
            // Pan mode
            this.state = 'pan';
            this.stateTf = this.g.getCTM().inverse();
            this.stateOrigin = this.getEventPoint(evt).matrixTransform(this.stateTf);
        } else {
            // Drag mode
            this.state = 'drag';
            this.stateTarget = evt.target;
            this.stateTf = g.getCTM().inverse();
            this.stateOrigin = getEventPoint(evt).matrixTransform(this.stateTf);
        }
    }

    /**
     * Handle mouse button release event.
     */
    handleMouseUp (evt) {
        if(evt.preventDefault) {
            evt.preventDefault();
        }
        evt.returnValue = false;

        if(this.state == 'pan' || this.state == 'drag') {
            this.state = '';
        }
        // console.log(dumpMatrix(this.g.getCTM()))
    }

    /* ADDITIONS */

    pan (x, y) {
        // var g = this.svg.getElementById("viewport");
        var ctm = this.g.getCTM();
        var k = this.svg.createSVGMatrix().translate(x, y);
        setCTM(this.g, ctm.multiply(k));
    }

    zoom_centered_at_screen_xy (delta, sx, sy) {
        animate_viewport_cancel();
        // var svgDoc = evt.target.ownerDocument;
        var ZOOM_MAX = SVG_ZOOM_ALBUM;
        var ZOOM_MIN = SVG_ZOOM_INIT;

        var z = 1 + delta; // Zoom factor: 0.9/1.1
        var g = svg_viewport; // svg.getElementById("viewport"); // getRoot(svgDoc);
        var p = svg.rootElement.createSVGPoint();
        p.x = sx; p.y = sy;
        p = p.matrixTransform(g.getCTM().inverse());

        // Compute new scale matrix in current mouse position
        var curCTM = g.getCTM();
        var localCurZoom = curCTM.a;
        var newZoom;
        if (ZOOM_MAX) {
            newZoom = (Math.min(ZOOM_MAX, Math.max(ZOOM_MIN, localCurZoom * z)));
        } else {
            newZoom = localCurZoom * z;
        }
        var k = svg.rootElement.createSVGMatrix().translate(p.x, p.y).scale(newZoom/curZoom).translate(-p.x, -p.y); 
        // setCTM(g, curCTM.multiply(k));
        setCTM(g, curCTM.multiply(k));
        // trigger("zoom");
        /*
        if(typeof(stateTf) == "undefined")
            stateTf = g.getCTM().inverse();
        stateTf = stateTf.multiply(k.inverse());
        */
        curZoom = newZoom;
        update_zoom_indicator();
    }

    element_position (element) {
        // returns {x:,y:} in screen coords relative to SVG element
        var bbox = element.getBBox(),
            cx = bbox.x + bbox.width/2,
            cy = bbox.y + bbox.height/2;

        var matrix = element.getScreenCTM();
        return {
            x: (matrix.a * cx) + (matrix.c * cy) + matrix.e,
            y: (matrix.b * cx) + (matrix.d * cy) + matrix.f
        }
        // to take svg position into account
        // var offset = this.svg.getBoundingClientRect();
        // return {
        //     x: (matrix.a * cx) + (matrix.c * cy) + matrix.e - offset.left,
        //     y: (matrix.b * cx) + (matrix.d * cy) + matrix.f - offset.top
        // }
    }

    set_center_xfactor (x) {
        this._center_mx = x;
    }

    set_center_yfactor (y) {
        this._center_mx = y;
    }

    // center_on (element) {
    //     var rect = this.svg.getBoundingClientRect(),
    //         cx = rect.width*this._center_mx,
    //         cy = rect.height*this._center_my,
    //         p = this.element_position(element),
    //         ctm = this.g.getCTM(),
    //         k = this.svg.createSVGMatrix().translate(cx-p.x, cy-p.y);
    //     // setCTM(this.g, ctm.multiply(k));
    //     // setCTM(this.g, k.multiply(ctm));
    //     this.animate(k.multiply(ctm));
    // }

    center_on (element, zoom, animate) {
        var rect = this.svg.getBoundingClientRect(),
            cx = rect.width*this._center_mx,
            cy = rect.height*this._center_my,
            p = this.element_position(element),
            ctm = this.g.getCTM(),
            curzoom = ctm.a,
            translater = this.svg.createSVGMatrix().translate(cx-p.x, cy-p.y);
        // setCTM(this.g, ctm.multiply(k));
        // setCTM(this.g, k.multiply(ctm));
        var newCTM;
        if (zoom) {
            var p2 = this.svg.createSVGPoint();
            p2.x = cx; p2.y = cy;
            p2 = p2.matrixTransform(this.g.getCTM().inverse());
            var zoomer = this.svg.createSVGMatrix()
                .translate(p.x, p.y)
                .scale(zoom/curzoom)
                .translate(-p.x, -p.y);
            newCTM = zoomer.multiply(translater.multiply(ctm));
        } else {
            newCTM = translater.multiply(ctm);
        }
        if (animate) {
            this.animate(newCTM);
        } else {
            setCTM(this.g, newCTM);
        }
    }

    cancel_animation () {
        if (this.anim_id !== null) {
            // console.log("cancel_animation");
            window.clearInterval(this.anim_id);
            this.anim_id = null;
        }
    }

    animate (m) {
        this.cancel_animation();
        var that = this,
            step = setCTM_animator(that.g, m),
            animator = function () {
                var done = step();
                if (done) {
                    that.anim_id = null;
                    // console.log("animation done")
                } else {
                    that.anim_id = window.setTimeout(animator, 10);
                }
            };
        animator();
    }

}

module.exports = SVGCamera;

