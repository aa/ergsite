var SVG_TEXT_WRAP_WIDTH = 100,
  NS_MAIN_URL = "./",
  NS_CATEGORY_URL = "Cat%C3%A9gorie/",
  NS_NEWS_URL = "Actualit%C3%A9s/",
  NS_PAGEWEB_URL = "Page_web/",
  TIMELINE_RADIUS_START = 2000,
  TIMELINE_RADIUS_YEAR_DELTA = 400,
  MOUSE_ITEM_ZOOM = 1.5 ; // zoom when mouse over
  // TIMELINE_RADIUS_START = 600;
  // TIMELINE_RADIUS_YEAR_DELTA = 200;

var IMAGE_TEXT_X = 25,
  IMAGE_TEXT_Y = 4;

var FORCE_MANYBODY_STRENGTH = -1500, // -60
  FORCE_MANYBODY_STRENGTH_ACTIVE = -1270, // repellant force for an active node
  FORCE_GRAVITY_MAIN= 0.2,
  FORCE_GRAVITY_SATELLITE = 0.5;

var ACTIVE_ITEM_SCALE = 0.25,
  MOUSE_ITEM_SCALE = 0.25;

// var linker_core_cb,
//   linker_active_cb,
//   linker_history_cb,
//   linker_cats_cb,
//   linker_timeline_cb,
//   category_list_ul;

/* Simulation status feedback */
var simstatus = d3.select("body").append("div")
  .style("position", "absolute")
  .style("left", "100px")
  .style("bottom", "10px")
  .style("z-index", 1000)
  .text("");

var sim_initial = true;

var set = require("./orderedset.js"),
  svgtextwrap = require("./svgtextwrap.js"),
  URLToolkit = require("url-toolkit"),
  time_spiral = require("./time_spiral.js"),

  // async = require("async"),
  svg = d3.select("svg"),
  svg_bounds = svg.node().getBoundingClientRect(),
  svg_zoom = d3.zoom()
    .scaleExtent([1 / 16, 4])
    .on("zoom", function () {
      svg_content.attr("transform", d3.event.transform);
      // console.log("transform", d3.event.transform, svg_content.attr("transform"));
    }),
  svg_rect = svg.append("rect")
    .attr("width", svg_bounds.width)
    .attr("height", svg_bounds.height)
    .style("fill", "none")
    .style("pointer-events", "all")
    .call(svg_zoom),
  svg_content = svg.append("g").attr("id", "content"),
  svg_links = svg_content.append("g").attr("class", "links"),
  svg_paths = svg_content.append("g").attr("class", "paths"),
  svg_nodes = svg_content.append("g").attr("class", "nodes"),
  width = +svg.attr("width"),
  height = +svg.attr("height"),
  current_mouse_item,
  current_active_item,
  catsdiv = document.getElementById("cats"),
  current_highlight_category = null,
  highlighted_categories = [],
  history = [],
  use_timeline = true,
  sim_links,

  simulation = d3.forceSimulation()
    // .force("link", d3.forceLink().id(function(d) { return d.url; }).distance(function (d) { return 30 + (Math.random() * 60) }))
    .force("link", d3.forceLink().id(function(d) { return d.url; }).distance(120))
    .force("x", d3.forceX(node_gravity_x).strength(node_gravity_force))
    .force("y", d3.forceY(node_gravity_y).strength(node_gravity_force))
    // .force("y", d3.forceY(0).strength(20))
    .force("charge", d3.forceManyBody().strength(node_manybody_strength))
    // .force("collide", d3.forceCollide(60))
    // .force("avoid", avoid_active_area_force())
    //.alphaTarget(0.3),
    .alphaDecay(0.1)
    .alphaMin(0.025)
    .on("tick", sim_ticked)
    .on("end", sim_ended)
    .stop(),

  sim_node = svg.selectAll("a.item"),
  sim_link = svg_links.selectAll("line"),
  // sim_link_l is the selection of all the page links (those of type l, as opposed to c)
  // used to compute the "active_area" hull
  sim_link_l = svg_links.selectAll("line"),


  nodeindex = require("./nodeindex.js")(),
  active_doc_href = null,
  nodes_inited = false;


function sim_ticked() {
    if (sim_initial) return;
    sim_link
        .attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });
    sim_node
        .attr("transform", function (d) { return "translate("+d.x+","+d.y+")"; });
    update_active_area();
    // if (current_active_item) {
    //   centerOnItem(current_active_item, 750);
    // }
    simstatus.text(simulation.alpha());
}

function sim_ended () {
  if (sim_initial) {
    sim_initial = false;
    var sel = svg.select("g.nodes")
      .selectAll("a.item")
      .data(all_nodes, function (d) { return d.url })
      .enter()
      .call(enter_nodes, templates_by_id, true);
    sim_node = svg.selectAll("a.item");
    sim_ticked();
    // console.log("active_doc_href", active_doc_href, current_active_item);
    // set_active_doc_href(active_doc_href);
  }
  simstatus.text("");
  if (active_doc_href) {
    var node = nodeindex.node_for_href(active_doc_href);
    setActiveItem(node, true);
  }
  document.getElementById("load").remove();
}

function node_manybody_strength (d) {
  return (d.linked_active) ? FORCE_MANYBODY_STRENGTH_ACTIVE : FORCE_MANYBODY_STRENGTH;
}

function node_gravity_force (d) {
  if (d.ns && d.ns.url == NS_MAIN_URL) {
    return FORCE_GRAVITY_MAIN;
  }
  return use_timeline ? FORCE_GRAVITY_SATELLITE : FORCE_GRAVITY_MAIN;
}

function node_gravity_x (d) {
  // console.log("node_gravity_x");
  return (use_timeline && d.ax) ? d.ax : 0;
}

function node_gravity_y (d) {
  return (use_timeline && d.ay) ? d.ay : 0;
}

///////////////////////////////////
/* Generic helper functions */

function urljoin (a, b) {
  // used to absolutize the relative url in the redirect
  var lsp = a.lastIndexOf('/');
  if (lsp >= 0) {
      return a.substring(0, lsp+1) + b
  }
  return b;
}

function list_remove_item (list, item) {
  for (var i=0, l=list.length; i<l; i++) {
    if (list[i] == item) {
      list.splice(i, 1);
      i-=1;
    }
  } 
}

function flist (x) {
  if (Array.isArray(x)) {
    return x;
  } else if (x) {
    return [x];
  } else {
    return [];
  }
}

// added explicit fill none because some platforms defaulted to a black fill
// (and seem not to apply the style sheet)... use !important to override in css
var active_area_inner_polygon = svg_links.append("polygon")
  .attr("class", "active_area_inner")
  .attr("style", "fill:none;stroke:none;"),
  active_area_outer_polygon = svg_links.append("polygon")
  .attr("class", "active_area_outer")
  .attr("style", "fill:none;stroke:none;");
var active_area_hull;

function scale_polygon (pp, s) {
  var center = d3.polygonCentroid(pp);
  for (var i=0, l=pp.length; i<l; i++) {
    var p = pp[i];
    p[0] = center[0] + ((p[0]-center[0])*s);
    p[1] = center[1] + ((p[1]-center[1])*s);
  }
}

function update_active_area () {
  if (sim_link_l.size() == 0) {
    active_area_hull = null;
    // hide the polygons
    active_area_inner_polygon.style("display", "none");
    active_area_outer_polygon.style("display", "none");
    return;
  } else {
    active_area_inner_polygon.style("display", "inline");
    active_area_outer_polygon.style("display", "inline");
  }

  var points = []; // Array(sim_link.size()*2);
  sim_link_l.each(function (d, i) {
    var $this = d3.select(this),
      x1 = parseFloat($this.attr('x1')),
      y1 = parseFloat($this.attr('y1'))
      x2 = parseFloat($this.attr('x2')),
      y2 = parseFloat($this.attr('y2'));
    if (!(Number.isNaN(x1) || Number.isNaN(y1) || Number.isNaN(x2) || Number.isNaN(y2))) {
      points.push([x1, y1]);
      points.push([x2, y2]);
    } else {
      // WHY DOES THIS HAPPEN ?! (many links have target x2,y2 as NaN)
      // console.log("NaN", x1, y1, x2, y2);
    }
  });
  // console.log("update_active_area.points", points.length);
  // polygonHull: see https://bl.ocks.org/mbostock/6f14f7b7f267a85f7cdc
  var hull = d3.polygonHull(points);
  // console.log("*", hull[0]);
  if (hull) {
    active_area_inner_polygon.attr("points", hull.map(function (x) { return x[0]+","+x[1] }).join(" "));
    scale_polygon(hull, 1.5);
    active_area_outer_polygon.attr("points", hull.map(function (x) { return x[0]+","+x[1] }).join(" "));
    // use the outer hull as the "avoid" area
    active_area_hull = hull;
  }
}

function avoid_active_area_force (x) {
  var nodes;
  function force(alpha) {
    if (active_area_hull) {
      var center = d3.polygonCentroid(active_area_hull);
      // console.log("center", center);
      for (var i = 0, n = nodes.length, node; i < n; ++i) {
        node = nodes[i];
        if (!node.linked_active && d3.polygonContains(active_area_hull, [node.x, node.y])) {
          // push outward from center
          // console.log("unauthorized node in the zone", node.title);
          var dx = node.x - center[0],
            dy = node.y - center[1];
          node.vx += (1/dx)*100;
          node.vy += (1/dy)*100;
        }
      }
    }
  }
  force.initialize = function(_) {
    nodes = _;
    // initialize();
  };
  return force;
}

function dragstarted(d) {
  // simworker
  if (!d3.event.active) simulation.alphaTarget(0.3).restart();
  d.fx = d.x;
  d.fy = d.y;
}

function dragged(d) {
  d.fx = d3.event.x;
  d.fy = d3.event.y;
}

function dragended(d) {
  // simworker
  if (!d3.event.active) simulation.alphaTarget(0);
  d.fx = null;
  d.fy = null;
}

/*
item / node states

.mouse (mouseover)
.active (doc open)
.highlight (category)

*/
function update_item_state (d) {
  if (d.mouse || d.active) {
    // d.fx = d.x;
    // d.fy = d.y;
  } else {
    d.fx = null;
    d.fy = null;
  }
}


function setMouseItem (i, center) {
  // console.log("setMouseItem", i, center);
    if (current_mouse_item) {
        d3.select(current_mouse_item).classed("mouse", false).each(function (d) {
            d.mouse = false;
            update_item_state(d);
        });

    }
    current_mouse_item = i;
    if (current_mouse_item) {
        d3.select(current_mouse_item).classed("mouse", true).each(function (d) {
            d.mouse = true;
            update_item_state(d);
        }).raise();
        if (center) {
          var node = d3.select(current_mouse_item).datum();
          // console.log("setMouseItem.center", node.x, node.y);
          // console.log("svg_content", svg_content.node());

          centerOnItem(node, 750);
          // var curt = d3.zoomTransform(svg_rect.node());
          // function transform() {
          //   return d3.zoomIdentity
          //     .translate(svg_bounds.width / 2, svg_bounds.height / 2)
          //     .scale(MOUSE_ITEM_ZOOM ? MOUSE_ITEM_ZOOM : curt.k)
          //     .translate(-node.x, -node.y);
          // }
          // // console.log("current_mouse_item", node, current_mouse_item);
          // // svg_content.call(svg_zoom.transform, d3.zoomIdentity);
          // // svg_content.call(svg_zoom.transform, transform);
          // svg_rect.transition().duration(750).call(svg_zoom.transform, transform);

        }
    }
}

function $elt_for_node (node) {
  return d3.selectAll("a")
    .filter(function (d) { return d == node })
}

function page_in_category (node, cat) {
  for (var i=0, l=node.cats.length; i<l; i++) {
    if (node.cats[i] == cat.url) { return true; }
  }
}

function rehighlight_cats () {
  console.log("rehighlight_cats", highlighted_categories.length);
  for (var i=0, l=highlighted_categories.length; i<l; i++) {
    var cat = highlighted_categories[i];
    console.log("cat", cat);
    for (var j=0, jl=cat.catmembers.length; j<jl; j++) {
      var cm = nodeindex.node_for_href(cat.catmembers[j]);
      if (cm) {
        console.log("catmember", cm);
        cm.linked_cat = true;
      }
    }
  }
}

function clear_cats () {
  // linker_cats_cb.checked = false;
  // linker_cats_cb.disabled = true;
  highlighted_categories = [];
  rebuild_links();
  // category_list_ul.innerHTML = "";
}

function toggle_timeline () {
  use_timeline = linker_timeline_cb.checked;
  console.log("use_timeline", use_timeline);
  simulation.force("x").strength(node_gravity_force);
  simulation.force("y").strength(node_gravity_force);
  simulation.alpha(1);
  console.log("sim alpha", simulation.alpha());
  rebuild_links();
}

function add_category (d) {
  // <li><label><input class="leaflet-control-layers-selector" checked="" type="checkbox" /><span>Race chevaline originaire d'Autriche</span><label></li>
  if (highlighted_categories.indexOf(d) !== -1) { return };
  highlighted_categories.push(d);
  d.category_index = highlighted_categories.length-1; // used in styling the links

  // linker_cats_cb.checked = true;
  // linker_cats_cb.disabled = false;

  var li = document.createElement("li"),
    label = document.createElement("label"),
    input = document.createElement("input"),
    span = document.createElement("span");
  span.textContent = d.title;
  label.appendChild(input);
  label.appendChild(span);
  li.appendChild(label);
  li.setAttribute("class", "linkc"+d.category_index); // matching style with line
  input.setAttribute("type", "checkbox");
  input.setAttribute("class", "leaflet-control-layers-selector")
  input.setAttribute("checked", "");
  input.addEventListener("input", function (e) {
    console.log("category.click", d, this.checked);
    // hide it
    li.remove();
    highlighted_categories.splice(highlighted_categories.indexOf(d), 1);
    if (highlighted_categories.length == 0) {
      clear_cats();
    } else {
      /// aaaaaaaaaaaarg, shift means indexes change, meaning colors
      for (var i=0; i<highlighted_categories.length; i++) {
        highlighted_categories[i].category_index = i;
      }
      rebuild_links();
    }
  });
  // category_list_ul.appendChild(li);
}

function set_active_doc_href (href) {
  // console.log("set_active_doc_href", href);
  var node;
  active_doc_href = href;
  if (active_doc_href) {
    node = nodeindex.node_for_href(href);
    if (node) {
      if (is_cat(node)) {
        console.log("adding node to highlighted_categories", node.title);
        // highlighted_categories = [node];
        add_category(node);
        // draw_category_path(node);
      } else {
        if (highlighted_categories.length) {
          // auto de-select category if page isn't in it (and active node isn't the category itself)
          if ((node !== highlighted_categories[0]) &&
              !page_in_category(node, highlighted_categories[0])) {
            console.log("page not in category, clearing");
            highlighted_categories = [];
          }
        }
        setActiveItem(node, true);
      }
    } else {
      // console.log("set_active_doc_href, no node for href", href)
    }
  }
  return;
}

function centerOnItem(item, duration) {
  var curt = d3.zoomTransform(svg_rect.node());
  function transform() {
    return d3.zoomIdentity
      .translate(svg_bounds.width / 2, svg_bounds.height / 2)
      .scale(MOUSE_ITEM_ZOOM ? MOUSE_ITEM_ZOOM : curt.k)
      .translate(-item.x, -item.y);
  }
  if (duration) {
    svg_rect.transition().duration(duration).call(svg_zoom.transform, transform);
  } else {    
    svg_rect.call(svg_zoom.transform, transform);
  }
}

function setActiveItem (d, center) {
    // console.log("setActiveItem", d, center);
    // change i to d

    if (current_active_item) {
        current_active_item.active = false;
        update_item_state(current_active_item);
        svg.selectAll("a.active").classed("active", false);
    }
    current_active_item = d;
    history.push(d);
    if (current_active_item) {
        current_active_item.active = true;
        update_item_state(current_active_item);
        var elt = $elt_for_node(current_active_item)
          .classed("active", true)
          .raise()
          .node();
        if (center && elt) {
          // console.log("transform!", d);

          centerOnItem(d, 750);
          // var curt = d3.zoomTransform(svg_rect.node());
          // function transform() {
          //   return d3.zoomIdentity
          //     .translate(svg_bounds.width / 2, svg_bounds.height / 2)
          //     .scale(MOUSE_ITEM_ZOOM ? MOUSE_ITEM_ZOOM : curt.k)
          //     .translate(-d.x, -d.y);
          // }
          // // console.log("current_mouse_item", node, current_mouse_item);
          // svg_rect.transition().duration(750).call(svg_zoom.transform, transform);

        }
        rebuild_links();
    }
}

/* LINKS / LINKES */

function rebuild_links () {

  var links = [];

  linker_clear();
  // if (linker_core_cb && linker_core_cb.checked) {
  linker_core(links);
  if (!sim_initial) {
      // linker_active_plus2(links);
      // linker_history(links);
      // if (highlighted_categories.length) {
      //    linker_cats(links);
      // }
  }
  // simworker
  // manybody_strength differs depending on active area
  // simulation.force("charge").strength(node_manybody_strength);  
  update_links(links);
  // update_nodes();
  sim_links = links;
  // if (allnodes) {
  //   do_layout(allnodes, links);

  // }
  //simulation.restart();
}

function update_nodes() {
  d3.selectAll("a.item")
    .classed("highlight_cat", function (d) {
      console.log("hc", d.linked_cat);
      return d.linked_cat;
    });
}

function linker_clear () {
  for (var i=0,l=nodeindex.nodes.length; i<l; i++) {
    nodeindex.nodes[i].linked_active = false;
    nodeindex.nodes[i].linked_cat = false;
    nodeindex.nodes[i].linked_history = false;
  }  
}

function link_class (d) {
  var ret = "link" + d.linktype;
  if (d.linktype == "c") {
    ret += " linkc"+d.category.category_index;
  }
  return ret;
}

function update_links (links) {
  // console.log("update_links", links);
  var link_update = svg_content.select("g.links")
    .selectAll("line")
    .data(links, function (d) { return d.linktype+d.source.url+"--"+d.target.url })
  link_update.enter().append("line").attr("class", link_class);
  link_update.exit().remove();

  sim_link = svg_content.select("g.links").selectAll("line");

  // sim_link_l is the selection of all the page links (those of type l, as opposed to c)
  // used to compute the "active_area" hull

  // sim_link_l = svg_content.select("g.links").selectAll("line")
  sim_link_l = sim_link
    .filter(function (d) { return d.linktype == "l" });

  // simworker
  simulation.force("link").links(links);
  // simulation.restart();
}

function is_cat (d) {
  return d.ns.url == NS_CATEGORY_URL;
}

function linker_active_plus2 (links) {
  var nodes = set(); 
  if (current_active_item) {
    walk(current_active_item, 2, nodes, links);
  }  
}

function link_type (source, target) {
  if (source.ns && source.ns.url == NS_MAIN_URL && target.ns && target.ns.url == NS_MAIN_URL) {
    return "l";
  } else {
    return "x";
  }
}

function walk (node, steps, nodes, links, visited) {
  // console.log("wikimap.walk", node, node.url);
  var x;
  if (visited === undefined) { visited = {}; }
  if (visited[node.url]) { return };
  visited[node.url] = true;
  nodes.add(node);
  node.linked_active = true;

  if (steps <= 0) {
    return;
  }
  if (node.links) {
    for (var i=0, l=node.links.length; i<l; i++) {
      x = nodeindex.node_for_href(node.links[i]);
      if (x && !is_cat(x) && !x.hidden) {
        links.push({source: node, target: x, linktype: link_type(node, x)})
        walk(x, steps-1, nodes, links, visited);
      }
    }
  }
  if (node.backlinks) {
    for (var i=0, l=node.backlinks.length; i<l; i++) {
      var x = nodeindex.node_for_href(node.backlinks[i]);
      if (x && !is_cat(x) && !x.hidden) {
        links.push({source: x, target: node, linktype: link_type(x, node)});
        walk(x, steps-1, nodes, links, visited);
      }
    }
  }
}

function linker_cats (links) {
  // create category_links based on highlighted_categories;
  for (var i=0,l=highlighted_categories.length; i<l; i++) {
    var d = highlighted_categories[i];
    console.log("rebuild_category_links", d, d.catmembers.length);
    var prev_node = d;

    if (d.catmembers.length) {
      console.log("linking", d.catmembers.length, "catmembers")
      var nodes = [];
      for (var j=0, jl=d.catmembers.length; j<jl; j++) {
        var p = nodeindex.node_for_href(d.catmembers[j]);
        if (p) {
          nodes.push(p);
          p.linked_cat = true;
        }
      }
      if (nodes.length >= 2) {
        for (var j=1, jl=nodes.length; j<jl; j++) {
          links.push({ source: nodes[j-1], target: nodes[j], category: d, linktype: "c" });
        }
        // close the loop
        links.push({ source: nodes[j-1], target: nodes[0], category: d, linktype: "c" });
      }
    }
  }
}

function linker_core (links) {
  // (ALL) LINKS
  // in order to limit the "arity" of nodes
  // we need to precompute the backlinks...
  // do this in prep stage ?!
  var arity_by_url = {};
  function get_arity (url) {
    return arity_by_url[url] || 0;
  }
  function incr_arity (url) {
    arity_by_url[url] = (arity_by_url[url] || 0) + 1;
  }
  var MAX_NODE_ARITY = 100;
  // core_nodes.forEach(function (n) {
  all_nodes.forEach(function (n) {
    if (!n.links) return;
    n.links = flist(n.links);
    for (var li=0, ll = n.links.length; li<ll; li++) {
      var link = n.links[li],
        link_d = nodeindex.node_for_href(link);
      if (link_d) {
//        if (get_arity(n.url) < MAX_NODE_ARITY && get_arity(link_d.url) <=MAX_NODE_ARITY && (!n.date && !link_d.date)) {
        if (get_arity(n.url) < MAX_NODE_ARITY && get_arity(link_d.url) <=MAX_NODE_ARITY) {
          links.push({source: n, target: link_d, linktype: 'm'});
          incr_arity(n.url);
          incr_arity(link_d.url);
        }
      }
    }
  });
}

function linker_history (links) {
  for (var i=1, l=history.length; i<l; i++) {
    links.push({source: history[i-1], target: history[i], linktype: 'h'});
  }
}


////////////////////////////
// Window events

window.addEventListener("message", function (event) {
    // console.log("WIKIMAP.message", event);
    if (event.data.msg == "mouseover") {
      // console.log("wikimap.mouseover", event.data.href);
      var node = nodeindex.node_for_href(event.data.href),
        item = node ? $elt_for_node(node).node() : undefined;
      // console.log("WIKIMAP.mouseover", event.data.href, item);
      if (item) {
        setMouseItem(item, true);
      }
    } else if (event.data.msg == "docloaded2") {
      // console.log("WIKIMAP.docloaded", event.data.href);
      set_active_doc_href(event.data.href);
    }
}, false);

///////////////////////
// window resize BACKGROUND
// Create a static background rect for zoom + panning
function on_resize () {
  svg_bounds = svg.node().getBoundingClientRect();
  svg_rect.attr("width", svg_bounds.width)
    .attr("height", svg_bounds.height);
  // NEW: for centered zooming?!
  svg.attr("width", svg_bounds.width)
    .attr("height", svg_bounds.height);  
}
window.addEventListener("resize", on_resize);
on_resize();

///////////////////////

function index_svg_for_templates (svg) {
  var by_id = {};
  // svg.selectAll("title").filter(function (d) {
  //   var t = d3.select(this).text();
  //   return t.indexOf("template") >= 0
  svg.selectAll("*[id]").each(function () {
    // console.log("svg indexing id", this.id);
    by_id[this.getAttribute('id')] = this;
    // this.remove();
    // this.style.visibility = "hidden";
  });
  return by_id;
}

var templates_by_id = index_svg_for_templates(svg);

function random_choice (l) {
    return l[Math.floor(Math.random() * l.length)];
}
// console.log("svgrect", svgrect);
function thumb_for_item (d, i) {
  // console.log("thumb_for_item", i, d);
  if (d.thumbs) {
    d.thumb = Array.isArray(d.thumbs) ? d.thumbs[0] : d.thumbs;
    // d.thumb = Array.isArray(d.thumbs) ? random_choice(d.thumbs) : d.thumbs;
    return d.thumb;
  }
}

function index_svg_for_templates (svg) {
  var by_id = {};
  // svg.selectAll("title").filter(function (d) {
  //   var t = d3.select(this).text();
  //   return t.indexOf("template") >= 0
  svg.selectAll("*[id]").each(function () {
    // console.log("svg indexing id", this.id);
    by_id[this.getAttribute('id')] = this;
    // this.remove();
    // this.style.visibility = "hidden";
  });
  return by_id;
}

function cat_label (x) {
  // console.log("cat_label", x);
  var ret = x.match(/\/([^/]*)\.html/)[1];
  return ret.replace(" ", "_");
}

function rebase (url) {
  return "wiki/" + url;
}

function enter_nodes (sel, templates_by_id, add_images) {
  var nodesel = sel.append(function (d,i,nodes) {
    var symbol_id = d.symbol_id || "Main";
    if (!templates_by_id[symbol_id]) { console.log("bad template name", symbol_id); }
    var g0 = document.createElementNS("http://www.w3.org/2000/svg", "a"),
      g1 = document.createElementNS("http://www.w3.org/2000/svg", "g"),
      template = templates_by_id[symbol_id],
      contents = template.cloneNode(true);
    template.style.visibility = "hidden";
    contents.style.visibility = "visible";
    g0.appendChild(g1);
    g1.appendChild(contents);
    g1.setAttribute("class", "contents");
    contents.setAttribute('id', template.getAttribute("id")+i);
    return g0;
  }).each(function (d, i) {
    var bbox = this.getBBox();
    var $this = d3.select(this),
      contents = $this.select("g.contents"),
      title = $this.select("title");
    title.text(d.title);
    // adjust transform to center object at 0, 0
    contents.node().setAttribute("transform", "translate("+ (-bbox.x-(bbox.width/2)) + "," + (-bbox.y-(bbox.height/2)) +")");
  })
  .attr("class", function (d) { return "item " + d.cats.map(cat_label).join(" ") })
  .attr("xlink:href", function (d) { return d.url })
  .attr("xlink:title", function (d) { return d.title })
  .attr("target", "over")
  .on("mouseover", function (d) {
    setMouseItem(this);
  }).on("mouseout", function (d) {
    setMouseItem(null);
  }).on("mousedown", function (d) {
    // console.log("myclickonmousedown")
    window.open(d.url, "over");
  });
  // .call(d3.drag()
      // .on("start", dragstarted)
      // .on("drag", dragged)
      // .on("end", dragended));

  if (add_images) {
    var with_pict = nodesel.filter(thumb_for_item);
    // console.log("enter_nodes: with_pict", with_pict.size());
    // with_pict.insert("image", ":first-child")

      // .append("rect")
      // .attr("class", "thumb")
      // .attr("x", -12)
      // .attr("y", -12)
      // .attr("width", 24)
      // .attr("height", 24);

    with_pict
      .append("image")
      .attr("class", "thumb")
      .attr("x", -12)
      .attr("y", -12)
      .attr("xlink:href", function (d) { return d.thumb })
      .attr("width", 24)
      .attr("height", 24);

    // with_pict
    //   .append("line")
    //   .attr("class", "image")
    //   .attr("x1", -12)
    //   .attr("y1", -12)
    //   .attr("x2", 12)
    //   .attr("y2", -12);
  }

  // Add text label
  nodesel.append("text")
    .attr("x", IMAGE_TEXT_X) 
    .attr("y", IMAGE_TEXT_Y) 
    .text(function (d) { return d.title })
    .call(svgtextwrap, SVG_TEXT_WRAP_WIDTH);

}




function absurl (p) {
  return URLToolkit.buildAbsoluteURL(window.location.toString(), p);
}

// var r = URLToolkit.buildAbsoluteURL(window.location.toString(), "../../wiki/index.json");
// console.log("absurl", r);

var templates_by_id = index_svg_for_templates(svg);
var loading_text = document.getElementById("load_text");
window.addEventListener("DOMContentLoaded", function () {
  console.log("loading data");
  // linker_core_cb = document.querySelector("#linker_core");
  // linker_active_cb = document.querySelector("#linker_active");
  // linker_history_cb = document.querySelector("#linker_history");
  // linker_core_cb.addEventListener("click", rebuild_links, false);
  // linker_active_cb.addEventListener("click", rebuild_links, false);
  // linker_history_cb.addEventListener("click", rebuild_links, false);
  // linker_cats_cb = document.querySelector("#linker_cats");
  // linker_cats_cb.addEventListener("click", clear_cats, false);
  // category_list_ul = document.querySelector("#category-list");

  // linker_timeline_cb = document.querySelector("#linker_timeline");
  // use_timeline = linker_timeline_cb.checked;
  // linker_timeline_cb.addEventListener("click", toggle_timeline, false);

  loading_text.textContent = "Loading the core... 1/4";
  window.setTimeout(function () { d3.json("data/main.json", load1); }, 0);
});


var all_nodes = [],
  core_nodes = [],
  external_nodes = [];


function add_timeline_links () {
  // DRAW TIMELINE
  // var dated = .filter(function (d) {
  //   var date = d.updated || d.start || d.published;
  //   if (date) {
  //     d.date = date;
  //     return true;
  //   }
  // });
  // dated.sort(function (x, y) {
  //   return (x.date < y.date) ? -1 : ((x.date > y.date) ? 1 : 0);
  // });
  for (var i=0, il=external_nodes.length; i<il; i++) {
    if (i+1 < il) {
      all_links.push({source: dated[i], target: dated[i+1], linktype: 't'})
    }
  }
  console.log("constructed timeline with", dated.length, "items");
}

function load1 (data) {
  // show core + links
  core_nodes = data['@graph'];
  // console.log("load1", core_nodes.length);
  all_nodes = core_nodes.slice();

  // ZOOM OUT
  // var cx = svg_bounds.width / 2,
  //   cy = svg_bounds.height / 2;
  // svg_rect.call(svg_zoom.transform, d3.zoomIdentity.translate(cx, cy).scale(0.25));

  nodeindex.add(data);
  // var sel = svg_nodes
  //   .selectAll("a.item")
  //   .data(core_nodes, function (d) { return d.url } )
  //   .enter()
  //   .call(enter_nodes, templates_by_id);

  // sim_node = svg.selectAll("a.item");
  // simworker
  // simulation.nodes(all_nodes);

  // make links
  // rebuild_links();
  // update_sim();

  loading_text.textContent = "Loading categories... 2/4";
  window.setTimeout(function () { d3.json("data/cats.json", load15); }, 0);
}

function load15 (data) {
  nodeindex.add(data);
  // load news...
  loading_text.textContent = "Loading NEWS... 3/4";
  window.setTimeout(function () { d3.json("data/news.json", load2); }, 0);
}

// https://stackoverflow.com/questions/6228302/javascript-date-iso8601
function dateFromISO8601(isostr) {
    var parts = isostr.match(/\d+/g);
    return new Date(parts[0], parts[1]-1, parts[2], parts[3], parts[4], parts[5]);
}

// function dayOfYear (d) {
//   return Math.floor( (d - new Date(d.getFullYear(), 0, 0)) / dayOfYear.ONE_DAY );
// }
// dayOfYear.ONE_DAY = 1000 * 60 * 60 * 24;

function add_external_nodes (g) {
  for (var i=0, l=g.length; i<l; i++) {
    var d = g[i],
      date = d.updated || d.start || d.published;
    if (date) {
      d.date = date;
      d.date_parsed = dateFromISO8601(d.date);
    }
    external_nodes.push(g[i]);
    all_nodes.push(g[i]);
  }
}

function load2 (data) {
  // show news
  nodeindex.add(data);
  add_external_nodes(data['@graph']);
  // load satellites...
  loading_text.textContent = "Loading SATELLITES... 4/4";
  window.setTimeout(function () { d3.json("data/satellite.json", load3); }, 0);
}

function sort_by (l, key, reverse) {
  l.sort(function (a, b) {
    a = key(a);
    b = key(b);
    if (reverse) {
      return (a < b) ? 1 : ((a > b) ? -1 : 0);
    } else {
      return (a < b) ? -1 : ((a > b) ? 1 : 0);
    }
  });
}

var DAY = 1000 * 60 * 60 * 24;
function date_iter (start, end, cb) {
  var s = new Date(start.getFullYear(), start.getMonth(), start.getDate()).getTime(),
    e = new Date(end.getFullYear(), end.getMonth(), end.getDate()).getTime(),
    c = s,
    delta = 1 * DAY,
    ret;
  while (c < e) {
    ret = new Date();
    ret.setTime(c);
    cb(ret);
    c += delta;
  }
}
function date_from_datetime (d) {
  return new Date(d.getFullYear(), d.getMonth(), d.getDate());
}

function days_by_month_from (start, end) {
  start = date_from_datetime(start);
  end = date_from_datetime(end);

  var cm = start.getUTCMonth(),
    cur_month = [],
    ret = [cur_month];
  date_iter(start, end, function (d) {
    var dm = d.getUTCMonth();
    if (dm != cm) {
      // console.log("change of month", d, cm, dm);
      cm = dm;
      cur_month = [];
      ret.push(cur_month);
    }
    cur_month.push(d);
  });
  return ret;
}

function myftime (d) {
  return d.getFullYear()+"-"+d.getUTCMonth()+"-"+d.getUTCDate();
}

MONTHS = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"];
function myftime_label(d) {
  return MONTHS[d.getUTCMonth()]+" "+d.getFullYear();
}


function draw_time_spiral (tspiral, svg_g) {
  // DRAW TIME SPIRAL
  var line = d3.line()
          .x(function(d) { return d.x; })
          .y(function(d) { return d.y; });

  var months = days_by_month_from(tspiral.start, tspiral.end);
  months = months.map(function (m) {
    return {
      date: m[0],
      points: m.map(tspiral)};
  });

  // optionally, reverse some paths
  // months.forEach(function (d) {
  //   if (d.date.getUTCMonth() < 6) {
  //     d.points.reverse();
  //   }
  // })

  // month paths
  svg_g.selectAll("path.month")
    .data(months)
    .enter()
    .append("path")
    .attr("class", "month")
    .attr("id", function (d) {
      return "timeline-"+myftime(d.date);
    })
    .attr("d", function (d) { return line(d.points) })
    .attr("fill", "none")
    .attr("stroke", "magenta");

  // month labels
  svg_g.selectAll("text.month")
    .data(months)
    .enter()
    .append("text")
    .attr("class", "month")
    .append("textPath")
    .attr("xlink:href", function (d) {
      return "#timeline-"+myftime(d.date) })
    .attr("side", "left")
    //.append("tspan")
    .text(function (d) {
      return myftime_label(d.date);
    })
}

function load3 (data) {
  nodeindex.add(data);
  add_external_nodes(data['@graph']);

  var _date = function (x) { return x.date_parsed };
  var xdates = external_nodes.filter(function (x) { return (x.date_parsed !== undefined) });
  sort_by(xdates, _date, false);

  var start = xdates[0].date_parsed,
    end = xdates[xdates.length-1].date_parsed;

  var tspiral = time_spiral({
    start: start,
    end: end,
    start_radius: TIMELINE_RADIUS_START,
    delta_radius: TIMELINE_RADIUS_YEAR_DELTA
  });

  var tspiral_labels = time_spiral({
    start: start,
    end: end,
    start_radius: TIMELINE_RADIUS_START + TIMELINE_RADIUS_YEAR_DELTA,
    delta_radius: TIMELINE_RADIUS_YEAR_DELTA
  });

  // assign anchor points
  xdates.forEach(function (d) {
    var p = tspiral(d.date_parsed);
    d.ax = p.x;
    d.ay = p.y;
  });

  // draw_time_spiral(tspiral_labels, svg_nodes.append("g").attr("class", "timeline"));
  draw_time_spiral(tspiral, svg_nodes.append("g").attr("class", "timeline"));

  // var sel = svg.select("g.nodes")
  //   .selectAll("a.item")
  //   .data(external_nodes, function (d) { return d.url })
  //   .enter()
  //   .call(enter_nodes, templates_by_id, true);

  // update nodes
  // sim_node = svg.selectAll("a.item");
  // simworker
  simulation.nodes(all_nodes);
  // ADDING IN NEW ALL LINKS STYLE...
  rebuild_links();
  simulation.alpha(1);
  simulation.restart();
  // simworker
  // do_layout(all_nodes, sim_links);
  // console.log("all_nodes", all_nodes.length, "sim_node", sim_node.size());

  // reset active item ?!
  // now happening in end of animation hook
  // set_active_doc_href(active_doc_href);

}

/* CONTROLS */
// var layers = document.querySelector(".leaflet-control-layers");
// var toggle = document.querySelector(".leaflet-control-layers-toggle");
// toggle.addEventListener("mouseover", function () {
//   layers.classList.add("leaflet-control-layers-expanded");
// })
// layers.addEventListener("mouseleave", function () {
//   layers.classList.remove("leaflet-control-layers-expanded");
// });
// toggle.addEventListener("click", function () {
//   layers.classList.add("leaflet-control-layers-expanded");
// });

function do_zoom (f, transition) {
  svg_rect.transition(transition).call(svg_zoom.scaleBy, f);
}
var zoom_in = document.querySelector(".leaflet-control-zoom-in");
zoom_in.addEventListener("click", function () { do_zoom(1.25) });
var zoom_out = document.querySelector(".leaflet-control-zoom-out");
zoom_out.addEventListener("click", function () { do_zoom(1/1.25) });

// // worker
// var worker = new Worker("worker.js");
// worker.onmessage = function(event) {
//   switch (event.data.type) {
//     case "tick": return wticked(event.data);
//     case "end": return ended(event.data);
//   }
// };

// function do_layout(nodes, links) {
//   worker.postMessage({
//     nodes: nodes,
//     links: links
//   });
// }

// function wticked(data) {
//   var progress = data.progress;
//   console.log("layout p", progress);

//   // meter.style.width = 100 * progress + "%";
// }
// function ended(data) {
//   var nodes = data.nodes,
//       links = data.links;
//   console.log("layout ended");
//   // console.log("compare", sim_links[0], data.links[0]);
//   for (var i=0, l=data.nodes.length; i<l; i++) {
//     all_nodes[i].x = data.nodes[i].x;
//     all_nodes[i].y = data.nodes[i].y;
//     all_nodes[i].vx = data.nodes[i].vy;
//     all_nodes[i].vy = data.nodes[i].vy;

//   }
//   console.log("post copy");
//   ticked();
// }


// function bounds (points) {
//   var left, top, right, bottom;
//   for (var i=0, l=points.length; i<l; i++) {
//     var p = points[i];
//     if ((left == undefined) || (p.x < left)) {
//       left = p.x;
//     }
//     if ((top == undefined) || (p.y < top)) {
//       top = p.y;
//     }
//     if ((right == undefined) || (p.x > right)) {
//       right = p.x;
//     }
//     if ((bottom == undefined) || (p.y > bottom)) {
//       bottom = p.y;
//     }

//   }
//   return {left: left, top: top, right: right, bottom: bottom};
// }

// function order_points (pp, bb) {
//   // split point into above & below
//   // order above ascending by x
//   // below descending by x
//   var my = bb.top + ((bb.bottom - bb.top) / 2),
//     above = pp.filter(function (x) {
//       return (x.y <= my);
//     }),
//     below = pp.filter(function (x) {
//       return (x.y > my);
//     });
//   console.log("order_points", pp.length, above.length, below.length);
//   function comp_x_asc (a, b) { return (a.x - b.x); }
//   function comp_x_desc (a, b) { return (b.x - a.x); }
//   above.sort(comp_x_asc);
//   below.sort(comp_x_desc);
//   return above.concat(below);
// }

// function draw_category_path (c) {
//   var line = d3.line()
//           .x(function(d) { return d.x; })
//           .y(function(d) { return d.y; });
//   var cm = c.catmembers.map(function (x) {
//       return nodeindex.node_for_href(x);
//     }),
//     bb = bounds(cm);
//   cm = order_points(cm, bb);
//   console.log("draw_category_path", c, cm, bb);

//   svg_paths.selectAll("path.cat")
//     .data([c])
//     .enter()
//     .append("path")
//     .attr("class", "cat")
//     .attr("id", function (d) {
//       return "cat-"+d.title;
//     })
//     .attr("d", function (d) { return line(cm) })
//     .attr("fill", "none")
//     .attr("stroke", "magenta");
