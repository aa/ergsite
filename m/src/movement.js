function movement_with_bounce (left, top, right, bottom, radius) {
    return function (d) {
        // honor d.fx & d.linked
        if (d.linked || d.fx) return;
        d.x += Math.cos(d.heading) * d.speed;
        d.y -= Math.sin(d.heading) * d.speed;
        if (d.x + radius > right) {
            var ba = Math.PI - (Math.PI/2) - d.heading;
            d.heading += ba*2;
            d.x = right - radius;
        } else if (d.x - radius < left) {
            var ba = Math.PI - (Math.PI/2) - d.heading;
            d.heading += ba*2;
            d.x = left + radius;
        }
        if (d.y + radius > bottom) {
            var ba = Math.PI - ((Math.PI/2) - d.heading) - (Math.PI/2);
            d.heading -= ba * 2;
            d.y = bottom - radius
        } else if (d.y - radius < top) {
            var ba = Math.PI - ((Math.PI/2) - d.heading) - (Math.PI/2);
            d.heading -= ba * 2;
            d.y = top + radius
        }        
    }        
}
module.exports = movement_with_bounce;