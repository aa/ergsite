// simple ordered set implementation with custom key function
function set (key) {
  if (key === undefined) { key = function (x) { return x.id } }
  var ret = {},
    index = {},
    items = [];
  ret.add = function (i) {
    var ikey = key(i);
    if (index[ikey] === undefined) {
      items.push(i);
      index[ikey] = true;
    }
  }
  ret.contains = function (i) {
    var ikey = key(i);
    return (index[ikey] !== undefined);
  }
  ret.reset = function () {
    index = {};
    items = [];
    ret.items = items;
  }
  ret.items = items;
  return ret;
}

module.exports = set;