#!/usr/bin/env python3

import argparse, json, sys, os
from objectify_ld import objectify_ld
# from urllib.parse import unquote as urlunquote
# from jinja2 import Template, DictLoader, Environment, FileSystemLoader
# from mediawikidump import myurlquote


def remove_duplicates (items, key=lambda x: x['url']):
    seen = {}
    ret = []
    for x in items:
        kx = key(x)
        if kx not in seen:
            seen[kx] = True
            ret.append(x)
    return ret

# def links (d):
#     # print ("links", d['url'], file=sys.stderr)
#     ret = [dict(linktype="link", **x) for x in d['links'] if type(x) == dict]
#     if 'rev_links' in d:
#         ret += [dict(linktype="rev", **x) for x in d['rev_links']]
#     ret = remove_duplicates(ret)
#     ret.sort(key=lambda x: x['url'])
#     return ret

def backlinks (d):
    # print ("links", d['url'], file=sys.stderr)
    links = [dict(linktype="link", **x) for x in d['links'] if type(x) == dict]
    links_urls = set([x['url'] for x in links])

    def is_redirection_to (x, url):
        return 'redirects_to' in x and x['redirects_to'] == url

    if 'rev_links' in d:
        ret = [dict(linktype="rev", **x) for x in d['rev_links']]
        ret = remove_duplicates(ret)
        ret = [x for x in ret if x['url'] not in links_urls]
        # Remove redirections that point to this
        ret = [x for x in ret if not is_redirection_to(x, d['url'])]
        ret.sort(key=lambda x: x['url'])
    else:
        ret = []
    return ret

# def make_depth_first_uniq_iterator (*initial_items, key=lambda x: x['url']):
#     """
#     Depth first in the sense that when called, items are pre-checked/marked for having been yielded before actually yielded.
#     In this way, recurrent calls of the iterator would suppress items that *will be* emitted due to a previous call
#     """
#     visited = {}
#     for x in initial_items:
#         visited[key(x)] = True
#     def uniq (items):
#         # pre-index items for depth-first
#         emit = []
#         for i in items:
#             ik = key(i)
#             if ik not in visited:
#                 emit.append(i)
#                 visited[ik] = True
#         for i in emit:
#             yield i
#     return uniq


if __name__ == "__main__":
    import copy
    ap = argparse.ArgumentParser("")
    ap.add_argument('--input', type=argparse.FileType('r'), default=sys.stdin)
    ap.add_argument('--output', type=argparse.FileType('w'), default=sys.stdout)
    args = ap.parse_args()

    data = json.load(args.input)
    odata = copy.deepcopy(data)
    index = objectify_ld(data, reverse=True, reverseprefix="rev_")

    for d in odata['@graph']:
        e = index[d['url']]
        d['backlinks'] = [x['url'] for x in backlinks(e)]
        print (d['url'], d['backlinks'], file=sys.stderr)

    # Add in backlinks context
    odata['@context']['backlinks'] = {
        '@type': '@id'
    }

    print (json.dumps(odata, indent=2), file=args.output)