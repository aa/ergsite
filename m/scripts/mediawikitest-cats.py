from __future__ import print_function
from mwclient import Site
import argparse, sys
from mediawikiutils import *


ap = argparse.ArgumentParser("")
ap.add_argument("--wikiprotocol", default="http")
ap.add_argument("--wikihost", default="modedemploi.erg.be")
ap.add_argument("--wikipath", default="/wiki/")
ap.add_argument("--user", default=None)
ap.add_argument("--password", default=None)
ap.add_argument("--page", default=None, help="Dump just the page with the given name (otherwise default is to dump all pages)")
args = ap.parse_args()

print ("Connecting...", file=sys.stderr)
site = Site((args.wikiprotocol, args.wikihost), path=args.wikipath)
if args.user:
    site.login(args.user, args.password)
if args.page:
    page = site.pages[args.page]

# allcats = list()
# allcats.sort(key=lambda x: x.name)

# e = site.pages["Category:Enseignants"]
# for m in e.members(sort="sortkey"):
#     print "  ", m.name.encode("utf-8")



print ("category_members", file=sys.stderr)
for m in category_members(site, "Category:Flux"):
    print (m.name.encode("utf-8"))

print ("subcats", file=sys.stderr)
for m in category_subcats(site, "Category:Flux"):
    print (m.name.encode("utf-8"))

# for c in site.allcategories():
#     print c.name.encode("utf-8")
#     for m in c.members(sort="sortkey"):
#         print "  ", m.name.encode("utf-8")
#     print