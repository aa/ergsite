# http://www.erg.be/erg/spip.php?page=ajax-news&id_breve=946&lang=fr&var_ajax_redir=1
import argparse, html5lib, json, re, sys
from urllib.request import urlopen
from urllib.parse import urlencode
from amateurwebscraper import scrape_fragment_to_mediawiki
from mediawikifixbrokenfilelinks import fixlinks, fixurlfilelinks
from mediawikiutils import ensure_unique_wiki_pagename
from collections import OrderedDict
from mediawikicargoutils import get_cargo_item
from mediawikitemplates import TemplatePage

months = "janvier février mars avril mai juin juillet août septembre octobre novembre décembre".split()

def fix_title (text):
    text = re.sub("\n", " ", text)
    text = re.sub("\s+", " ", text)
    return text

def extract_date (text):
    d = {}
    def repl (m):
        d['day'] = int(m.group(1))
        d['month'] = months.index(m.group(2))+1
        d['year'] = int(m.group(3))
        return ""
    text = re.sub("\(Publié le (\d+)(?:er)? (\w+) (\d\d\d\d)\)", repl, text)
    if 'year' in d:
        return text, d
    else:
        return text, None

def find_image (src):
    """ find first image in source and REMOVE it, returns Image (or None) and (changed) source"""
    ret = {'image': None}
    def r (m):
        ret['image'] = m.group(2)
    src = re.sub(r"\[\[((?:File|Fichier|Image)\:(.+?))(?:\|.+?)\]\]", r, src, count=1)
    return ret['image'], src

def main(args):
    wiki = Site((args.wikiprotocol, args.wikihost), path=args.wikipath)
    if args.user:
        wiki.login(args.user, args.password)
    with open(args.html) as f:
        t = html5lib.parse(f, treebuilder="etree", namespaceHTMLElements=False)
    qvars = {'page': 'ajax-news', 'lang':'fr', 'var_ajax_redir': 1}
    data = []
    count = 0
    items = t.findall(".//a[@href]")
    if args.reverse:
        items = reversed(items)
    for a in items:
        href = a.attrib.get("href")
        # print (href)
        m = re.search(r"^javascript:deployer_news\((\d+)\)", href)
        if m:
            if args.skip and count < args.skip:
                count += 1
                continue
            d = {}
            nid = int(m.group(1))
            title = fix_title(a.text)
            print ("[{0}]newsscraper id:{1}, {2}".format(count+1, nid, title), file=sys.stderr)
            if "/" in title:
                title, subtitle = title.split("/", 1)
                title = title.strip()
                subtitle = subtitle.strip()
            else:
                subtitle = ""

            d['title'] = title
            d['subtitle'] = subtitle
            qvars['id_breve'] = nid
            url = args.url+"?" + urlencode(qvars)
            # print (url, file=sys.stderr)
            # GET THE NEWSITEM ITEM src
            htmlsrc = urlopen(url).read().decode("utf-8").strip()
            # remove "puce" images
            htmlsrc = htmlsrc.replace("""<img src="local/cache-vignettes/L8xH11/puce-32883.gif?1462051513" width='8' height='11' class='puce' alt="-" />""", "")
            # print ("-"*20, file=sys.stderr)
            # print ("GOT HTMLSRC", htmlsrc, file=sys.stderr)
            # count += 1
            # continue
            htmlsrc, published = extract_date(htmlsrc)
            d['html'] = htmlsrc
            # print ("published", published, file=sys.stderr)
            d['published'] = published
            data.append(d)

            # ANY EXISTING PAGE ? get it now & process its src with TemplatePage
            tpage = TemplatePage()
            pvars = get_cargo_item(wiki, "News", where="spipid={0}".format(nid))
            if pvars:
                print ("Reusing existing page {0}".format(pvars['pageName']), file=sys.stderr)
                # print ("pvars", pvars, file=sys.stderr)
                pagetitle = pvars['pageName']
                page = wiki.pages[pagetitle]
                tpage.parse(page.text())
            else:
                pagetitle = ensure_unique_wiki_pagename(wiki, "Actualités:"+title)
                page = wiki.pages[pagetitle]
                assert (not page.exists)

            src = scrape_fragment_to_mediawiki(htmlsrc, args.url)
            src = fixurlfilelinks(src, wiki, phpuploader=None, user=args.user)

            template = tpage.get_template("News", create=True)
            nt = OrderedDict()
            nt['_template'] = "News"
            if '_startpos' in template:
                nt['_startpos'] = template['_startpos']
            if '_endpos' in template:
                nt['_endpos'] = template['_endpos']
            nt['Subtitle'] = subtitle
            nt['Start'] = template.get("Start", "")
            nt['End'] = template.get("End", "")
            nt['Published'] = "{0[year]}/{0[month]:02d}/{0[day]:02d}".format(published)
            # nb: find_image may modify the source (removes the image link in question)
            image, src = find_image(src)
            nt['Image'] = image or ''
            nt['Spipid'] = nid
            nt['Description'] = src
            tpage.replace_template(template, nt)
            src = tpage.unparse()
            # print ("TITLE: {0}".format(page.name), file=sys.stderr)
            # print ("NEWSRC", file=sys.stderr)
            # print ("-"*20, file=sys.stderr)
            # print (src, file=sys.stderr)

            page.save(src, description="news scraper")
            count += 1
            if args.limit and count >= args.limit:
                break

    # print (json.dumps(data, indent=2))


if __name__ == "__main__":
    import argparse
    from mwclient import Site

    ap = argparse.ArgumentParser("")
    ap.add_argument("--wikiprotocol", default="http")
    ap.add_argument("--wikihost", default="erg.activearchives.org")
    ap.add_argument("--wikipath", default="/mw/")
    ap.add_argument("--user", default=None)
    ap.add_argument("--password", default=None)
    ap.add_argument("--html", default="news/Erg-News.html")
    ap.add_argument("--url", default="http://www.erg.be/erg/spip.php")
    ap.add_argument("--skip", type=int, default=None)
    ap.add_argument("--limit", type=int, default=None)
    ap.add_argument("--reverse", action="store_true", default=False)

    args = ap.parse_args()
    main(args)