import argparse, sys
from mwclient import Site

ap = argparse.ArgumentParser("")
ap.add_argument("--wikiprotocol", default="http")
ap.add_argument("--wikihost", default="erg.activearchives.org")
ap.add_argument("--wikipath", default="/mw/")
ap.add_argument("--user", default=None)
ap.add_argument("--password", default=None)
ap.add_argument("--category", default=None)
ap.add_argument("--reason", default=None)

args = ap.parse_args()

wiki = Site((args.wikiprotocol, args.wikihost), path=args.wikipath)
if args.user:
    print ("Login", file=sys.stderr)
    wiki.login(args.user, args.password)

if args.category:
    c = wiki.pages["Category:"+args.category]
    for p in c.members():
        print ("Deleting {0}".format(p.name), file=sys.stderr)
        p.delete(reason = args.reason)

