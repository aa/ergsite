# -*- coding: utf-8 -*-

"""

"""


from __future__ import print_function
from mwclient import Site, APIError
from mwclient.page import Page
import re, os, sys, argparse, datetime, time, json
from jinja2 import Environment, FileSystemLoader, Template
import html5lib
from xml.etree import ElementTree as ET 
from mediawikiutils import category_members, category_subcats, allpages
from mediawiki_get_startpage import get_startpage
from time import mktime
from datetime import datetime
import locale

try:
    from urlparse import urlparse, urljoin
    from urllib import urlencode, quote as urlquote, unquote as urlunquote
except ImportError:
    from urllib.parse import urlparse, quote as urlquote, unquote as urlunquote, urljoin


try:
    locale.setlocale(locale.LC_TIME, "fr_BE.utf8")
except locale.Error:
    print ("Error setting locale", file=sys.stderr)

# https://en.wikipedia.org/wiki/Help:MediaWiki_namespace
# To get localized names use these codes with the site.namespaces dictionary
NS = {
    "Special": -1,
    "Main": 0,
    "User": 2,
    "Site": 4,
    "File": 6,
    "Category": 14
}

# File/Images get resized to this amount for
# thumb: url included as thumb in data for map
# icon: image included
# large: fullsize file linked to (ipv original)
THUMB_SIZE = 96
LARGE_SIZE = 1024

# some etreeutils
def parentchilditerwithindex (elt):
    for parent in elt.iter():
        for i, child in enumerate(parent):
            yield parent, child, i

def replace_elt (t, elt, tag):
    for p, c, i in parentchilditerwithindex(t):
        if c == elt:
            # print ("replacing {0} with {1}".format(elt.tag, tag), file=sys.stderr)
            newelt = ET.SubElement(p, tag)
            p.remove(elt)
            p.remove(newelt)
            p.insert(i, newelt)
            return newelt

def innerHTML (elt):
    if elt.text != None:
        ret = elt.text
    else:
        ret = u""
    return ret + u"".join([ET.tostring(x, method="html", encoding="unicode") for x in elt])


def myurlquote (s):
    """ removing colon! """
    return urlquote(s, safe="/_()")

def strip_namespace (n):
    parts = n.split(":", 1)
    if len(parts) == 2:
        return parts[1]
    return n

def datetimeformat (t, format='%Y-%m-%d %H:%M:%S'):
    return time.strftime(format, time.localtime(t))

##########################

def page_url (site, page):
    """ key function: returns a URL for a given page
    
    """
    # print ("[page_url]", page.name, file=sys.stderr)
    base = os.path.split(site.site['base'])[0]
    uret = os.path.join(base, myurlquote(page.normalize_title(page.name)))
    return uret

def full_url (site, href):
    """ used to absolutize URLS used in links and thumbs """
    base = os.path.split(site.site['base'])[0]
    return urljoin(base, href)

SLASH = "\u2044" # "%2F"

def filename_for_page_title (t, site):
    """ USED replace / with % 2F ... assumes unicode input """
    # print ("filename_for_page_title", t, file=sys.stderr)
    t = t.replace(" ", "_").replace("/", SLASH)
    ns, name = '', t
    if ":" in t:
        for nsid, nsname in site.namespaces.items():
            if t.startswith(filename_for_namespace(nsname)+":"):
                ns, name = t.split(":", 1)
                break
    # print ("ns, name", ns, name, file=sys.stderr)
    name = name.replace(" ", "_").replace("/", SLASH)
    ns = ns.replace(" ", "_").replace("/", SLASH)
    return os.path.join(ns, name)

def filename_for_page (p, site):
    """ USED """
    return filename_for_page_title(p.normalize_title(p.name), site)

def filename_for_namespace (ns):
    return ns.replace(" ", "_").replace("/", SLASH)

def is_edit_link (href):
    return "action=edit" in href

def is_file_link (href):
    return href.startswith("/mw/index.php/Fichier:")

###########################
# Localize file links

def filepage_for_href (wiki, href):
    """ /mw/index.php/Fichier:Foo => page object or None """
    href = urlunquote(href)
    FILE = wiki.namespaces[NS['File']]
    pagename = re.sub(r"^/mw/index\.php/{0}:".format(FILE), FILE+":", href)
    try:
        return wiki.pages[pagename]
    except KeyError:
        print ("Error getting URL for file {0}".format(pagename), file=sys.stderr)
        return None

def first_page_using_file (filepage):
    for cpage in filepage.imageusage():
        return cpage
    
def get_image_url_and_thumb (site, filepage, size=96):
    """ Replacement / complement for mwclient imageinfo, generates thumb url as well as fullsize url"""
    if isinstance(filepage, Page):
        filename = filepage.name
    else:
        filename = filepage
    result = site.api(
        "query",
        titles=filename,
        prop="imageinfo",
        iiprop="url|mime",
        iiurlwidth=size,
        formatversion=2)
    result = result['query']['pages'][0]['imageinfo'][0]
    return result['url'], result['thumburl'], result['mime']

################


# def page_backlinks (p):
#     ret = []
#     for lp in p.backlinks():
#         if lp.redirect:
#             for llp in lp.backlinks():
#                 ret.append(llp)
#         else:
#             ret.append(lp)
#     return ret


def url_to_title (site, url):
    pat = site.site['articlepath'].replace("$1", "THEPAGETITLE")
    pat = re.escape(pat).replace("THEPAGETITLE", "(.+)")
    m = re.search(pat, url)
    if m:
        t = m.group(1)
        t = urlunquote(t)
        t = t.replace("_", " ")
        return t

def localize_pagelinks (pagesrc, parentpath, site, local_images=None, path=None, mw_images="/mw/images", page=None):
    """
    This function rewrites the HTML source of a wiki page
    to use relative links to dumped paths. It also
    gathers/generates metadata (links + thumbs) along the way

    When local_images is given file urls are relativized to this path
    otherwise URLs are absolutized to their location on the wiki server.

    Returns: newpagesrc, links, thumbs

    So the output of site.parse.... has link URLS include unescaped colons
#             <tr>
# <td class="field_Title even"><a href="/mw/index.php/Page_web:Du_plomb_au_web_en_passant_par_l%E2%80%99OCR" title="Page web:Du plomb au web en passant par l’OCR">Du plomb au web en passant par l’OCR</a></td>
# <td class="field_Updated even">2018-03-13 9:59:37 PM</td>
# </tr>

    """
    # use for relativizing links
    # t = html5lib.parse(pagesrc, treebuilder="etree", namespaceHTMLElements=False)
    t = html5lib.parseFragment(pagesrc, treebuilder="etree", namespaceHTMLElements=False)
    links = []
    thumbs = []

    def relativize_file_url (href):
        if href.startswith("http"):
            href = urlparse(href).path
        if href.startswith(mw_images):        
            href = href[len(mw_images):].lstrip("/")
            rpath = os.path.join(local_images, href)
            href = os.path.relpath(rpath, parentpath)
            return href

    def relativize_src_set (ss):
        url, rest = ss.strip().split(" ", 1)
        return relativize_file_url(url)+" "+rest

    for elt in t.findall(".//*[@src]"):
        href = elt.attrib.get("src")
        if href.startswith(mw_images):
            if local_images: # Relativize
                elt.attrib['src'] = relativize_file_url(href)
                if "srcset" in elt.attrib:
                    srcset = elt.attrib.get("srcset", "").strip().split(",")
                    srcset = [relativize_src_set(x) for x in srcset]
                    elt.attrib['srcset'] = ", ".join(srcset)
            else: # Absolutize
                elt.attrib['src']= full_url(site, href)
    redlinks = []
    for a in t.findall(".//*[@href]"):
        linkclass = a.attrib.get("class", "")
        href = a.attrib.get("href")
        if "redlink=1" in href:
            redlinks.append(a)
            continue
        # print ("  RL processing", href, file=sys.stderr)
        if "external" in linkclass:
            if ("wiki" not in linkclass):
                # print ("  RL adding external href", href, file=sys.stderr)
                links.append(href) # use as is
            continue

        # if not "category" in linkclass and not is_edit_link(href):
        #     links.append(href)
        handled = False
        if is_file_link(href):
            # print ("  RL processing file link", href, file=sys.stderr)
            filepage = filepage_for_href(site, href)
            # print ("Processing File", filepage.name, file=sys.stderr)
            if filepage:
                # if cpage:
                #     print ("Found usage page", cpage.name, file=sys.stderr)
                # else:
                #     print ("No usage page", file=sys.stderr)

                # Request and record a (smaller) thumb URL (for the map viz)
                fileurl, thumburl, mime = get_image_url_and_thumb(site, filepage, THUMB_SIZE)
                if thumburl not in thumbs:
                    if local_images:
                        thumbs.append(relativize_file_url(thumburl))
                    else:
                        thumbs.append(thumburl)

                # FOR IMAGES ... Change link to a large size "thumb" rather than the original
                # other kinds of files should be directly linked (aka PDF)
                if mime.startswith("image/"):
                    # point to (site absolute) large size thumb of original file URL (no hostname)
                    # ie /mw/images/a3/1024px_foo.jpg
                    _, largeurl, _ = get_image_url_and_thumb(site, filepage, LARGE_SIZE)
                    if local_images:
                        a.attrib["data-original"] = relativize_file_url(fileurl)
                    else:
                        a.attrib["data-original"] = fileurl
                else:
                    # point to original
                    largeurl = fileurl
                # newhref = url_path(fileurl)
                # LINK TO large version (but *not* full size original)
                # LEAVE ABS URL
                newhref = largeurl
                if local_images:
                    newhref = relativize_file_url(newhref)
                # newhref = urlparse(largeurl).path
                # Does the link contain an image... if so fancyboxify it
                if a.find(".//img") != None:
                    a.attrib["data-fancybox"] = "gallery"
                    cpage = first_page_using_file(filepage)
                    if cpage and cpage != page:
                        # FOR IMAGES appearing in queries, add a caption with a link to the files first usage page
                        cpagefn = filename_for_page(cpage, site)
                        cpage_path = os.path.join(path, filename_for_page_title(cpage.name, site)+".html")
                        cpage_path = os.path.relpath(cpage_path, parentpath)
                        cpage_href = myurlquote(cpage_path)
                        caption = "<a href='{0}' class='cpagelink'>{1}</a>".format(cpage_href, cpage.page_title)
                        a.attrib["data-caption"] = caption
                        a.attrib["data-context-page"] = cpage_href
                a.attrib["href"] = newhref
                links.append(newhref)
                handled = True

        if not handled and not is_edit_link(href):
            # For all other links...
            # since they are not external
            # Attempt to distill a title from the URL
            # 
            title = url_to_title(site, href)
            if title:
                # page = filename_for_page(site.pages[title], "") + ".html"
                href = os.path.join(path, filename_for_page_title(title, site)+".html")
                href = myurlquote(os.path.relpath(href, parentpath))
                a.attrib["href"] = href
                # print ("  RL adding link", href, file=sys.stderr )
                links.append(href)
            # else:
                # print ("  RL link has no title", href, file=sys.stderr)
                # print ("  rewrote link {0} => {1}".format(href, a.attrib['href']).encode("utf-8"), file=sys.stderr)

    # VIDEOS
    # for x in t.findall(".//video"):
    #     poster = x.attrib.get("poster")
    #     src = x.find(".//source")
        # rewrite to link with image inside
        # add poster to thumbs
        # add stuff to links ?! maybe not
        # special CASE :: in the case of VIMEO... link to pageweb.url

    for x in redlinks:
        span = replace_elt(t, x, "span")
        span.text = x.text
        span.attrib['class'] = "redlink"

    # ret = ET.tostring(t, method="html", encoding='unicode')
    ret = innerHTML(t)
    # print (type(ret))
    return ret, links, thumbs

def dumppage (p, site, path, htmltemplate, data=None, local_images=None, force=False):
    if (p.page_title == ""):
        print ("[bad page title, skipping]: {0}".format(p.name), file=sys.stderr)
        return None
    print ("[dumppage] {0}...".format(p.name), file=sys.stderr)
    wikiurl = page_url(site, p)
    def log (path):
        print ("Dumping {0} to {1}...".format(p.name, path), file=sys.stderr)

    thispagefilename = filename_for_page(p, site)
    thispageparentpath = os.path.split(thispagefilename)[0]
    thispagefullparentpath = os.path.split(os.path.join(path, thispagefilename))[0]
    base = os.path.join(path, thispagefilename)
    # mwpath = base + ".mw"
    htmlpath = base + ".html"
    # jsonpath = base + ".json"

    if data != None and 'revision' in data and p.revision == data['revision'] and not force:
        print ("{0} is up to date ({1})".format(p.name, p.revision), file=sys.stderr)
        return data

    # RESET data
    data = {}
    # data['url'] = page_url(site, p)
    # data['mwsrc'] = myurlquote(mwpath)
    # data['localhtml'] = myurlquote(htmlpath)
    thispagebasename = os.path.basename(thispagefilename)
    data['url'] = myurlquote(thispagebasename+".html")
    print ("URL", data['url'], file=sys.stderr)
    # data['json'] = myurlquote(jsonpath)
    # data['redirect'] = p.redirect
    if p.redirect:
        rp = p.redirects_to()
        # data['redirects_to'] = rp.name
        # data['redirects_to_url'] = page_url(site, rp)
        rp = os.path.relpath(filename_for_page(rp, site)+".html", thispageparentpath)
        data['redirects_to'] = myurlquote(rp)

    # data['namespace'] = p.namespace
    # data['is_cat'] = p.namespace == 14
    # data['name'] = p.name
    data['title'] = p.page_title
    data['revision'] = p.revision

    def rel_href (x):
        href = os.path.join(path, filename_for_page_title(x.name, site)+".html")
        return myurlquote(os.path.relpath(href, thispagefullparentpath))

    cats = list(p.categories())
    data['cats'] = [rel_href(x) for x in cats]
    if p.namespace == NS['Category']:
        subcats = category_subcats(site, p.name)
        data['subcats'] = [rel_href(x) for x in subcats]
        catmembers = category_members(site, p.name)
        data['catmembers'] = [rel_href(x) for x in catmembers]

    # ensure parent path
    try:
        os.makedirs(os.path.split(htmlpath)[0])
    except OSError:
        pass

    # output mwsrc
    # log(mwpath)
    # with open(mwpath, 'w') as f:
    #     f.write(pagetext)


    log(htmlpath)
    with open(htmlpath, 'w') as f:
        # p.links() turned out to HANG (or at least take an inordinate amount of time) on dynamic pages / those involving queries
        # Workaround: pull links from the rendered page itself...
        # similarly backlinks + extlinks is excluded here in favor of directly exploring the actual page contents
        # print (usite.parse {0}".format(p.name), file=sys.stderr)

        try:
            htmlsrc = site.parse(page=p.name)['text']['*'] # see note in localize_pagelinks about unescape colons in links
        except APIError as e:
            htmlsrc = ""

        tvars = {
            'title': p.page_title,
            'name': p.name,
            'page': p,
            'data': data,
            'categories': cats,
            'redirect': p.redirect,
            # 'backlinks': backlinks,
            'wikiurl': wikiurl,
            'namespace': p.namespace, # numeric... evt. would be nice to have a text rep of namespace
            'is_cat': (p.namespace == NS['Category']),
        }
        pagetext = p.text() # sets last_rev_time
        try:
            tvars['last_modified'] = datetime.fromtimestamp(mktime(p.last_rev_time))
        except TypeError:
            pass
        if tvars['is_cat']:
            tvars['subcats'] = subcats
            tvars['catmembers'] = catmembers

        tpath, tname = os.path.split(htmltemplate)
        env = Environment(loader=FileSystemLoader(tpath))
        env.filters['datetimeformat'] = datetimeformat
        def local_url (p):
            path = filename_for_page(p, site)+".html"
            path = os.path.relpath(path, thispageparentpath)
            return myurlquote(path)
        env.filters['local_url'] = local_url
        def relativize (url):
            path_in = os.path.join(path, url)
            return os.path.relpath(path_in, thispagefullparentpath)
        env.filters['relativize'] = relativize
        env.filters['page_url'] = lambda x: page_url(site, x)
        env.filters['strip_namespace'] = strip_namespace
        def strftime (dt, format="%Y %m %d"):
            return dt.strftime(format)
        env.filters['strftime'] = strftime
        template = env.get_template(tname)

        # parentpath = os.path.join(path, os.path.split(filename_for_page(p, site))[0])
        htmlsrc, links, thumbs = localize_pagelinks(htmlsrc, thispagefullparentpath, site, local_images=local_images, path=path, page=p)
        tvars['content'] = htmlsrc
        htmlsrc = template.render(**tvars)

        data['links'] = links # [full_url(site, x) for x in links]
        data['thumbs'] = thumbs # [full_url(site, x) for x in thumbs]
        f.write(htmlsrc)

    return data

def process_dump (args):
    site = Site((args.wikiprotocol, args.wikihost), path=args.wikipath)
    if args.user:
        site.login(args.user, args.password)

    context = None
    if args.context:
        with open(args.context) as f:
            context = json.load(f)

    def process_pages (pages, ns_name="", ns_id=0):
        jsonpath = os.path.join(args.path, filename_for_namespace(ns_name), "index.json")
        output = []
        old_items_by_title = {}
        try:
            with open (jsonpath) as f:
                data = json.load(f)
                for p in data['items']:
                    old_items_by_title[p['title']] = p
        except IOError:
            pass
        data = {}
        if ns_name:
            context['@base'] = args.baseurl + myurlquote(filename_for_namespace(ns_name)+"/")
        else:
            context['@base'] = args.baseurl

        if context:
            data['@context'] = context
        data['url'] = ""
        data['namespace'] = ns_name
        data['namespace_id'] = ns_id
        data['items'] = output = []
        # pages = list(site.allpages(namespace=nsid))
        pages = list(pages)
        pages.sort(key=lambda x: x.name)
        for i, p in enumerate(pages):
            print ("{0} {1}/{2} {3}...".format(ns_name, i+1, len(pages), p.page_title), file=sys.stderr)
            d = old_items_by_title.get(p.page_title)
            d = dumppage(p, site, path=args.path, htmltemplate=args.template, data = d, force=args.force, local_images = args.local_images)
            # allow d to be None (if bad page)
            if d != None:
                output.append(d)
            if args.limit and i+1>=args.limit:
                break
        with open(jsonpath, "w") as f:
            json.dump(data, f, indent=2)

    if args.namespace:
        for ns in args.namespace:
            for nsid, name in site.namespaces.items():
                if (ns == name):
                    process_pages(site.allpages(namespace=nsid), name, nsid)
    if args.page:
         p = site.pages[args.page]
         dumppage(p, site, path=args.path, htmltemplate=args.template, force=args.force, local_images=args.local_images)

    if args.allpages:
        process_pages(site.allpages())

    if args.allcats:
        process_pages(site.allcategories(), site.namespaces[NS["Category"]], NS["Category"])
    # elif args.subcats:
    #     top = site.pages[args.subcats]
    #     print ("\nDumping {0} + subcategories ...".format(top.name))
    #     d = dumppage(top, site, path=args.path, htmltemplate = template, force=args.force)
    #     output.append(d)

    #     do = [top]
    #     while len(do):
    #         nextround = []
    #         for x in do:
    #             for c in category_subcats(site, x.name):
    #                 print ("{0}...".format(c.name), file=sys.stderr)
    #                 # USING FORCE=TRUE FOR CATEGORIES since subcats don't seem to be automatically updated ?!
    #                 # d = dumppage(c, site, path=args.path, htmltemplate=template, force=True)
    #                 d = dumppage(c, site, path=args.path, htmltemplate=template, force=args.force)
    #                 output.append(d)
    #                 nextround.append(c)
    #         do = nextround

    # json.dump({'@graph': output}, args.output, indent=2)


def process_template (args):
    site = Site((args.wikiprotocol, args.wikihost), path=args.wikipath)
    if args.user:
        site.login(args.user, args.password)
    # print ("Processing template", file=sys.stderr)
    tpath, tname = os.path.split(args.template)
    env = Environment(loader=FileSystemLoader(tpath))
    env.filters['datetimeformat'] = datetimeformat
    outparentpath, outfilename = os.path.split(args.output)
    def local_url (t):
        path = os.path.join(args.path, filename_for_page_title(t, site)+".html")
        path = os.path.relpath(path, outparentpath)
        return myurlquote(path)
    env.filters['local_url'] = local_url
    def relativize (url):
        path_in = os.path.join(args.path, url)
        return os.path.relpath(path_in, outparentpath)
    env.filters['relativize'] = relativize
    env.filters['page_url'] = lambda x: page_url(site, x)
    env.filters['strip_namespace'] = strip_namespace

    env.filters['parse'] = lambda x: site.parse(text=x)['text']['*']

    def llpl (src):
        htmlsrc, links, thumbs = localize_pagelinks(src, outparentpath, site, local_images=args.local_images, path=args.path)
        return htmlsrc

    env.filters['localize_pagelinks'] = llpl



    template = env.get_template(tname)
    tvars = {'site': site}
    tvars['startpage'] = get_startpage(site)
    tvars['allpages'] = lambda: allpages(site)
    htmlsrc = template.render(**tvars)
    with open(args.output, "w") as f:
        f.write(htmlsrc)

if __name__ == "__main__":
    ap = argparse.ArgumentParser("")
    ap.add_argument("--wikiprotocol", default="http")
    ap.add_argument("--wikihost", default="erg.activearchives.org")
    ap.add_argument("--wikipath", default="/mw/")

    ap.add_argument("--user", default=None)
    ap.add_argument("--password", default=None)


    sub = ap.add_subparsers(help='sub-command')
    sub.required = True

    apdump = sub.add_parser('dump', help="dump sets of pages using a template")

    apdump.add_argument("--path", default="m/wiki", help="path to save pages to, default: m/wiki")
    apdump.add_argument("--local-images", default=None, help="if given, file urls will be rewritten as relative links to this local images folder")
    apdump.add_argument("--limit", type=int, default=None)
    apdump.add_argument("--template", default="m/templates/wikipage.html")
    apdump.add_argument("--page", default=None, help="Dump just the page with the given name (otherwise default is to dump all pages)")
    apdump.add_argument("--allcats", default=False, action="store_true")
    apdump.add_argument("--subcats", default=None)
    apdump.add_argument("--emptycats", default=False, action="store_true")
    apdump.add_argument("--allpages", default=False, action="store_true")
    apdump.add_argument("--force", action="store_true", default=False)
    apdump.add_argument("--output", type=argparse.FileType('w'), default=sys.stdout)
    apdump.add_argument("--css", default=None, help="path to css")
    apdump.add_argument("--namespace", action="append")
    apdump.add_argument("--baseurl", default="http://erg.activearchives.org/m/wiki/", help="should end with a trailing slash")
    apdump.add_argument("--context", default="m/scripts/mediawikidump.context.json", help="add this json as @context")
    apdump.set_defaults(func=process_dump)

    aptemplate = sub.add_parser('template', help="render a template in a site-wide context")
    aptemplate.add_argument("--path", default="m/wiki", help="path to save pages to, default: m/wiki")
    aptemplate.add_argument("--local-images", default=None, help="if given, file urls will be rewritten as relative links to this local images folder")
    aptemplate.add_argument("template")
    aptemplate.add_argument("output")
    aptemplate.set_defaults(func=process_template)

    # args = ap.parse_args()
    # main(args)

    args= ap.parse_args()
    params = vars(args).copy()
    del params['func']
    res = args.func(args)
    # print (res)
