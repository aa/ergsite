#!/usr/bin/env python3

import json, sys

def parse_context (data):
    ret = {}
    url_types = ret['@id_type'] = []
    if "@context" in data:
        for key, value in data['@context'].items():
            if type(value) == dict and "@type" in value:
                typ = value["@type"]
                if typ == "@id":
                    url_types.append(key)
            if value == "@id":
                ret['@id'] = key
    return ret

def filter_value (v, by_id, verbose=False):
    if v == "./":
        v = ""
    if v in by_id:
        # print ("filter_value: found ITEM {0}, replacing with object {1}".format(v, by_id[v]['url']), file=sys.stderr)
        return by_id[v]
    if verbose and not (v.startswith("mailto:") or v.startswith("https:") or v.startswith("http:") or v.startswith("../images")):
        print ("filter_value: not found {0}".format(v), file=sys.stderr)
    return v

def as_list (x):
    if type(x) == list or type(x) == tuple:
        return x
    elif x:
        return [x]
    else:
        return []

def compact_list (x):
    if type(x) == list and len(x) == 1:
        return x[0]
    return x

def objectify_ld (data, reverse=False, reverseprefix="rev_", verbose=False):
    context = parse_context(data)
    id_key = context['@id']
    by_id = {}
    # first pass: create by_id index
    for item in data['@graph']:
        if id_key in item:
            item_id = item.get(id_key)
            # print ("storing item {0} under id {1}".format(item, item_id), file=sys.stderr)
            by_id[item_id] = item
    # second pass, resolve URL values to actual objects using the index
    # TODO: create reverse indexes in the process
    for item_id, item in by_id.items():
        for key in list(item.keys()):
            if key in context['@id_type']:
                newvalues = [filter_value(x, by_id) for x in as_list(item[key])]
                item[key] = newvalues
                if reverse:
                    for v in newvalues:
                        if type(v) == dict:
                            revkey = reverseprefix+key
                            if revkey not in v:
                                v[revkey] = []
                            v[revkey].append(item)
    return by_id

if __name__ == "__main__":
    import argparse
    ap = argparse.ArgumentParser("")
    ap.add_argument("input")
    ap.add_argument("--reverse", action="store_true", default=False)
    ap.add_argument("--reverseprefix", default="rev_")
    ap.add_argument("--verbose", action="store_true", default=False)
    args = ap.parse_args()
    with open (args.input) as f:
        data = json.load(f)
    by_id = objectify_ld(data, reverse=args.reverse, reverseprefix=args.reverseprefix, verbose=args.verbose)
    # for item_id, item in by_id.items():
    #     print ("URL", item_id)
    #     if "links" in item:
    #         print ("  links")
    #         for l in item["links"]:
    #             print ("    {0}".format(l))
