#!/usr/bin/env python3

from __future__ import print_function
import argparse, sys, json, time, os
from jinja2 import Template, DictLoader, Environment, FileSystemLoader

ap = argparse.ArgumentParser("template + svg = interface")
ap.add_argument("template")
ap.add_argument("svg")
ap.add_argument("--output", type=argparse.FileType('w'), default=sys.stdout)
args = ap.parse_args()

tpath, tname = os.path.split(args.template)
env = Environment(loader=FileSystemLoader(tpath))
import jinjafilters
for name, fn in jinjafilters.all.items():
    env.filters[name] = fn

from xml.etree import ElementTree as ET  

ET.register_namespace("","http://www.w3.org/2000/svg")
ET.register_namespace("xlink","http://www.w3.org/1999/xlink")
with open(args.svg) as f:
    svgt = ET.parse(f)
    # print ("svgt", svgt)
    svg_root = svgt.getroot()
    svg_root.attrib['xmlns:xlink'] = 'http://www.w3.org/1999/xlink'
    del svg_root.attrib['viewBox']
    svg = ET.tostring(svgt.getroot(), method="xml")

tvars = {'svg': svg}
template = env.get_template(tname)
print (template.render(**tvars).encode("utf-8"), file=args.output)
