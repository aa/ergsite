from urllib.parse import urlparse, urljoin, unquote as urlunquote
from urllib.request import Request, urlopen
from urllib.error import HTTPError, URLError
import os, sys

# extensions for wget 
EXT = {}
EXT["image/png"] = "png"
EXT["image/gif"] = "gif"
EXT["image/jpeg"] = "jpg"
EXT["image/jpg"] = "jpg"
EXT["image/svg"] = "svg"
EXT["image/svg+xml"] = "svg"
EXT["application/svg+xml"] = "svg"
EXT["application/svg"] = "svg"
EXT["application/pdf"] = "pdf"

def ensure_extension (filename, ext):
    base, fext = os.path.splitext(filename)
    return base+"."+ext

def wget (url, cachedir, blocksize=4*1000, user_agent='Mozilla/5.0'):
    # if type(url) == unicode:
    #     url = url.encode("utf-8")
    try:
        # fin = urlopen(url)
        fin = urlopen(Request(url, headers={'User-Agent': user_agent}))
        ct = fin.info().get("content-type")
        filename = os.path.basename(urlunquote(urlparse(url).path)) or "image"
        if ct in EXT:
            filename = ensure_extension(filename, EXT[ct])
        count = 0
        # path = os.path.join(cachedir, "tmp."+EXT[ct])
        path = os.path.join(cachedir, filename)
        with open(path, "wb") as fout:
            while True:
                data = fin.read(blocksize)
                if not data:
                    break
                fout.write(data)
                count += len(data)
        if count > 0:
            return path

        # print ("[wget] skipping {0} content-type {1}".format(url, ct), file=sys.stderr)
        # return None

    except HTTPError as e:
        print ("[wget]: skipping {0} HTTP ERROR {1}".format(url, e.code), file=sys.stderr)
        return None
    except URLError as e:
        print ("[wget]: skipping {0} URL ERROR {1}".format(url, e.reason), file=sys.stderr)
        return None
    # CATCH ALL EXCEPTIONS (dangerous)
    # except Exception as e:
    #     print ("[wget]: skipping {0} An exception occured {1}".format(url, e), file=sys.stderr)
    #     return None

# def youtube_dl (url, cachedir, format=None, youtube_dl_path="youtube-dl"):
#     args = [youtube_dl_path]
#     if format != None:
#         args.append("-f")
#         args.append(format)
#     args.append(url)
#     p = subprocess.Popen(args, cwd=cachedir, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
#     output, _ = p.communicate()
#     output = output.decode("utf-8")
#     m = re.search(r"^\[download\] Destination: (.+?)\s*$", output, re.M)
#     if m:
#         filename = m.group(1)
#         # print ("DOWNLOADED \"{0}\"".format(filename), file=sys.stderr)
#         return os.path.join(cachedir, filename)
#     else:
#         m = re.search(r"^\[download\] (.+?) has already been downloaded", output, re.M)
#         if m:
#             filename = m.group(1)
#             # print ("ALREADY DOWNLOADED \"{0}\"".format(filename))
#             return os.path.join(cachedir, filename)
#         else:
#             # print ("DOWNLOAD FAILURE", file=sys.stderr)
#             return None

def download_url_to_file (href, cachedir):
    """ returns path to file """
    # if "mixcloud.com/" in href:
    #     print ("[mixcloud] {0}".format(href), file=sys.stderr)
    #     return youtube_dl(href, cachedir)
    # elif "vimeo.com/" in href:
    #     print ("[vimeo] {0}".format(href), file=sys.stderr)
    #     return youtube_dl(href, cachedir, format="http-360p")
    # elif "youtube.com/" in href:
    #     print ("[youtube] {0}".format(href), file=sys.stderr)
    #     return youtube_dl(href, cachedir, format="18")
    # elif "youtu.be/" in href:
    #     print ("[youtube] {0}".format(href), file=sys.stderr)
    #     return youtube_dl(href, cachedir, format="18")
    # else:
    print ("[download_url_to_file]", href, file=sys.stderr)
    return wget(href, cachedir)

