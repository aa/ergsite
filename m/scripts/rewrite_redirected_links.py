#!/usr/bin/env python3

import argparse, json, sys, os
from urllib.parse import urljoin
from objectify_ld import objectify_ld

from mediawikidump import myurlquote
import html5lib
from xml.etree import ElementTree as ET


def resolve_redirects(d, index):
    while "redirects_to" in d:
        # print ("redirects_to", type(d['redirects_to']), file=sys.stderr)
        # print ("redirect {0} -> {1}".format(d['url'], d['redirects_to'][0]['url']), file=sys.stderr)
        d = d['redirects_to'][0]
    return d

def rewrite_redirected_links (t, index, url_for_file):
    count = 0
    for a in t.findall(".//a[@href]"):
        oldhref = a.attrib.get("href")
        href = oldhref
        href = urljoin(url_for_file, href)
        if href in index:
            d = index[href]
            # print ("d", d['url'], file=sys.stderr)
            d = resolve_redirects(d, index)
            if (type(d) != dict):
                print ("WARNING: REDIRECTION TO NON-EXISTANT PAGE {0}".format(d))
                continue
            if d['url'] != href: # string indices must be integers?!
                newhref = d['url']
                newhref = os.path.relpath(newhref, os.path.split(url_for_file)[0])
                print ("[rewrite_redirected_links] {0}: {1} --> {2}".format(url_for_file, oldhref, newhref), file=sys.stderr)
                a.attrib['href'] = newhref
                count += 1
        # print ("sniff {0}".format(href), file=sys.stderr)
    return count

if __name__ == "__main__":
    ap = argparse.ArgumentParser("")
    ap.add_argument("--data", default="m/data/merge.json")
    ap.add_argument("--basepath", default="m/wiki/")
    ap.add_argument('input', nargs="+")
    args = ap.parse_args()

    with open(args.data) as f:
        data = json.load(f)
        index = objectify_ld(data, reverse=True, reverseprefix="rev_")

    for n in args.input:
        # print (n, file=sys.stderr)
        # url_for_file = myurlquote(n.replace(args.basepath, ""))
        url_for_file = "/" + myurlquote(n)

        # if url_for_file not in index:
        #     print ("Warning: {0} not in index (deleted page?)".format(url_for_file), file=sys.stderr)
        #     continue
        # d = index[url_for_file]

        with open(n) as f:
            t = html5lib.parse(f.read(), namespaceHTMLElements=False)

        if rewrite_redirected_links(t, index, url_for_file):
            with open(n, "w") as f:
                print ("<!DOCTYPE html>", file=f)
                print (ET.tostring(t, method="html", encoding="unicode"), file=f)
