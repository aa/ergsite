import re


def r(m):
    link = m.group(1)
    label = m.group(2)
    label = label.strip()
    file = m.group(3)
    if label:
        return "[[{0}|link={1}|{2}]]".format(file, link, label)
    else:
        return "[[{0}|link={1}]]".format(file, link)
    # return r"[[\2|link=\1]]"

test = """

[https://www.mixcloud.com/widget/iframe/?feed=https%3A%2F%2Fwww.mixcloud.com%2FErgote_Radio%2Fworkshop-the-reader%2F&hide_cover=1 Workshop The Reader[[File:https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/a/2/6/a/7283-925e-4d22-a5f4-e3add659bf44]]]<br />

[https://www.mixcloud.com/widget/iframe/?feed=https%3A%2F%2Fwww.mixcloud.com%2FErgote_Radio%2Fworkshop-the-reader%2F&hide_cover=1 [[File:https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/a/2/6/a/7283-925e-4d22-a5f4-e3add659bf44]]]<br />

[https://www.mixcloud.com/widget/iframe/?feed=https%3A%2F%2Fwww.mixcloud.com%2FErgote_Radio%2Fd%25C3%25A9bat-r%25C3%25A9sille-3%2F&hide_cover=1 D&amp;bat Résille #3[[Image:https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/7/3/3/a/f6c0-b32b-4ac9-b10a-7209db50375b]]]

"""

print ( re.sub(r"\[(https?:\/\/\S+) (.*)\[\[((?:File|Image)\:.+?)\]\]\]", r, test) )
