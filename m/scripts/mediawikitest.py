from mwclient import Site
import argparse


ap = argparse.ArgumentParser("")
ap.add_argument("--wikiprotocol", default="http")
ap.add_argument("--wikihost", default="erg.activearchives.org")
ap.add_argument("--wikipath", default="/mw/")
ap.add_argument("--user", default=None)
ap.add_argument("--password", default=None)
ap.add_argument("--page", default=None, help="Dump just the page with the given name (otherwise default is to dump all pages)")
args = ap.parse_args()

site = Site((args.wikiprotocol, args.wikihost), path=args.wikipath)
if args.user:
    site.login(args.user, args.password)
if args.page:
    page = site.pages[args.page]

