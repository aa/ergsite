import re, json
from collections import OrderedDict


def escape_template_value (src):
    if type(src) == str:
        return src.replace("|", "{{!}}")
    else:
        return src

def unparse_templates (templates, osrc):
    newsrc = ''
    pos = 0
    dpos = 0

    # Use only templates that come from the osrc (have start and endpos)
    # and make sure they are in original order (they ought to be anyway, but sort just in case)
    use_templates = [ t for t in templates if ('_startpos' in t and '_endpos' in t) ]
    use_templates.sort(key=lambda x: x['_startpos'])

    for t in use_templates:
        start, end = t['_startpos'], t['_endpos']
        if start > pos:
            newsrc += osrc[pos:start]
        pos = start
        newsrc += unparse_template(t)
        pos = end
    if pos < len(osrc):
        newsrc += osrc[pos:]
    return newsrc

def unparse_template(d):
    first = True
    ret = "{{"
    for key, value in d.items():
        if (key == "_template"):
            assert(first)
            ret += value+"\n"
            first = False
        elif not key.startswith("_"):
            assert (not first)
            ret += "|{0}={1}\n".format(key, escape_template_value(value))
    ret += "}}"
    return ret

def parse_templates_iter (src):
    """ Extract & parse templates, remembering location in original src """
    def parse_template (src):
        parts = src.split("|")
        d = OrderedDict()
        d['_template'] = parts[0].strip()
        for p in parts[1:]:
            if "=" in p:
                pname, pval = p.split("=", 1)
            else:
                pname, pval = p, ''
            pname = pname.strip()
            pval = pval.strip()
            d[pname] = pval
        return d

    def read_contents (tokens):
        ret = ""
        open_count = 1
        for t, start, end in tokens:
            if t == "{{":
                open_count += 1
            elif t == "}}":
                open_count -= 1
                if open_count == 0:
                    return ret, end
            ret += t
        return ret, end

    def _tokens (src):
        last = 0
        for x in re.finditer(r"{{|}}", src):
            start, end, txt = x.start(0), x.end(0), x.group(0)
            # print (start, end, txt)
            if start > last:
                yield src[last:start], last, start
            yield txt, start, end
            last = end
        if end < len(src):
            yield src[end:], end, len(src)

    ret = []
    # tokens = re.finditer(r"((?:\{\{)|(?:\}\})|(?:[^\{\}]*))", src, flags=re.DOTALL)
    # return list(tokens)
    # tokens = iter(re.split(r"({{|}})", src))
    tokens = _tokens(src)
    for p, start, end in tokens:
        if p == "{{":
            src, endpos = read_contents(tokens)
            t = parse_template(src)
            t['_startpos'] = start
            t['_endpos'] = endpos
            yield t

def parse_templates (src):
    return list(parse_templates_iter(src))


class TemplatePage (object):
    def __init__(self):
        self.src = ''
        self.templates = []

    def parse(self, src):
        self.src = src
        self.templates = parse_templates(src)

    def get_template (self, name, create=False):
        for t in self.templates:
            if t['_template'] == name:
                return t
        if create:
            t = OrderedDict()
            t['_template'] = name
            self.templates.append(t)
            return t

    def replace_template (self, old, new):
        index = self.templates.index(old)
        self.templates.remove(old)
        self.templates.insert(index, new)

    def unparse (self, append_new_templates=False):
        """ default behaviour is to prepend new templates """
        old_templates = []
        new_templates = []
        for t in self.templates:
            if ('_startpos' in t and '_endpos' in t):
                old_templates.append(t)
            else:
                new_templates.append(t)
        if old_templates:
            src = unparse_templates(old_templates, self.src)
        else:
            src = self.src
        if new_templates:
            newsrc = ''
            for t in new_templates:
                newsrc = newsrc + unparse_template(t) + "\n"
            if append_new_templates:
                src = src + "\n" + newsrc
            else:
                src = newsrc + src
        return src

if __name__ == "__main__":
    text = """
ONE
{{News
|Start=2017/12/05
|End=2017/12/05
|Published=2017/11/22
|Image=Talk A.Maes ERG-263x300.jpeg
|Description=Anne Marie Maes + Nova XX le mardi 5/12 au local 1P.02 de 11 à 12h.
présentation dans le cadre du Master « Pratiques graphiques et complexité scientifique » 
Ouvert à tou.te.s ! Curieu.x.ses bienvenu.e.s
—————————————
Dans le cadre de l’exposition NOVA XX qui se tiendra aux Halles Saint-Gerry dès le vendredi 9 décembre, nous aurons le plaisir de recevoir à l’erg Stéphanie Pécourt, curatrice de l’exposition, et Anne Marie Maes, artiste.
Le NOVA XX entend valoriser les oeuvres de créatrices. Il vise à contribuer aux investissements transversaux dans la recherche pluridisciplinaire et ce en favorisant la rencontre entre les innovateurs de champs complémentaires. A l’adresse des jeunes générations, le NOVA XX vise à stimuler des vocations et la découverte de talents féminins. 
ANNE-MARIE MAES (BE)
SENSORIAL SKIN FOR AN INTELLIGENT GUERILLA BEEHIVE
Anne Marie Maes est une artiste fondatrice et directrice de plusieurs collectifs d’art à but non lucratifs tels que Looking Glass, So-on et Okno. Pendant des décennies, elle a été une leader reconnue pour des projets novateurs en art et science en Belgique, et a trouvé des manières très originales de mettre en lumière les structures cachées dans la nature en construisant des méthodes originales bio-technologiques pour sonder le monde vivant, qu’elle traduit ensuite en créations artistiques.
Elle a un profil international solide et a exposé (entre autres) à Bozar à Bruxelles, à l’Université de Koç à Istanbul, au Borges Center à Buenos Aires, à l’Arsenals Museum de Riga, à la Skolska Gallery de Prague, à l’Institut de biologie évolutive de Barcelone, au Musée du design de Mons, à l’Institut des études avancées de Berlin, au Museum de Domeinen à Sittard, à la Grey Area Gallery à Korcula, au Musée des sciences et des techniques à Milan. En 2017, elle a reçu la Mention honorifique dans la catégorie Arts hybrides à Ars Electronica pour The Intelligent Guerilla Beehive.
{{ Template }}
The work of AnneMarie Maes is a tight coupling between art and science. It meanders on the edge of biology, ecology and technology. AnneMarie Maes’ artistic research is materialized in techno-organic objects that are inspired by factual/fictional stories; in artifacts that are a combination of digital fabrication and craftsmanship; in installations that reflect both the problem and the (possible) solution, in multispecies collaborations, in polymorphic forms and models created by eco-data.

https://annemariemaes.net/
}}
TWO
{{foo|bar=|baz=michael }}
THREE
"""

    templates = parse_templates(text)
    print ("templates", len(templates))
    for i, t in enumerate(templates):
        t['templateIndex'] = i + 1

    print (unparse_templates(templates, text))
    # print (json.dumps(templates, indent=2))
    # for i, t in enumerate(templates):
    #     print ("*{0}*".format(text[t['_startpos']:t['_endpos']]))