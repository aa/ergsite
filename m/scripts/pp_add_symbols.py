import argparse, json, sys, os
import unicodedata
from urllib.parse import quote as urlquote, unquote as urlunquote
from xml.etree import ElementTree as ET 


def strip_accents(s):
   return ''.join(c for c in unicodedata.normalize('NFD', s)
                  if unicodedata.category(c) != 'Mn')

ap = argparse.ArgumentParser("Add symbol_id property based on svg input and node categories")
ap.add_argument('--input', type=argparse.FileType('r'), default=sys.stdin)
ap.add_argument('--output', type=argparse.FileType('w'), default=sys.stdout)
ap.add_argument('--main', default="Main")
ap.add_argument('--category', default="Category")
ap.add_argument('--catlabel', default="/m/wiki/Cat%C3%A9gorie/")
ap.add_argument("svg")
args = ap.parse_args()


def extract_ids (svgpath):
    symbol_ids = set()
    t = ET.parse(svgpath)
    for idelt in t.findall(".//*[@id]"):
        _id = idelt.attrib['id']
        print ("svgid", _id, file=sys.stderr)
        symbol_ids.add(_id)
    return symbol_ids

# extract the ids given in the SVG file
symbol_ids = extract_ids(args.svg)

# Read the input json data
data = json.load(args.input)
c = data['@context']
g = data['@graph']

def ensure_list (x):
    if type(x) == str:
        return [x]
    return x

def cat_name (x):
    """map data category filename to a matching symbol id
    /m/wiki/Cat%C3%A9gorie/Cours_th%C3%A9oriques.html 
    ==>
    Cours_theoriques
    """
    _, x = os.path.split(x)
    x, _ = os.path.splitext(x)
    x = urlunquote(x)
    # x = x.replace("'", "")
    x = strip_accents(x)
    return x

for x in g:
    urlp = urlunquote(x['url'])
    # print (urlp, file=sys.stderr)
    for c in ensure_list(x['cats']):
        c = cat_name(c)
        if c in symbol_ids:
            # print ("Setting symbol_id:{0} for {1}".format(c, urlp), file=sys.stderr)
            x['symbol_id'] = c
            break
    if 'symbol_id' not in x:
        # try fallback
        if x['url'].startswith(args.catlabel) and args.category in symbol_ids:
            x['symbol_id'] = args.category
        elif args.main in symbol_ids:
            x['symbol_id'] = args.main

print (json.dumps(data, indent=2), file=args.output)
