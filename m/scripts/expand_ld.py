#!/usr/bin/env python3

from pyld import jsonld
import json

import argparse
ap = argparse.ArgumentParser("")
ap.add_argument("input")
ap.add_argument("--indent", type=int, default=2)
args = ap.parse_args()

with open(args.input) as f:
    data = json.load(f)
print (json.dumps(jsonld.expand(data), indent=args.indent))
