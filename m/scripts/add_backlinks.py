#!/usr/bin/env python3

import argparse, json, sys, os
from objectify_ld import objectify_ld
from urllib.parse import unquote as urlunquote
from jinja2 import Template, DictLoader, Environment, FileSystemLoader
from mediawikidump import myurlquote
import html5lib
from xml.etree import ElementTree as ET


# def remove_duplicates (items, key=lambda x: x['url']):
#     seen = {}
#     ret = []
#     for x in items:
#         kx = key(x)
#         if kx not in seen:
#             seen[kx] = True
#             ret.append(x)
#     return ret

# def links (d):
#     # print ("links", d['url'], file=sys.stderr)
#     ret = [dict(linktype="link", **x) for x in d['links'] if type(x) == dict]
#     if 'rev_links' in d:
#         ret += [dict(linktype="rev", **x) for x in d['rev_links']]
#     ret = remove_duplicates(ret)
#     ret.sort(key=lambda x: x['url'])
#     return ret

# def backlinks (d):
#     # print ("links", d['url'], file=sys.stderr)
#     links = [dict(linktype="link", **x) for x in d['links'] if type(x) == dict]
#     links_urls = set([x['url'] for x in links])

#     def is_redirection_to (x, url):
#         return 'redirects_to' in x and x['redirects_to'] == url

#     if 'rev_links' in d:
#         ret = [dict(linktype="rev", **x) for x in d['rev_links']]
#         ret = remove_duplicates(ret)
#         ret = [x for x in ret if x['url'] not in links_urls]
#         # Remove redirections that point to this
#         ret = [x for x in ret if not is_redirection_to(x, d['url'])]
#         ret.sort(key=lambda x: x['url'])
#     else:
#         ret = []
#     return ret

# def make_depth_first_uniq_iterator (*initial_items, key=lambda x: x['url']):
#     """
#     Depth first in the sense that when called, items are pre-checked/marked for having been yielded before actually yielded.
#     In this way, recurrent calls of the iterator would suppress items that *will be* emitted due to a previous call
#     """
#     visited = {}
#     for x in initial_items:
#         visited[key(x)] = True
#     def uniq (items):
#         # pre-index items for depth-first
#         emit = []
#         for i in items:
#             ik = key(i)
#             if ik not in visited:
#                 emit.append(i)
#                 visited[ik] = True
#         for i in emit:
#             yield i
#     return uniq

def make_relpath (basepath):
    def relpath (url):
        # print ("relpath", url, basepath, file=sys.stderr)
        return os.path.relpath(url, os.path.split(basepath)[0])
    return relpath    

def parentchilditerwithindex (elt):
    for parent in elt.iter():
        for i, child in enumerate(parent):
            yield parent, child, i

def et_set_contents (t, elt, newsrc):
    # clear original element
    contents = list(elt)
    for c in contents:
        elt.remove(c)
    elt.text = ""

    newfragment = html5lib.parseFragment(newsrc, namespaceHTMLElements=False)
    for p, c, i in parentchilditerwithindex(t):
        if c == elt:
            elt.text = newfragment.text
            for c in newfragment:
                elt.append(c)

NS_MAIN = "./"
NS_CAT = "Cat%C3%A9gorie/"

def is_internal (d):
    return d['ns']['url'] in (NS_MAIN, NS_CAT)

def internal (links):
    return [x for x in links if is_internal(x)]

def external (links):
    return [x for x in links if not is_internal(x)]


if __name__ == "__main__":
    ap = argparse.ArgumentParser("")
    ap.add_argument("--data", default="m/data/merge.json")
    ap.add_argument("--template", default="m/templates/backlinks.html")
    ap.add_argument("--basepath", default="m/wiki/")
    ap.add_argument('input', nargs="+")
    args = ap.parse_args()

    with open(args.data) as f:
        data = json.load(f)
        index = objectify_ld(data, reverse=True, reverseprefix="rev_")


    tpath, tname = os.path.split(args.template)
    for n in args.input:
        print (n, file=sys.stderr)
        # url_for_file = myurlquote(n.replace(args.basepath, ""))
        url_for_file = "/" + myurlquote(n)
        if url_for_file not in index:
            print ("Warning: {0} not in index (deleted page?)".format(url_for_file), file=sys.stderr)
            continue
        d = index[url_for_file]
        # if 'backlinks' in d:
        #     print ("backlinks.length", len(d['backlinks']), file=sys.stderr)
        # else:
        #     print ("no backlinks", n, file=sys.stderr)
        env = Environment(loader=FileSystemLoader(tpath))
#        env.filters['links'] = links
#        env.filters['backlinks'] = backlinks
#        env.filters['uniq'] = make_depth_first_uniq_iterator(d)
        # print ("url_for_file", n, url_for_file, file=sys.stderr)
        env.filters['relativize'] = make_relpath(url_for_file)
        env.filters['internal'] = internal
        env.filters['external'] = external

        template = env.get_template(tname)
        with open(n) as f:
            t = html5lib.parse(f.read(), namespaceHTMLElements=False)
        backlinks = t.find('.//*[@id="backlinks"]')
        if backlinks != None:
            et_set_contents(t, backlinks, template.render(d=d))
        else:
            print("Warning: No @id=backlinks", file=sys.stderr)
        with open(n, "w") as f:
            print ("<!DOCTYPE html>", file=f)
            print (ET.tostring(t, method="html", encoding="unicode"), file=f)


    # print (input_url)
    # # for n, d in walk_uniq(d, index, maxdepth=1):
    # #     print ("    "*d, urlunquote(n['url']))
    # for d2 in uniq(links(d)):
    #     print ("{0}".format(d2['url']))
    #     for d3 in uniq(links(d2)):
    #         print ("    {0}".format(d3['url']))
