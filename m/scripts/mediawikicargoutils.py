import sys

def get_cargo_item (wiki, table, fields="", where=""):
    # print ("get_cargo_item", table, fields, where, file=sys.stderr)
    result = wiki.get("cargoquery",
        tables=table,
        fields="_pageName=pageName",
        where=where,
        limit=1)
    result = result['cargoquery']
    if len(result) > 0:
        return result[0]['title']
