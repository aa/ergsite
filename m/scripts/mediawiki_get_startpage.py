# http://erg.activearchives.org/mw/index.php/Discussion:Accueil
from mwclient import Site
from urllib.parse import urlparse, unquote as urlunquote
import re, os

def get_startpage(wiki):
    _, name = os.path.split(urlparse(wiki.site.get("base")).path)
    name = urlunquote(name).replace("_", " ")
    name = re.sub("^Discussion:", "", name)
    return name

if __name__ == "__main__":
    import argparse
    ap = argparse.ArgumentParser("Returns the name of the start page")
    ap.add_argument("--wikiprotocol", default="http")
    ap.add_argument("--wikihost", default="erg.activearchives.org")
    ap.add_argument("--wikipath", default="/mw/")
    args = ap.parse_args()
    wiki = Site((args.wikiprotocol, args.wikihost), path=args.wikipath)
    print (get_startpage(wiki))
