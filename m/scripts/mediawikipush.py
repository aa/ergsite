from __future__  import print_function
import argparse, sys, os
from mwclient import Site


def pagename_for_filename (n):
    return os.path.splitext(n)[0].replace("_", " ").decode("utf-8")

def process_file(site, n, summary=None, replace=False):
    _, filename = os.path.split(n)
    with open(n) as f:
        text = f.read().decode("utf-8")
    pagename = pagename_for_filename(n)
    page = site.pages[pagename]
    if page.exists and not replace:
        print (u"Page {0} already exists, skipping".format(pagename).encode('utf-8'))
        return
    if not page.exists:
        print (u"Creating page {0}...".format(pagename).encode("utf-8"), file=sys.stderr)
    else:
        print (u"Updating page {0}...".format(pagename).encode("utf-8"), file=sys.stderr)
    page.save(text, summary=summary)

if __name__ == "__main__":
    ap = argparse.ArgumentParser("")
    ap.add_argument("--wikiprotocol", default="http")
    ap.add_argument("--wikihost", default="modedemploi.erg.be")
    ap.add_argument("--wikipath", default="/wiki/")
    ap.add_argument("--user", default="bot")
    ap.add_argument("--password", default="password")
    ap.add_argument("--pagename", default=None)
    ap.add_argument("--summary", default=None, help="Text to use as edit summary")
    ap.add_argument("--replace", action="store_true", default=False)
    ap.add_argument("input", nargs="+")
    args = ap.parse_args()

    site = Site((args.wikiprotocol, args.wikihost), path=args.wikipath)
    site.login(args.user, args.password)
    for n in args.input:
        process_file(site, n, summary=args.summary, replace=args.replace)
