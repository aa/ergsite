import argparse, json, sys
from objectify_ld import parse_context, as_list, compact_list
from urllib.parse import urljoin


ap = argparse.ArgumentParser("Absolutize URLs .. ie so that they start with /")
ap.add_argument('input')
ap.add_argument("baseurl")
ap.add_argument("--output", type=argparse.FileType('w'), default=sys.stdout)
args = ap.parse_args()

with open(args.input) as f:
    data = json.load(f)

c = data['@context']
g = data['@graph']

cd = parse_context(data)
id_key = cd['@id']

for d in g:
    if id_key in d:
        d[id_key] = urljoin(args.baseurl, d[id_key])
    for key in cd['@id_type']:
        if key in d:
            d[key] = compact_list([urljoin(args.baseurl, x) for x in as_list(d[key])])

print (json.dumps({
        '@context': c,
        '@graph': g
    }, indent=2), file=args.output)
