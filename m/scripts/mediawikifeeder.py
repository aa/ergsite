# from urllib.request import urlopen
from xml.etree import ElementTree as ET 
import feedparser, html5lib
import sys, subprocess, re, datetime
import os
from urllib.parse import urlparse, urljoin, unquote as urlunquote
from urllib.request import urlopen
# import hashlib
from mwclient import Site
from time import mktime
from mediawikiutils import ensure_unique_wiki_pagename, upload_file_to_wiki
import json
from html import unescape as html_unescape
from urllib.error import HTTPError, URLError
from amateurwebscraper import scrape, scrape_url_to_mediawiki, scrape_fragment_to_mediawiki
from mediawikifixbrokenfilelinks import fixlinks, fixurlfilelinks
from utils import wget, download_url_to_file

"""
         _ __    _ ___           _____ 
 _    __(_) /__ (_) _/__ ___ ___/ / _ \
| |/|/ / /  '_// / _/ -_) -_) _  / , _/
|__,__/_/_/\_\/_/_/ \__/\__/\_,_/_/|_| 

"""

THUMBNAIL_EXTS = set(("png", "jpg", "gif", "pdf", "svg", "mp4"))
DATETIME_STRF = "%Y-%m-%d %H:%M:%S"

# def ts2dt (ts):
#     return datetime.datetime.fromtimestamp(mktime(ts))

def rss_feed_skip_item (item, feed):
    # Skip files in mediawiki feeds
    if feed.feed.get("generator", "").lower().startswith("mediawiki"):
        title = item.get('title', '')
        if title.startswith("File:") or title.startswith("Image:") or title.startswith("Fichier:"):
            return True

# URL normalisation... strip mediawiki diff URLs
def rss_feed_normalize_url (url, feed):
    # http://ustensile.be/index.php?title=Html2print&diff=7836&oldid=7829 => http://ustensile.be/index.php/Html2print
    # http://curlybraces.be/wiki/index.php?title=ERG::Code-2017-1&diff=2094&oldid=2089 => http://curlybraces.be/wiki/index.php/ERG::Code-2017-1
    newurl = re.sub(r"index.php\?title=(.+?)&diff=\d+&oldid=\d+$", r"index.php/\1", url)
    if newurl != url:
        print ("[normalize_url] {0} => {1}".format(url, newurl), file=sys.stderr)
        return newurl
    return url

def strip_all_whitespace (text):
    """ Trims a string, normalizing internal runs of (any) whitespace to a single space """
    return re.sub("\s+", " ", text).strip()

def is_vimeo_feed_url (url):
    # https://vimeo.com/251116269
    return re.search("^https?://vimeo.com/\d+$", url) != None


def find_image (src):
    """ find first image in source and REMOVE it, returns Image (or None) and (changed) source"""
    ret = {'image': None}
    def r (m):
        ret['image'] = m.group(2)
    src = re.sub(r"\[\[((?:File|Fichier|Image)\:(.+?))(?:\|.+?)\]\]", r, src, count=1)
    return ret['image'], src

############################
# CLASSES
############################

#  _ __   __ _  __ _  ___ 
# | '_ \ / _` |/ _` |/ _ \
# | |_) | (_| | (_| |  __/
# | .__/ \__,_|\__, |\___|
# |_|          |___/      

class PageWeb (object):
    """ Wrapper for a wiki page that represents a "PageWeb" record -- aka a URL/RSS Item """
    @staticmethod
    def pagename_for_url (url):
        """ Example: """
        p = urlparse(url)
        hostname = p.netloc
        if ":" in hostname:
            hostname = hostname.split(":")[0]
        return os.path.join(hostname, os.path.basename(p.path))

    @staticmethod
    def get_item_date (i):
        """ Example: """
        if 'updated' in i and 'updated_parsed' in i:
            return datetime.datetime.fromtimestamp(mktime(i['updated_parsed']))
        elif 'published' in i and 'published_parsed' in i:
            return datetime.datetime.fromtimestamp(mktime(i['published_parsed']))

    @classmethod
    def from_pagename (cls, wiki, pagename):
        """ Example: """
        result = wiki.get("cargoquery",
            tables="Page_web",
            fields="_pageID=pageID,_pageTitle=pageTitle,_pageName=pageName,_pageNamespace=pageNamespace,URL,Title,Updated,Site,Thumbnail",
            where="_pageName=\"{0}\"".format(pagename),
            limit=1)
        result = result['cargoquery']
        if len(result) > 0:
            result = result[0]['title']
            return cls(wiki, pagename, result['URL'], result['Title'], result['Updated'], result['Site'], result['Thumbnail'])

    @classmethod
    def from_url (cls, wiki, url):
        """ Lookup PageWeb with url, create if necessary based on title """
        result = wiki.get("cargoquery",
            tables="Page_web",
            fields="_pageID=pageID,_pageTitle=pageTitle,_pageName=pageName,_pageNamespace=pageNamespace,URL,Title,Updated,Site,Thumbnail",
            where="URL=\"{0}\"".format(url),
            limit=1)
        result = result['cargoquery']
        if len(result) > 0:
            result = result[0]['title']
            return cls(wiki, result['pageName'], result['URL'], result['Title'], result['Updated'], result['Site'], result['Thumbnail'])
        else:
            return cls(wiki, None, url, None)
        # elif title:
        #     # create a new wiki page object for the URL
        #     # nb: ensure_unique_wiki_pagename is called here because this means that
        #     # no "Page web" already exists for this URL,
        #     # we ensure unique, so that in the case of recurring titles, say "Untitled"
        #     # you would then use "Untitled (2)"
        #     # Ideally this would be a transaction whereby the wiki page would be immediately created
        #     # and commit the related URL ... but it isn't (assumption is that the subsequent code
        #     # takes care of that and that no overlapping updates occur)
        #     #
        #     pagename = ensure_unique_wiki_pagename(wiki, "Page web:"+title)
        #     page = wiki.pages.get(pagename)
        #     return cls(wiki, page, url, title)

    def __init__ (self, wiki, pagename, url, title, updated='', site='', thumbnail=''):
        """ title is suggested title when page needs to be created, if page with url already exists, title will be reset """
        self.wiki = wiki
        self.pagename = pagename
        self.page = None 
        self.url = url
        self.title = title
        self.updated = updated
        self.site = site
        self.thumbnail = thumbnail

    def ensure_page (self):
        if self.page == None:
            if self.pagename == None:
                self.pagename = "Page web:" + self.title
            self.page = self.wiki.pages.get(self.pagename)

    def scrape_rss (self, item, phpuploader=None, user=None):
        """ Create wiki page based on the contents of an rss feed item """
        # print ("[process_feed_item]item.description:\n{0}\n".format(item.description), file=sys.stderr)
        src = scrape_fragment_to_mediawiki(item.description, self.url)
        # if not self.title:
        self.title = item.title

        # UPLOAD FILES TO WIKI
        src = fixurlfilelinks(src, self.wiki, phpuploader=phpuploader, user=user)
        image, _ = find_image(src)
        if image:
            self.thumbnail = image
        # print("[process_feed_item].title", self.title, file=sys.stderr)
        # print("[process_feed_item].thumbnail", self.thumbnail, file=sys.stderr)
        # print("[process_feed_item].src", src, file=sys.stderr)
        self.save_scrape(src)
        # fixlinks(self.pagename, self.wiki, phpuploader=phpuploader, user=user)

    def scrape_html (self, site=None, item_title=None, title_selector=None, remove_title=True, content_selector=None, remove_selector=None, phpuploader=None, user=None):
        """ Attempt to scrape contents based on HTML itself (as opposed to a feed item) """
        # if selectors are None ... attempt to load from Site when present
        if content_selector == None and self.site:
            if site == None:
                site = SiteWeb(self.wiki, self.site)
            content_selector = site.content_selector
            title_selector = site.title_selector
            remove_selector = site.exclude_selector
        # get site object to determine selectors to use (if any)
        scrapetitle, src = scrape_url_to_mediawiki(self.url, title_selector=title_selector, remove_title=remove_title, content_selector=content_selector, remove_selector=remove_selector)
        self.title = scrapetitle or item_title # allow item_title as backup
        if self.updated==None:
            self.updated = datetime.datetime.now().strftime(DATETIME_STRF)
        # print ("TITLE", scrapetitle, file=sys.stderr)
        # print ("SRC", newsrc, file=sys.stderr)
        src = fixurlfilelinks(src, self.wiki, phpuploader=phpuploader, user=user)
        image, _ = find_image(src)
        if image:
            self.thumbnail = image
        self.save_scrape(src)
        # fixlinks(self.pagename, self.wiki, phpuploader=phpimportimages, user=user)

    def save_scrape (self, newsrc):
        new_contents = """{{{{Page web
|URL={url}
|Title={title}
|Updated={updated}
|Site={site}
|Thumbnail={thumbnail}
}}}}\n""".format(
    url=self.url,
    title=self.title,
    updated=self.updated,
    site=self.site,
    thumbnail=self.thumbnail
    )
        new_contents += newsrc
        self.ensure_page()
        self.page.save(new_contents, description="scrape")
        # need to catch + loop on:
        # mwclient.errors.APIError: ('editconflict', 'Edit conflict.', 'See http://erg.activearchives.org/mw/api.php for API usage. Subscribe to the mediawiki-api-announce mailing list at &lt;https://lists.wikimedia.org/mailman/listinfo/mediawiki-api-announce&gt; for notice of API deprecations and breaking changes.')



#      _ _       
#  ___(_) |_ ___ 
# / __| | __/ _ \
# \__ \ | ||  __/
# |___/_|\__\___|

class SiteWeb (object):
    @staticmethod
    def parse_dt (val):
        try:
            return datetime.datetime.strptime(val, DATETIME_STRF)
        except ValueError:
            return None

    @staticmethod
    def unparse_dt (dt):
        return dt.strftime(DATETIME_STRF)

    @classmethod
    def get_all (cls, wiki):
        result = wiki.get("cargoquery",
            tables="Site_web",
            fields="_pageName=pagename,Site_url=site_url,Feed_url=feed_url,Last_updated=last_updated,Scraper,Content_selector,Title_selector,Exclude_selector,Description",
            where="feed_url<>''",
            order_by="pagename")
        # print (json.dumps(result, indent=2))
        # html_unescape is here cause URLs with & seem to get escaped!
        return [cls(
                wiki=wiki,
                pagename=r['title']['pagename'],
                site_url=html_unescape(r['title']['site_url']),
                feed_url=html_unescape(r['title']['feed_url']),
                last_updated=cls.parse_dt(r['title']['last_updated']),
                scraper=r['title']['Scraper'],
                content_selector=r['title']['Content selector'],
                title_selector=r['title']['Title selector'],
                exclude_selector=r['title']['Exclude selector'],
                description=r['title']['Description']
                ) for r in result['cargoquery']]

    def __init__ (self, wiki, pagename, site_url=None, feed_url=None, last_updated=None, scraper=None, content_selector=None, title_selector=None, exclude_selector=None, description=None):
        self.wiki = wiki
        self.pagename = pagename
        if feed_url:
            self.site_url = site_url
            self.feed_url = feed_url
            self.last_updated = last_updated
            self.scraper = scraper
            self.content_selector = content_selector
            self.title_selector = title_selector
            self.exclude_selector = exclude_selector
            self.description = description
        else:
            self.site_url = None
            self.feed_url = None
            self.last_updated = None
            self.init()

    def init (self):
        """ set site_url, feed_url and last_updated from wiki data, if present """ 
        result = self.wiki.get("cargoquery",
            tables="Site_web",
            fields="_pageID=pageID,_pageTitle=pageTitle,_pageName=pageName,_pageNamespace=pageNamespace,Site_url,Feed_url,Last_updated,Scraper,Content_selector,Title_selector,Exclude_selector,Description",
            where="_pageName=\"{0}\"".format(self.pagename),
            limit=1)
        # NB: pageName is with the namespace (when not Main), pageTitle is without
        result = result['cargoquery']
        # print ("got result", result, file=sys.stderr)
        if len(result) > 0:
            result = result[0]['title']
            self.site_url = html_unescape(result['Site url'])
            self.feed_url = html_unescape(result['Feed url'])
            self.scraper = result.get('Scraper')
            self.content_selector = result.get('Content selector')
            self.title_selector = result.get('Title selector')
            self.exclude_selector = result.get('Exclude selector')
            self.description = result.get('Description')
            if result['Last updated']:
                self.last_updated = self.parse_dt(result['Last updated'])

    def template_text (self):
        return """{{{{Site web
|Site_url={site_url}
|Feed_url={feed_url}
|Last_updated={updated}
|Scraper={scraper}
|Content_selector={content_selector}
|Title_selector={title_selector}
|Exclude_selector={exclude_selector}
|Description={description}
}}}}""".format(
    site_url=self.site_url,
    feed_url = self.feed_url,
    updated=self.last_updated.strftime(DATETIME_STRF),
    scraper=self.scraper,
    content_selector=self.content_selector,
    title_selector=self.title_selector,
    exclude_selector=self.exclude_selector,
    description=self.description)

    def save (self):
        """
        TODO!!! template updater should allow selective replacement of specific values,
        leaving other unknown values (avoid the above template!!!)
        THIS will break in future if the template gets new values
        """
        page = self.wiki.pages[self.pagename]
        page_text = page.text()
        temppat = re.compile(r"\{\{Site web\s*\|.+?\}\}", flags=re.DOTALL)
        m = temppat.search(page_text)
        if m:
            print ("[Site web].save Replacing template", file=sys.stderr)
            # Replace existing template with new data
            page.save(temppat.sub(lambda x: self.template_text(), page_text))
        else:
            print ("[Site web].save ! Template not found. Skipping save", file=sys.stderr)
            # page.save(self.template_text())

    def process_feed(self, cachedir, limit=None, skip=None, phpuploader=None, user=None, force=False, pretend=False):
        print ("[process_feed]: feed_url {0}".format(self.feed_url), file=sys.stderr)
        # print ("[scraper]", self.scraper, file=sys.stderr)
        # print ("[content_selector]", self.content_selector, file=sys.stderr)
        # print ("[title_selector]", self.title_selector, file=sys.stderr)
        # print ("[exclude_selector]", self.exclude_selector, file=sys.stderr)

        # adding urlopen as I otherwise encountered malformed XML issues (feedparser fail silently)
        try:
            feed = feedparser.parse(urlopen(self.feed_url))
            # ensure in reverse chronological order (probably redundant but important for the processing)
            # print ("[process_feed]: {0} items".format(len(feed.entries)), file=sys.stderr)
            feed.entries.sort(key=lambda x: PageWeb.get_item_date(x), reverse=True)
            # print (feed.entries[0].published_parsed, "to", feed.entries[-1].published_parsed)

            count = 0
            # Process in CHRONOLOGICAL order... skipping elements that OLDER than / equal to feed's last updated timestamp
            all_entries = reversed(feed.entries)
            all_entries = [x for x in all_entries if not rss_feed_skip_item(x, feed)]

            skipped = 0

            # Filter list when last updated is present (and not using force)
            if not force and self.last_updated:
                use_all_entries = []
                # print ("processing feed, last_updated {0}".format(self.last_updated.strftime(DATETIME_STRF)), file=sys.stderr)
                for item in all_entries:
                    item_dt = PageWeb.get_item_date(item)
                    if item_dt <= self.last_updated:
                        # print ("Skipping older item {0}".format(item.title), file=sys.stderr)
                        skipped += 1
                    else:
                        use_all_entries.append(item)
                all_entries = use_all_entries
            # else:
            #     print ("processing feed, last_updated is not set", file=sys.stderr)
            if skipped > 0 and len(all_entries) == 0:
                print ("No new items since feed last updated", file=sys.stderr)

            all_entries = list(all_entries)
            # remove_duplicates
            all_entries_nodups = []
            seen = set()
            for item in all_entries:
                item_url = rss_feed_normalize_url(item.link, feed)
                if not item_url in seen:
                    all_entries_nodups.append(item)
                    seen.add(item_url)
            all_entries = all_entries_nodups
            for i, item in enumerate(all_entries):
                if skip != None and i<skip:
                    continue
                item_url = rss_feed_normalize_url(item.link, feed)
                print ("[process_feed] {0}/{1}: {2} ( {3} )".format(i+1,len(all_entries), item.title, item_url), file=sys.stderr)
                item_dt = PageWeb.get_item_date(item)
                item_updated = item_dt.strftime(DATETIME_STRF)
                # pageweb = PageWeb(self.wiki, url=normalize_url(item.link), title=item.title)
                # edge case... if scraping from HTML... should allow title here!

                pageweb = PageWeb.from_url(self.wiki, url=item_url)
                pageweb.site = self.pagename
                pageweb.updated = item_updated
                if self.scraper == "RSS":
                    # pageweb = PageWeb.from_url(self.wiki, url=item_url, title=item.title)
                    print ("[scrape_rss]", file=sys.stderr)
                    pageweb.scrape_rss(item, phpuploader=phpuploader, user=user)
                else:
                    print ("[scrape_html]", file=sys.stderr)
                    pageweb.scrape_html(item_title=item.title, site=self, phpuploader=phpuploader, user=user)

                    # print ("Scraping HTML", file=sys.stderr)
                    # scrapetitle, newsrc = scrape_url_to_mediawiki(item_url, title_selector=self.title_selector, remove_title=True, content_selector=self.content_selector, remove_selector=self.exclude_selector)
                    # pageweb = PageWeb.from_url(self.wiki, url=item_url, title=scrapetitle or item.title)
                    # pageweb.updated = SiteWeb.unparse_dt(item_dt)
                    # # pageweb could be new... in which case we need to set the site
                    # pageweb.site = self.pagename
                    # pageweb.save_scrape(newsrc)
                    # # fixlinks
                    # # print ("Fixing image links", file=sys.stderr)

                    # fixlinks(pageweb.page.name, self.wiki, phpuploader=phpuploader, user=user)

                if self.last_updated == None or item_dt > self.last_updated:
                    self.last_updated = item_dt
                    if not pretend:
                        self.save()
                count += 1
                if limit and count>=limit:
                    break
        except URLError as e:
            print ("WARNING [process_feed] Skipping feed {0}, URL Error: {1}".format(self.feed_url, e.reason), file=sys.stderr)
        except HTTPError as e:
            print ("WARNING [process_feed] Skipping feed {0}, HTTP Error {1}".format(self.feed_url, e.code), file=sys.stderr)

def process_feeds(wikiprotocol, wikihost, wikipath, user, password, source, limit, skip, force, pretend, feedlimit, cachedir, phpimportimages):
    # wiki
    wiki = Site((wikiprotocol, wikihost), path=wikipath)
    if user:
        wiki.login(user, password)
    if source:
        print ("Processing {0}".format(source), file=sys.stderr)
        flux = SiteWeb(wiki, source)
        flux.process_feed(cachedir, limit=limit, skip=skip, phpuploader=phpimportimages, user=user, force=force, pretend=pretend)
    else:
        print ("Processing all feeds", file=sys.stderr)
        for i, f in enumerate(SiteWeb.get_all(wiki)):
            print ("[{0}] {1}".format(i, f.pagename), file=sys.stderr)
            print ("feed_url: {0}".format(f.feed_url), file=sys.stderr)
            if f.last_updated:
                print ("last_updated: {0}".format(f.last_updated), file=sys.stderr)
            else:
                print ("last_updated: ---", file=sys.stderr)
            if not pretend:
                f.process_feed(cachedir, limit=limit, skip=skip, phpuploader=phpimportimages, user=user, force=force, pretend=pretend)
            print (file=sys.stderr)
            if feedlimit and i+1>=feedlimit:
                break

def cli_scrape (wikiprotocol, wikihost, wikipath, user, password, page, site, title_selector, remove_title, content_selector, remove_selector, phpimportimages):
    wiki = Site((wikiprotocol, wikihost), path=wikipath)
    if user:
        wiki.login(user, password)

    if page:
        page = PageWeb.from_pagename(wiki, page)
        page.scrape(title_selector=title_selector, remove_title=remove_title, content_selector=content_selector, remove_selector=remove_selector, phpimportimages=phpimportimages)
    elif site:
        # site = wiki.pages(site)
        result = wiki.get("cargoquery",
            tables="Page_web",
            fields="_pageID=pageID,_pageTitle=pageTitle,_pageName=pageName,_pageNamespace=pageNamespace,URL,Title,Updated,Site",
            where="site=\"{0}\"".format(site), limit=100)
        result = result['cargoquery']
        if len(result) > 0:
            for row in result:
                item = row['title']
                print (item['pageTitle'], file=sys.stderr)
                page = PageWeb(wiki, item['pageName'], item['URL'], item['Title'], item['Updated'], item['Site'])
                page.scrape(title_selector=title_selector, remove_title=remove_title, content_selector=content_selector, remove_selector=remove_selector, phpimportimages=phpimportimages)
    else:
        print ("Scrape all pages with no update timestamp", file=sys.stderr)
        result = wiki.get("cargoquery",
            tables="Page_web",
            fields="_pageID=pageID,_pageTitle=pageTitle,_pageName=pageName,_pageNamespace=pageNamespace,URL,Title,Updated,Site,Thumbnail",
            where="Updated IS NULL")
        # print (result)
        result = result['cargoquery']
        if len(result) > 0:
            for rw in result:
                r = rw['title']
                print ("{0}".format(r['pageName']), file=sys.stderr)
                pageweb = PageWeb(wiki, r['pageName'], r['URL'], r['Title'], updated=None, site=r['Site'], thumbnail=r['Thumbnail'])
                pageweb.scrape(phpimportimages=phpimportimages, user=user)

if __name__ == "__main__":
    from argparse import ArgumentParser
    ap = ArgumentParser("")
    # sub-commands
    sub = ap.add_subparsers(help='sub-command')
    sub.required = True

    # shared args
    ap.add_argument("--wikiprotocol", default="http")
    # ap.add_argument("--wikihost", default="localhost") #"erg.activearchives.org"
    ap.add_argument("--wikihost", default="erg.activearchives.org")
    ap.add_argument("--wikipath", default="/mw/")
    ap.add_argument("--user", default=None)
    ap.add_argument("--password", default=None)
    ap.add_argument("--phpimportimages", default=None, help="optional: path to mw/maintenance/importImages.php. Default is to upload via the API")

    # feed
    apfeed = sub.add_parser('feed', help="scrape a one or all sites using associated rss feeds")
    apfeed.add_argument("source", nargs="?", help="pagename of Site web/Sites satellites")
    # ap.add_argument("--feed", default="http://ergoteradio.tumblr.com/rss")
    # ap.add_argument("--download", action="store_true", default=False)
    apfeed.add_argument("--limit", type=int, default=None)
    apfeed.add_argument("--skip", type=int, default=None)
    apfeed.add_argument("--force", default=False, action="store_true")
    apfeed.add_argument("--pretend", default=False, action="store_true")
    apfeed.add_argument("--feedlimit", type=int, default=None)
    apfeed.add_argument("--cachedir", default="cache", help="directory where files are temporarily downloaded")
    apfeed.set_defaults(func=process_feeds)

    # scrape
    apscrape = sub.add_parser('scrape', help="scrape a single page")
    apscrape.add_argument("--page", default=None)
    apscrape.add_argument("--site", default=None)
    # apscrape.add_argument("--cachedir", default="cache", help="directory where files are temporarily downloaded")
    apscrape.add_argument('--title-selector', default=None, help="CSS selector to extract title content")
    apscrape.add_argument('--remove-title', default=False, action="store_true", help="remove title (if specified with a selector)")
    apscrape.add_argument('--content-selector', default=None, help="CSS selector to extract main content")
    apscrape.add_argument('--remove-selector', default=None, help="CSS selector of content to remove")
    apscrape.set_defaults(func=cli_scrape)

    # GO!
    args= ap.parse_args()
    params = vars(args).copy()
    del params['func']
    res = args.func(**params)
    print (res)

