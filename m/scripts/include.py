#!/usr/bin/python3

import argparse, sys, os
"""
Resolves paths relative to input path (if not stdin).
This base path can be overridden with --cwd. 

"""

ap = argparse.ArgumentParser("")
ap.add_argument("--include", default="#include")
ap.add_argument("--cwd", default=None)
ap.add_argument("--list", default=False, action="store_true")
ap.add_argument('infile', nargs='?', type=argparse.FileType('r'), default=sys.stdin)
ap.add_argument('outfile', nargs='?', type=argparse.FileType('w'), default=sys.stdout)
args = ap.parse_args()

path = "."
if args.infile.name != "<stdin>":
    path, _ = os.path.split(args.infile.name)
if args.cwd:
    path = args.cwd

for line in args.infile:
    if line.startswith(args.include):
        filename = line.split()[1]
        filename = os.path.join(path, filename)
        if args.list:
            if os.path.exists(filename):
                print (filename, file=args.outfile)
        else:
            try:
                with open(filename) as f:
                    for line in f:
                        args.outfile.write(line)
                    args.outfile.write("\n")
            except IOError as e:
                print (e, file=args.outfile)
    else:
        if not args.list:
            args.outfile.write(line)
