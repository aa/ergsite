from __future__  import print_function
import argparse, sys, os
from mwclient import Site
import datetime
import json
from urllib.parse import quote as urlquote
from mediawikidump import filename_for_page_title, myurlquote
from html import unescape as htmlunescape


# def myurlquote (s):
#     """ removing colon! """
#     return urlquote(s, safe="/_()")

# def page_url_from_name (site, pagename):
#     """ key function: returns a URL for a given page
    
#     """
#     # print ("[page_url]", page.name, file=sys.stderr)
#     # page = site.pages[pagename]
#     base = os.path.split(site.site['base'])[0]
#     uret = os.path.join(base, myurlquote(pagename.replace(" ", "_")))
#     return uret

def get_date (d, name):
    v = d.get(name)
    if v:
        p = int(d.get(name+"__precision"))
        if p == 0:
            return datetime.datetime.strptime(v, "%Y-%m-%d %H:%M:%S").isoformat()
        else:
            return datetime.datetime.strptime(v, "%Y-%m-%d").isoformat()
    return v

def parse_date (s, fmt="%Y-%m-%d %H:%M:%S"):
    return datetime.datetime.strptime(s, fmt)

def get_page_webs (wiki):
    # print ("get_cargo_item", table, fields, where, file=sys.stderr)
    results = []
    while True:
        result = wiki.get("cargoquery",
            tables="Page_web",
            fields="_pageName=pageName,Updated",
            limit=500,
            offset = len(results),
            order_by="Updated")
        # print ("got {0} results".format(len(result['cargoquery'])))
        if len(result['cargoquery']) == 0:
            break
        for x in result['cargoquery']:
            x = x['title']
            d = {}
            # NB: cargo values get htmlescaped ... so like & => &amp; and " => &quot;
            pageName = htmlunescape(x['pageName'])
            d['url'] = myurlquote(filename_for_page_title(pageName+".html", wiki))
            if pageName != x['pageName']:
                print ("pageName: \"{0}\" -> \"{1}\" -> {2} ".format(x['pageName'], pageName, d['url']), file=sys.stderr)
            # print ("url", d['url'], file=sys.stderr)
            d['updated'] = get_date(x, 'Updated')
            results.append(d)
    return results

def get_news (wiki):
    # print ("get_cargo_item", table, fields, where, file=sys.stderr)
    results = []
    while True:
        result = wiki.get("cargoquery",
            tables="News",
            fields="_pageName=pageName,Start,End,Published",
            limit=500,
            offset = len(results),
            order_by="Published")
        # print ("got {0} results".format(len(result['cargoquery'])))
        if len(result['cargoquery']) == 0:
            break
        for x in result['cargoquery']:
            x = x['title']
            d = {}
            pageName = htmlunescape(x['pageName'])
            d['url'] = myurlquote(filename_for_page_title(pageName+".html", wiki))
            if pageName != x['pageName']:
                print ("pageName: \"{0}\" -> \"{1}\" -> {2} ".format(x['pageName'], pageName, d['url']), file=sys.stderr)
            d['published'] = get_date(x, 'Published')
            if x['Start']:
                d['start'] = get_date(x, 'Start')
            if x['End']:
                d['end'] = get_date(x, 'End')
            results.append(d)
    return results

if __name__ == "__main__":
    ap = argparse.ArgumentParser("dump json-ld for all dated items in the wiki (Page web, Actualities)")
    ap.add_argument("--wikiprotocol", default="http")
    ap.add_argument("--wikihost", default="erg.activearchives.org")
    ap.add_argument("--wikipath", default="/mw/")
    # ap.add_argument("--user", default="bot")
    # ap.add_argument("--password", default="password")
    args = ap.parse_args()

    wiki = Site((args.wikiprotocol, args.wikihost), path=args.wikipath)
    # wiki.login(args.user, args.password)
    data = {}
    data['@context'] = {
        "url": "@id",
        "@vocab": "http://mediawiki.org/terms/",
        "@base": "http://erg.activearchives.org/m/wiki/"
    }

    data['@graph'] = g = []
    g.extend(get_page_webs (wiki))
    g.extend(get_news (wiki))      
    print (json.dumps(data, indent=2))
