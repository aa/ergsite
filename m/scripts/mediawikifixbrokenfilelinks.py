from mwclient import Site
import argparse, re, sys
from mediawikiutils import upload_url_to_wiki
from datetime import datetime
from mwclient.errors import EditError

def filetest (p):
    """ Filter evil 1x1 pixel images """
    im = Image.open(p)
    w, h = im.size
    if (w <= 1 and h <= 1):
        return False
    return True

def ensure_image_option (opts, newopts="thumb|none"):
    if opts == None:
        opts = ""
    opts = opts.split("|")
    opts = [x for x in opts if x]
    for nop in newopts.split("|"):
        if nop not in opts:
            opts.append(nop)
    return "|" + "|".join(opts)

def fixurlfilelinks (src, wiki, timestamp=None, phpuploader=None, user=None):
    if timestamp == None:
        timestamp = datetime.now().strftime("%Y/%m/%d")
    def linksub (m):
        # print ("[fixurlfilelinks].linksub", m.group(1), file=sys.stderr)
        url = m.group(1)
        page = upload_url_to_wiki(url, wiki, "{{{{Provenance web|URL={0}|Date={1}]}}}}".format(url, timestamp), phpuploader=phpuploader, user=user)
        if page == None:
            print ("URL failed {0} leaving link as is: {1}".format(m.group(1), m.group(0)), file=sys.stderr)
            return m.group(0)
        else:
            return "[[{0}{1}]]".format(page.name, ensure_image_option(m.group(2), "thumb|none"))
    return re.sub(r"\[\[(?:File|Fichier|Image)\:(https?\://.+?)(\|.*?)?\]\]", linksub, src, flags=re.I)

def fixlinks (pagename, wiki, phpuploader=None, user=None):
    print ("fixlinks {0}".format(pagename), file=sys.stderr)
    page = wiki.pages[pagename]
    tries = 0
    while tries < 3:
        try:
            src = page.text()
            # print ("src", src, file=sys.stderr)
            newsrc = fixurlfilelinks(src, wiki, phpuploader=phpuploader, user=user)
            if newsrc != src:
                # print ("[fixlinks] {0} saving updated page...".format(page.name), file=sys.stderr)
                page.save(newsrc, description="fix broken file links bot")
                return newsrc
            break
        except EditError as e:
            print ("EditError {0}".format(e), file=sys.stderr)
            tries += 1

if __name__ == "__main__":
    ap = argparse.ArgumentParser("")
    ap.add_argument("--wikiprotocol", default="http")
    ap.add_argument("--wikihost", default="erg.activearchives.org")
    ap.add_argument("--wikipath", default="/mw/")
    # ap.add_argument("--wikihost", default="localhost")
    # ap.add_argument("--wikipath", default="/mw/")
    # ap.add_argument("--user", default=None)
    # ap.add_argument("--password", default=None)
    ap.add_argument("--user", default="Michael Murtaugh@bot")
    ap.add_argument("--password", default="3ap1p11k33snual7hr2m9nldvg1jo0si")
    ap.add_argument("--page", default=None, help="fix just the page with the given name (otherwise default is to use Category:Pages with broken file links)")
    ap.add_argument("--phpimportimages", default=None, help="optional: path to mw/maintenance/importImages.php. Default is to upload via the API")
    ap.add_argument("--catname", default="Pages avec des liens de fichiers brisés")
    # ap.add_argument("--catname", default="Pages with broken file links")
    
    args = ap.parse_args()

    wiki = Site((args.wikiprotocol, args.wikihost), path=args.wikipath)
    if args.user:
        wiki.login(args.user, args.password)

    if args.page:
        fixlinks(args.page, wiki, phpuploader=args.phpimportimages, user=args.user)
    else:
        for page in wiki.categories[args.catname]:
            print (page.name, file=sys.stderr)
            fixlinks(page.name, wiki, phpuploader=args.phpimportimages, user=args.user)
