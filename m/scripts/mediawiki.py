# -*- coding: utf-8 -*-

from mediawikisettings import *
from mwclient import Site
from mwclient.page import Page
import re, os, urllib, unicodedata


SLASH = u"\u2044" # "%2F"

def page_url (site, page):
    base = os.path.split(site.site['base'])[0]
    uret = os.path.join(base.encode("utf-8"), urllib.quote(page.normalize_title(page.name).encode("utf-8")))
    assert type(uret) == str
    return uret

def url_to_title (site, url):
    if type(url) == unicode:
        url = url.encode("utf-8")
    pat = site.site['articlepath'].decode("utf-8").replace("$1", "THEPAGETITLE")
    pat = re.escape(pat).replace("THEPAGETITLE", "(.+)")
    m = re.search(pat, url)
    if m:
        t = m.group(1)
        t = urllib.unquote(t)
        t = t.replace(b"_", b" ")
        return t.decode("utf-8")

def strip_namespace (n):
    parts = n.split(":", 1)
    if len(parts) == 2:
        return parts[1]
    return n

def make_title_link_regex (site, compile=True):
    pnames = []
    links = {}
    for p in site.allpages():
        pagelink = page_url(site, p)
        key = unicodedata.normalize("NFC", p.name).encode("utf-8")
        links[p.name] = pagelink
        links[key] = pagelink
        links[p.name.lower()] = pagelink
        pnames.append(p.name)
        # print type(p.name), p.name
    pat = b"|".join(re.escape(unicodedata.normalize("NFC", x).encode("utf-8")) for x in pnames)
    if compile:
        return (re.compile(pat, re.I), links)
    else:
        return (pat, links)

def link_titles (text, pat=None, links=None, target=None, site=None):
    if pat == None:
        pat, links = make_title_link_regex(site)

    if type(target) == unicode:
        target = target.encode("utf-8")

    def repl(m):
        text = m.group(0)
        if text in links:
            link = links[text]
            if target:
                return b"""<a href="{0}">{1}</a>""".format(link, text)
            else:
                return b"""<a href="{0}" target="{1}">{2}</a>""".format(link, target, text)

        else:
            return b"???"

    if type(text) == unicode:
        text = unicodedata.normalize("NFC", text)
        text = text.encode("utf-8")
    ret = re.sub(pat, repl, text)
    return ret.decode("utf-8")

def text_to_links (text, pat=None, links=None, site=None, titles=False):
    """ simpler version of link_titles, simply returns a list of page titles """
    if pat == None:
        pat, links = make_title_link_regex(site)
    if type(text) == unicode:
        text = unicodedata.normalize("NFC", text)
        text = text.encode("utf-8")

    matches = re.finditer(pat, text)
    page_titles = [m.group(0) for m in matches]
    page_titles = [links[x] for x in page_titles if x in links]
    if titles:
        page_titles = [url_to_title(site, x) for x in page_titles]
    return page_titles


def link_titles_wiki (text):
    return link_titles(text, target="wiki")

def filename_for_page_title (t, path):
    """ replace / with % 2F ... assumes unicode input returns bytes """
    # fname = t.replace(u" ", u"_").replace(u"/", u"_%2F").encode("utf-8")
    # if t.startswith(u"Catégorie:"):
    #     t = t[10:]
    #     fname = t.replace(u" ", u"_").replace(u"/", SLASH).encode("utf-8")
    #     return os.path.join(path, CATPATH, fname)
    # else:
    fname = t.replace(u" ", u"_").replace(u"/", SLASH).encode("utf-8")
    return os.path.join(path, fname)

def filename_for_page (p, path):
    """ returns bytes """
    return os.path.join(path, p.normalize_title(p.name).replace("/", SLASH).encode("utf-8"))

def local_url (p):
    if isinstance(p, Page): 
        return urllib.quote(filename_for_page(p, "")+b".html")
    else:
        return urllib.quote(filename_for_page_title(p, "")+b".html")

# deprecate this?!
class TitleLinker (object):
    def __init__(self, site):
        self.pat, self.links = make_title_link_regex(site)
    def link (self, text, target="wiki"):
        return link_titles(text, self.pat, self.links, target=target)

# template rendering as a jinja filter
class MediaWikiFilters (object):
    def __init__(self, site, linktitles=False):
        self.site = site
        # setup the regex used by linktitles
        if linktitles:
            self.pat, self.links = make_title_link_regex(site)

    def linktitles (self, text, target="wiki"):
        return link_titles(text, self.pat, self.links, target=target)

    def url2title (self, url):
        return url_to_title(self.site, url)

    def expandtemplates (self, title):
        ret = self.site.parse(text=u"{{"+title+u"}}")
        return ret['text']['*']
        # return (text, self.pat, self.links, target=target)

    def stripnamespace(self, title):
        return strip_namespace(title)

    def local_url (self, p):
        return local_url(p)

    def text_to_links (self, text):
        return text_to_links(text, self.pat, self.links, self.site)

# https://stackoverflow.com/questions/23477472/how-can-i-combine-a-character-followed-by-a-combining-accent-into-a-single-cha

if __name__ == "__main__":
    pass
    # text = u"Livres parlés 2017 : 06 Pierre-Philippe Duchâtelet Vimeo / erg’s videos 2017-09-15 22:4"
    # print link_titles(text)

    # stext = u"Pierre-Philippe Duchâtelet"
    # rtext = u"Pierre-Philippe Duchâtelet"

    # print stext == rtext

    # pat, links = make_title_link_regex(False)
    # print pat

    # # text = text.encode("utf-8")
    # # stext = stext.encode("utf-8")
    # # stext = re.escape(stext)
    # # print stext, type(stext)

    # # # spat = ur"Pierre\-Philippe\ Duch\âtelet"
    # m = re.search(re.escape(stext.encode("utf-8")), text.encode("utf-8"), re.I)
    # # # m = re.search(ur"Pierre\-Philippe\ Duch\âtelet", text, re.I)
    # print m
    # if m:
    #     print m.group(0)