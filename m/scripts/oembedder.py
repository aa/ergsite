import oembed, json, sys
# from urlparse import urlparse, urlunparse
try:
    from urlparse import urlparse, urlunparse
except ImportError:
    from urllib.parse import urlparse, urlunparse
from urllib.error import HTTPError, URLError


URLS = [
    ('https://www.flickr.com/services/oembed', ['https://*.flickr.com/*']),
    ('https://www.mixcloud.com/oembed/', ['https://www.mixcloud.com/*/*/']),
    ('https://vimeo.com/api/oembed.json', ["https://vimeo.com/*", "https://vimeo.com/album/*/video/*", "https://vimeo.com/channels/*/*", "https://vimeo.com/groups/*/videos/*", "https://vimeo.com/ondemand/*/*", "https://player.vimeo.com/video/*"]),
    ('https://www.youtube.com/oembed', []),
    ('https://www.dailymotion.com/services/oembed', ['https://www.dailymotion.com/video/*']),
    ('https://www.facebook.com/plugins/video/oembed.json', ['https://www.facebook.com/video.php', 'https://www.facebook.com/*/videos/*']),
    ('https://soundcloud.com/oembed', ['https://soundcloud.com/*'])
]

def get_oembed (url, normalizeProtocol="https"):
    consumer = oembed.OEmbedConsumer()
    for api, urls in URLS:
        endpoint = oembed.OEmbedEndpoint(api, urls)
        consumer.addEndpoint(endpoint)
    if normalizeProtocol:
        scheme, netloc, path, params, query, fragment = urlparse(url)
        url = urlunparse((normalizeProtocol, netloc, path, params, query, fragment))
    try:
        return consumer.embed(url).getData()
    except oembed.OEmbedNoEndpoint:
        return
    except URLError as e:
        print ("[get_oembed] URLError {0}: {1}".format(url, e), file=sys.stderr)
    except HTTPError as e:
        print ("[get_oembed] HTTPError {0}: {1}".format(url, e), file=sys.stderr)

if __name__ == "__main__":
    import argparse
    ap = argparse.ArgumentParser("")
    ap.add_argument("--url", default="http://www.flickr.com/photos/wizardbt/2584979382/")
    args = ap.parse_args()

    response = get_oembed(args.url)
    print(json.dumps(response, indent=2))



