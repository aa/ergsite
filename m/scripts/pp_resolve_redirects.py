import argparse, json, sys

ap = argparse.ArgumentParser("Resolves redirections, outputting only preferred nodes, and rewriting links, cats")
ap.add_argument('input')
args = ap.parse_args()

with open(args.input) as f:
    data = json.load(f)

c = data['@context']
g = data['@graph']

redirects_by_url = {}
nodes_by_url = {}

new_graph = []

for d in g:
    nodes_by_url[d['url']] = d
    if 'redirects_to' in d:
        redirects_by_url[d['url']] = d['redirects_to']
    elif 'links' in d:
        new_graph.append(d)

def ensure_list (x):
    if type(x) == list:
        return x
    return [x]

# check for any necessary link rewrites

def resolve_redirect (url):
    while url in redirects_by_url:
        url = redirects_by_url[url]
    return url

def rewrite (node, key):
    if key in d:        
        newvals = []
        for linkurl in ensure_list(d.get(key)):
            if linkurl in nodes_by_url:
                if linkurl in redirects_by_url:
                    rl = resolve_redirect(linkurl)
                    print ('In {0}:\n  re "{1}"\n  to:"{2}"'.format(d['url'], linkurl, rl), file=sys.stderr)
                    newvals.append(rl)
                else:
                    newvals.append(linkurl)
            else:
                 print ("missing node for link {0} in {1}".format(linkurl, d['url']), file=sys.stderr)
        d[key] = newvals

for d in new_graph:
    rewrite(d, 'links')
    rewrite(d, 'cats')
    rewrite(d, 'subcats')
    rewrite(d, 'catmembers')

print (json.dumps({
        '@context': c,
        '@graph': new_graph
    }, indent=2))
