from urllib.request import urlopen
from urllib.parse import quote as urlquote
import json
import argparse

ap = argparse.ArgumentParser("")
ap.add_argument('url')
args = ap.parse_args()


url = "http://noembed.com/embed" # ?url=

useurl = url + "?url=" + urlquote(args.url)
print (useurl)
resp = urlopen(useurl).read().decode("utf-8")
resp = json.loads(resp)
print (json.dumps(resp, indent=2))

