#!/usr/bin/env python3

# amateurwebscraper.py
import sys, os
import re
import html5lib
from urllib.request import urlopen, Request
from urllib.parse import urljoin, urlparse
# from xml.etree import ElementTree as ET 
import subprocess
from cssselect import GenericTranslator, SelectorError
# nb using lxml for xpath
from etreeutils import replace_elt, textContent, parentchilditer, innerHTML
from oembedder import get_oembed
import lxml.etree as ET
import requests

# # See CRAZY IMPORT FROM http://lxml.de/tutorial.html

"""
                  __                            __                            
 ___ ___ _  ___ _/ /____ __ ______  _    _____ / /  ___ _______ ____  ___ ____
/ _ `/  ' \/ _ `/ __/ -_) // / __/ | |/|/ / -_) _ \(_-</ __/ _ `/ _ \/ -_) __/
\_,_/_/_/_/\_,_/\__/\__/\_,_/_/    |__,__/\__/_.__/___/\__/\_,_/ .__/\__/_/   
                                                              /_/             
"""

def myurlopen (url, user_agent='Mozilla/5.0'):
    # print ("open", url, type(url), file=sys.stderr)
    req = Request(url, headers={'User-Agent': user_agent})
    return urlopen(req)

def get_content_type (url, user_agent='Mozilla/5.0'):
    #print ("get_content_type {0}".format(url), file=sys.stderr)
    r = requests.head(url, headers={'user-agent': user_agent}, timeout=5.0)
    ret = r.headers['Content-Type']
    # ret = myurlopen(url, user_agent).info().get("content-type")
    print ("get_content_type {0} {1}".format(ret, url), file=sys.stderr)
    return ret

def get_content_type_and_length (url, user_agent='Mozilla/5.0'):
    #print ("get_content_type {0}".format(url), file=sys.stderr)
    r = requests.head(url, headers={'user-agent': user_agent}, timeout=5.0)
    ret = r.headers['Content-Type'], int(r.headers.get('Content-Length', '0'))
    # ret = myurlopen(url, user_agent).info().get("content-type")
    print ("get_content_type_and_length {0} {1}".format(ret, url), file=sys.stderr)
    return ret

# URL normalisation... strip mediawiki diff URLs
def normalize_url (url):
    # http://ustensile.be/index.php?title=Html2print&diff=7836&oldid=7829 => http://ustensile.be/index.php/Html2print
    # http://curlybraces.be/wiki/index.php?title=ERG::Code-2017-1&diff=2094&oldid=2089 => http://curlybraces.be/wiki/index.php/ERG::Code-2017-1
    newurl = re.sub(r"index.php\?title=(.+?)&diff=\d+&oldid=\d+$", r"index.php/\1", url)
    if newurl != url:
        print ("[normalize_url] {0} => {1}".format(url, newurl), file=sys.stderr)
        return newurl
    return url

def css_to_xpath (s):
    return GenericTranslator().css_to_xpath(s)

def extract_content (t, title_selector, content_selector, remove_title):
    """
    Extract content of a document using html5lib + (optional) content and title css selectors
    Returns contents (str)
    """
    # print ("[extract_content]", url, file=sys.stderr)

    title = ""
    # Try title_selector
    if title_selector:
        for item in t.xpath(css_to_xpath(title_selector)):
            title = textContent(item)
            if remove_title:
                item.getparent().remove(item)
    # otherwise the contents of the title tag
    if not title:
        title = t.find(".//title")
        if title != None:
            title = title.text

    content = ""
    found_content = False
    if content_selector:
        content = """<!DOCTYPE html>
<html>
<head>
    <title>{0}</title>
    <meta charset="utf-8">
</head>
<body>
""".format(title)
        for x in t.xpath(css_to_xpath(content_selector)):
            found_content = True
            content += ET.tostring(x, encoding="unicode", method="html")
        content += """
</body>
</html>"""
    # otherwise extract the contents of the body tag
    if not found_content:
        content = innerHTML(t.find(".//body"))
        # content = ET.tostring(t, encoding="unicode", method="html")

    return title, content

def get_iframe_src_from_html (src):
    t = html5lib.parse(src, namespaceHTMLElements=False, treebuilder="lxml")
    iframe = t.find(".//iframe[@src]")
    if iframe != None:
        return iframe.attrib.get("src")

def iframe_to_links (t):
    """ pandoc doesn't render iframes, so let's transform them into links """
    for iframe in t.findall(".//iframe[@src]"):
        src = iframe.attrib.get("src")
        print ("replacing iframe with link for", src, file=sys.stderr)
        link_label = src
        link_href = src
        a = replace_elt(t, iframe, "a")
        a.attrib["class"] = "iframe"
        oembeddata = get_oembed(src)
        if oembeddata:
            print ("[iframe_to_links].OMBED", oembeddata, file=sys.stderr)
            thumb_url = oembeddata.get("thumbnail_url") or oembeddata.get("image")
            if thumb_url:
                print ("inserting image to oembed thumb", thumb_url, file=sys.stderr)
                # INSERT IMAGE
                img = ET.SubElement(a, "img")
                img.attrib['src'] = thumb_url
                link_label = ''
            html = oembeddata.get("html")
            if html:
                iframe_src = get_iframe_src_from_html(html)
                if iframe_src:
                    print ("using oembed iframe src as href", iframe_src, file=sys.stderr)
                    link_href = iframe_src

        a.attrib["href"] = link_href
        if link_label:
            a.text = link_label

def parentchilditer (elt):
    for parent in elt.iter():
        for child in parent:
            yield parent, child

def strip_empty_links (t):
    remove = []
    for p, c in parentchilditer(t):
        if c.tag == "a":
            html = innerHTML(c).strip()
            if html == "":
                remove.append((p, c))
    for p, c in remove:
        print ("[strip_empty_links].removing element {0}".format(ET.tostring(c)), file=sys.stderr)
        p.remove(c)


def replace_oembed_links (t):
    """ Function to replace links to say mixcloud with thumbnailed links to iframe """
    added_images = set()
    for oa in t.findall(".//a[@href]"):
        href = oa.attrib.get("href")
        oembeddata = get_oembed(href)
        if oembeddata:
            print ("[replace_oembed_links].OMBED", oembeddata, file=sys.stderr)
            html = oembeddata.get("html")
            if html:
                ohref = get_iframe_src_from_html(html)
                if not ohref:
                    print ("[no iframe found in oembed html]", file=sys.stderr)
                if ohref:
                    print ("[replace_oembed_links] replacing {0} with {1}".format(href, ohref), file=sys.stderr)
                    a = replace_elt(t, oa, "a")
                    a.text = oa.text
                    a.attrib["href"] = ohref
                    a.attrib["class"] = "iframe"

                    thumb_url = oembeddata.get("thumbnail_url") or oembeddata.get("image")
                    if thumb_url and not thumb_url in added_images: # only add the same thumb_url once
                        added_images.add(thumb_url)
                        print ("[replace_oembed_links] inserting image {0}".format(thumb_url), file=sys.stderr)
                        img = ET.SubElement(a, "img")
                        img.attrib['src'] = thumb_url
                    else:
                        a.text = href

def pandoc (src, fro="html", to="mediawiki"):
    # print ("[pandoc]", file=sys.stderr)
    p = subprocess.Popen(["pandoc", "--from", fro, "--to", to], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate(src.encode("utf-8"))
    return stdout.decode("utf-8")

def absolutize_links (t, url):
    for elt in t.findall(".//*[@href]"):
        href = elt.attrib.get("href")
        elt.attrib['href'] = urljoin(url, href)
    for elt in t.findall(".//*[@src]"):
        href = elt.attrib.get("src")
        elt.attrib['src'] = urljoin(url, href)

# def replace_linked_images (t):
#     """ <a href="original"><img src="thumb"></a> ==> <img src="original" /> """
#     replacements = []
#     for p, elt in parentchilditer(t):
#         if p.tag == "a" and p.attrib.get("href") and elt.tag == "img":
#             replacements.append((p, elt))
#     for a, img in replacements:
#         print ("replace_linked_image", a.attrib['href'], file=sys.stderr)
#         newimg = replace_elt(t, a, "img")
#         newimg.attrib['src'] = a.attrib['href']

IMAGE_CONTENT_TYPES = set((
    "image/png",
    "image/gif",
    "image/jpeg",
    "image/jpg",
    "image/svg",
    "image/svg+xml",
    "application/svg+xml",
    "application/svg",
    "application/pdf"))

from urllib.error import URLError
# from http.client import RemoteDisconnected

MAX_CONTENT_LENGTH = 10 * 1000 * 1000

def looks_like_media_link (url):
    parts = urlparse(url)
    _, ext = os.path.splitext(parts.path)
    ext = ext.lstrip(".").lower()
    return ext in ("jpg", "jpeg", "pdf", "gif", "png", "svg")

def embed_linked_media (t):
    """ Transform linked images and PDFs into images pointing to resources directly, removing eventual contained thumbnail images """
    for elt in t.findall(".//a[@href]"):
        href = elt.attrib.get("href")
        # print ("[embed_linked_media] checking href", href, file=sys.stderr)
        if looks_like_media_link(href):
            try:
                ct, clen = get_content_type_and_length(href)
                if ct in IMAGE_CONTENT_TYPES and clen < MAX_CONTENT_LENGTH:
                    print ("embed_linked_media, replacing link with img:", href)
                    img = replace_elt(t, elt, "img")
                    img.attrib['src'] = href
            except Exception:
                pass
        # except URLError:
        #     pass
        # except UnicodeEncodeError:
        #     pass

def mediawiki_fix_external_image_links_syntax (mwsrc):
    """
    Fixes a problem in pandoc's transformed media wiki output where File objects are wrapped in an external link.
    Transforms this to Mediawiki's prefered [[File: with |link=]]
    [http://some.url eventual label [[File:Some File|options]] ]
    ==>
    [[File:Some File|options|link=http://some.url|eventual label]]

    [https://www.mixcloud.com/widget/iframe/?feed=https%3A%2F%2Fwww.mixcloud.com%2FErgote_Radio%2Fworkshop-the-reader%2F&hide_cover=1 Workshop The Reader[[File:https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/a/2/6/a/7283-925e-4d22-a5f4-e3add659bf44]]]<br />
    ==>
    [[File:https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/a/2/6/a/7283-925e-4d22-a5f4-e3add659bf44|link=https://www.mixcloud.com/widget/iframe/?feed=https%3A%2F%2Fwww.mixcloud.com%2FErgote_Radio%2Fworkshop-the-reader%2F&hide_cover=1|Workshop The Reader]]<br />

    [https://www.mixcloud.com/widget/iframe/?feed=https%3A%2F%2Fwww.mixcloud.com%2FErgote_Radio%2Fworkshop-the-reader%2F&hide_cover=1 [[File:https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/a/2/6/a/7283-925e-4d22-a5f4-e3add659bf44]]]<br />
    ==>
    [[File:https://thumbnailer.mixcloud.com/unsafe/600x600/extaudio/a/2/6/a/7283-925e-4d22-a5f4-e3add659bf44|link=https://www.mixcloud.com/widget/iframe/?feed=https%3A%2F%2Fwww.mixcloud.com%2FErgote_Radio%2Fworkshop-the-reader%2F&hide_cover=1]]<br />

    """
    def r(m):
        link = m.group(1)
        label = m.group(2)
        label = label.strip()
        file = m.group(3)
        if label:
            return "[[{0}|link={1}|{2}]]".format(file, link, label)
        else:
            return "[[{0}|link={1}]]".format(file, link)
        # return r"[[\2|link=\1]]"
    return re.sub(r"\[(https?:\/\/\S+) (.*)\[\[((?:File|Fichier|Image)\:.+?)\]\]\]", r, mwsrc)


# Unify the scrape pipeline

# title, content, and remove selectors
# Create pseudo document for fragments (with title from feed?)
# <body> ... </body>



def scrape_url_to_mediawiki (url, title_selector=None, content_selector=None, remove_title=False, remove_selector=None):
    # url = normalize_url(url)
    print ("scrape_url_to_mediawiki {0}, content_selector: {1}".format(url, content_selector), file=sys.stderr)
    print ("[urlopen]", file=sys.stderr)
    f = urlopen(url)
    print ("[urlopen] opened, parsing with html5lib")
    t = html5lib.parse(f, namespaceHTMLElements=False, treebuilder="lxml")
    # print (ET.tostring(t, encoding="unicode", method="html"))
    if remove_selector:
        for elt in t.xpath(css_to_xpath(remove_selector)):
            # print ("Removing", elt, file=sys.stderr)
            elt.getparent().remove(elt)
    print ("absolutize_links", file=sys.stderr)
    absolutize_links(t, url)
    print ("strip_empty_links", file=sys.stderr)
    strip_empty_links(t)
    print ("iframe_to_links", file=sys.stderr)
    iframe_to_links(t)
    print ("embed_linked_media", file=sys.stderr)
    embed_linked_media(t)
    print ("replace_oembed_links", file=sys.stderr)
    replace_oembed_links(t)
    print ("extract_content", file=sys.stderr)    
    title, src = extract_content(t, title_selector, content_selector, remove_title)
    print ("pandoc", file=sys.stderr)
    src = pandoc(src, to="mediawiki").strip()
    print ("mediawiki_fix_external_image_links_syntax", file=sys.stderr)
    src = mediawiki_fix_external_image_links_syntax(src)
    # if title:
    #     src = "= {0} =\n\n".format(title) + src
    # src += "\n\n[[Category:Scrape links]]"
    return title, src


def scrape_fragment_to_mediawiki (src, url):
    # url = normalize_url(url)
    t = html5lib.parseFragment("<div>"+src+"</div>", namespaceHTMLElements=False, treebuilder="lxml")
    t = t[0] # parseFragment with lxml gives a list of Elements
    absolutize_links(t, url)
    strip_empty_links(t)
    iframe_to_links(t)
    embed_linked_media(t)
    replace_oembed_links(t)

    src = innerHTML(t)

    src = pandoc(src, to="mediawiki").strip()
    # print ("[scrape_fragment_to_mediawiki].src1", src, file=sys.stderr)
    src = mediawiki_fix_external_image_links_syntax(src)
    # print ("[scrape_fragment_to_mediawiki].src2", src, file=sys.stderr)
    return src

def scrape (url, title_selector, content_selector, remove_title, remove_selector, wikiprotocol, wikihost, wikipath, wikipage, user, password):
    """ tester """
    import mwclient
    wiki = mwclient.Site((wikiprotocol, args.wikihost), path=args.wikipath)
    wiki.login(user, password)
    title, newsrc = scrape_url_to_mediawiki(url, title_selector, content_selector, remove_title, remove_selector)
    page = wiki.pages[wikipage]
    page.save(newsrc)

if __name__== "__main__":
    import argparse
    ap = argparse.ArgumentParser("")
    sub = ap.add_subparsers(help='sub-command')

    ap_norm = sub.add_parser('normalize', help="normalize a URL")
    ap_norm.add_argument('url')
    ap_norm.set_defaults(func=normalize_url)

    aps2 = sub.add_parser('scrape', help="scrape a URL (2)")
    aps2.add_argument('url')
    aps2.add_argument('--title-selector', default=None, help="CSS selector to extract title content")
    aps2.add_argument('--remove-title', default=False, action="store_true", help="remove title (if specified with a selector)")
    aps2.add_argument('--content-selector', default=None, help="CSS selector to extract main content")
    aps2.add_argument('--remove-selector', default=None, help="CSS selector of content to remove")

    aps2.add_argument("--wikiprotocol", default="http")
    aps2.add_argument("--wikihost", default="localhost")
    aps2.add_argument("--wikipath", default="/mw/")
    aps2.add_argument("--wikipage", default="Test")
    aps2.add_argument("--user", default="Michael Murtaugh@bot")
    aps2.add_argument("--password", default="3ap1p11k33snual7hr2m9nldvg1jo0si")
    # aps2.add_argument("--page", default=None)

    aps2.set_defaults(func=scrape)


    args = ap.parse_args()
    # print (args)
    params = vars(args).copy()
    del params['func']

    res = args.func(**params)
    print (res)
