import argparse, json, sys, os
import unicodedata
from urllib.parse import quote as urlquote, unquote as urlunquote
from xml.etree import ElementTree as ET 


def strip_accents(s):
   return ''.join(c for c in unicodedata.normalize('NFD', s)
                  if unicodedata.category(c) != 'Mn')

ap = argparse.ArgumentParser("Export JSON items to separate files (core + agenda + satellite)")
ap.add_argument('--input', type=argparse.FileType('r'), default=sys.stdin)
ap.add_argument('--main', default="m/data/main.json")
ap.add_argument('--cats', default="m/data/cats.json")
ap.add_argument('--news', default="m/data/news.json")
ap.add_argument('--satellite', default="m/data/satellite.json")
args = ap.parse_args()

data = json.load(args.input)
c = data['@context']
g = data['@graph']

main_g = []
cats_g = []
news_g = []
satellite_g = []

for x in g:
    url = x['url']
    urlp = urlunquote(url)
    if url.startswith("/m/wiki/Page_web/"):
        satellite_g.append(x)
    elif url.startswith("/m/wiki/Actualit%C3%A9s/"):
        news_g.append(x)
    elif url.startswith("/m/wiki/Cat%C3%A9gorie/"):
        cats_g.append(x)
    else:
        main_g.append(x)

with open (args.main, "w") as f:
    json.dump({'@context': c, '@graph': main_g}, f, indent=2)
with open (args.cats, "w") as f:
    json.dump({'@context': c, '@graph': cats_g}, f, indent=2)
with open (args.news, "w") as f:
    json.dump({'@context': c, '@graph': news_g}, f, indent=2)
with open (args.satellite, "w") as f:
    json.dump({'@context': c, '@graph': satellite_g}, f, indent=2)
# print (json.dumps(data, indent=2), file=args.output)
