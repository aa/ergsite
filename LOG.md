11 Sep 2017
================

* Complete & construct the full "site map" (as json?!) ... load / display separately (simplified display?)
* Connect feed links to courses
* Include feeds in pages2json

Doing this reminds of the needs for better hypertext tools for: integrating search, proactively helping link to id'd sections of a document, and recording the link in relative form. At the same time, also the need for simplicity of using a name as a "tag" by simply marking it up (for instance as **markdown strong**). In both cases, it comes back to the importance of the **index** in relation to writing and linking.

ebook as writing space.... index as (functional) document(s).

Hypertext was a step back in terms of an understanding of language as inherently a mechanism of linking. The explicit links of the URL adds a punitively explicit system of reference that's at once both too heavy and too fragile.

What is "linkable" ... expand the definition to include things like <em> terms ...


17 Sep 2017
======================
* Code to "auto-link" wiki page titles in text, testing with feed renderings
* Transferred "pages" content to mode d'emploi mediawiki
* Next step: static dump of wiki to simple static pages
* Next step: static dump of feed pages ... add auto linking with wiki titles
* Connect with "simple" wiki map viz


12 Oct 2017
========================
* URLS ... Add a "sub-URL" for pages viewed via the frame 

USE THE HISTORY API!!!!

    erg.be/m/#blogs.erg.be/agenda/foo_bar.html ...

### Double-headed links!!!

<!> Links (from the agenda for instance) should trigger in PARALLEL a contextual message in the "over" panel -- so you open a 

So from the agenda, a link to a blog post such as:

    http://culturesnumeriques.erg.be/spip.php?article144

would simultaneously address the OVER panel with a second URL:

    http://erg.be/m/wiki/Cultures_Numeriques_(flux).html


22 Nov 2017
=============

After some time off... (and a trip to Chicago)

Focus on the idea of a feed reader. Currently, really liking the agenda views, need to return to these and make super functional. Reading in the train, again reminded of the value of producing an offline view -- a la tt-rss (which is what I'm reading now of my own feeds). So:

* Produce a tt-rss style view with
* A joined feed
* Per-source views.
* "All articles", "Fresh articles", "Starred articles" ... consider personalizing feeds per viewer ??

For ERG:
* Upcoming events

Use the labels from tt-rss (ability to categorize the sources)

Prefix single-source listing with information about a particular feed.
Add "About source" link to mixed source listing? (or inline them before particular feeds).

As much as I love the dynamic SVG stuff, I need to find a way to make this more SPECIFIC / lightweight as ONE OF MANY interfaces.

Reading a hastily loaded mediawiki page (https://meta.wikimedia.org/wiki/Research_on_e-books_with_Wikimedia_content/Interview_with_Anja_Groten_from_Hackers_and_Designers)... thinking: What about using the feeds as bookmarks: use "etherdump" (mediawiki API) to make copies of mediawiki articles ?! In other words, as part of the feed reading process... Improve the feed by using the mediawiki api and grabbing / copying the full text of a given page.

later
========================


wifi:
192.168.254.104

etherbox:
192.168.254.5


wireless (ERG_PROF)
192.168.3.189


plugged into computer lab, assigned 192.168.1.5 (kind of surprising!)


etherbox: 192.168.1.6
ergwiki: 192.168.1.7


5 mar 2018
=============

MY TODO's
* mediawikifeeder.py make faster... (maybe split registration and pulling)
* 
* debug the network (etherbox)
* tunnel for the etherpad
* how to have a stable IP in the local network
* How to access the pad local
* Prepare the workshop for this week...
* Page Web HTML ...
Flatversion with no styles


* Journal...
* Date
* HTML (with h1, h2)... RSS feed piece to have an extract.
> Ideally you would have just the blog post.
etherbox => wiki ??

How to make things SSTYLE able with CSS...

* Backlinks in HTML dump
* MENU integrated in HTML
* Ability to  zoom the HTML panel.
* PAGE VARIATIONS...
* Framing pages...
* Switch to a more flexible layout a la map... shrinking the box...
* Add ability to upload custom CSS for a "style garden" style menu <!>

TODAY
* Adjust "by hand" a particular dump.
* Dump Page web pages separately (own subdirectory?!)
* Test pandoc to convert pages to mediawiki
* Test links to convert pages to mediawiki (text only)
* Support for Manually created Page Web (auto scrape)

TOMORROW
* Make scraping FASTER (don't contact EVERY link!)
PageWeb.... using Site?!
* get the public web page to display the local network ip address!!!!



http://culturesnumeriques.erg.be/spip.php?article215
div#main-content

Ability to add a "content"/scraper selector (css) to the Site Web ... and use this in page scraping.
(python translation of css to xpath?!)

Imagine an uploaded PDF/Zine with links to the sources pages appearing linked via backlinks.

Part I: Focus on scraping

x extract based on selector
x pandoc HTML to mediawiki
* <!> mark as in-need-of-link-fixes (Category)
* remove title
* exclude selector
* fix links (download + replace)
* test per page with selectors provided by hand (test in local wiki?)




Part II: Dumping + Interface
Part III: Styling the dump (CSS ZEN GARDEN)




http://www.multimedialab.be/blog/?p=2450
content: div.post
title: h2

# MEDIWIKI FIXING

## Images are File forms with external URL

example:

    [[File:http://www.multimedialab.be/blog/wp-content/uploads/2017/10/cover_index.jpg|Post-production]]

    [[File:http://www.multimedialab.be/blog/wp-content/uploads/2012/09/pixel_gris.gif|482x1px|Grey line]]

treatment:
    Download URL to wiki, replace link with resulting file (already have code in RSS processor)


## Links

example:
    [http://www.multimedialab.be/doc/erg/2017-2018/nicolas_bourriaud_postproduction_extrait.zip Télécharger le PDF zippé (2 Mo).]
    [http://videoarti.pbworks.com/w/file/fetch/94598963/digital_folklore1.pdf ici la préface (.pdf, EN).]

Download PDFs and change to Local File Refs.


VISIT the automatic category pages with broken file links

FIX: http://localhost/mw/index.php?title=Category:Pages_with_broken_file_links&action=edit&redlink=1







friday 15u: 

6 Mar 2018
============



30 Mar 2018
===============
Not sure where my portes ouvertes notes have gone but...

x mediawikidump.py : Need to bring back the distinction of categorized + link to a category as they are BOTH interesting and different.
x menu
x category indexing
x render links from PRE template src
X category drawing (both as page and perimeter?!)
x Active node

x special rule for category links (catmembers + links merge)

x mouseover highlighting (in graph)
x mouseover highlighting (in doclinks?)
* camera controls (manual) 
x camera controls (automatic) ... https://bl.ocks.org/mbostock/b783fbb2e673561d214e09c7fb5cedee
* check sim cooling rate (can it settle faster?!)
* add classes to shapes... try styling via style sheet
* create an "alternative" stylesheet/svg and a selector ( do this via different index html to allow also different SVG templates!!!! )
* icons in graph

* update on pi

* public server sync!
* search


15 juin deadline
======================

Last meeting with Alexia, Stephanie, Sammy, Laurence + Me

Phase II: Galerie + (Indexhibit)

Accueil: Welcome to school, not to site.
Modedemploi... What do the symbols mean? A legend!!!

* Historique... how are
Top of page ... show SYMBOL + CATEGORY names (avoid using the word category!)

* VIEW HISTORY ?!

Pages dans la categorie...

Backlinks....
could they be organized...

* Show the categories in the Index
* Move links in the Actualities archive (2013 ... 2015) on TOP
* Page dans la categorie ... on TOP of page ??
* Draw the categorie NAME on the BORDER !!!! (the question of what is the green link...)
* ? two categories at the same time .... (add back a *** dynamic LEGEND) ... and make the categories clickable.
* ICONS for EACH CATEGORY
* Compact form of left right as INSETs? (and make flippable)
* Start with just the map ?
* How to make a zoom out ... ?? cache an image of all the elements

28 May 2018
===============

### Map

* POC: Draw map based on data from pages
* Show History
* Wrap titles
* Legend
* Start with "just the map"

### Wiki

* Require login to edit



31 May 2018
===================

things I absolutely must get to today

* merge old map with new...
x create items in core + external groups
* load order: (i core + ii events/satellite)
x center nodes, zoom out...
* dynamic nodes + linking
* allinks (Main namespace nodes only)
* link styles
* history line --> symbol ... active as marker on history line ?!?!?!


<!><!> BUG: & + "" in titles break add_dates...
Portes_Ouvertes_%26_ZRT._2.0Photos_par_Marc_Wathieu.html


* legend ... part of skin page!?
* timeline
* revisit categories
* consider drawing order based on screen position!
* push into layers (allowing multiple ?!)
* text on edge
* "links" (core only ... "pseudo" category?)
* history
* URLs
* clarify search / index page
* cgi remake
* symbol rotation!
* leaflet zoom buttons (working)
x css classes for category links (for legend symbols?!)
* menu as select ?!

* ?separate tabs ipv splitscreen on smaller devices
* ?External links in external tab ?! (simplification!)
* Add last modified line (with subtle wiki link) in wikipage footer
* Add a menu link in wikipage (and make css hidable/overridable!)


* Two columns in wikipage template