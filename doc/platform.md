The Platform
======================

### Layout of the interface

The interface is organized as a "browser" page that contains two iframes, the "map" and the "over". The over is designed to be retractable, the user can click to compact the panel.

![](frames.png)


### How to open a link in the overlay?

Set the *target* attribute of the link to "over"

    <a href="http://copyright.rip/somepage.html" target="over">special information</a>

Links with target="over" are opened in the *over frame*.


### How to open a wiki page in the overlay?

Make a link with the URL of the wiki page and set the *target* attribute to "over"

    <a href="http://modedemploi.erg.be/wiki/index.php/Bachelors" target="over">Bachelors</a>

Links with target="over" are opened in the *over frame*. In addition, links to wiki pages are automatically replaced with the local "mini" page equivilent.


### The index

The index.json file contains a snapshot of meta data about:
* the wiki pages, including time of last update and the URL of the local "mini" page that gets displayed in the over frame, and links between pages
* aggregated RSS feeds including URLs of local copies of the feed content
* information about linked media (wiki files and RSS attachments and linked media) including the URLs of local (archive) copies and generated thumbnails.


Wiki categories are represented in the index with type "Collection".

The wiki pages also contain information about the RSS feeds themselves.

This file contains all the ingredients used in preparing the various interfaces. Interfaces can be built *statically* via (HTML) templates (as in the case of the agenda pages), or *dynamically* loaded each time a user views the interface (as in the case of the sitemap).

Aggregated items have one or (typically) more types. NB each item represents an event which is either the creation of a new item, or the updating of an already existing resource.

Activity types:

* created
* modified

Resource types:

* webpage
* collection
* blog
* mediawiki
* git
* video
* sound
* image
* font

Following RSS, items can have one or more attachments. Typically these are media files (image, audio, and video) but can also be other kinds of files (e.g. SVG fonts in the case of the FDDL typography feed).



