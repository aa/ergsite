
Requirements
===============

Extra python modules (use pip to install):

    PyLD: data joining
    jinja2: templating
    html5lib: html parsing
    icalendar: ical parsing
    iso8601: date parsing
    pillow (PIL): thumbnail creation

    # svg.path: SVG "simplification" in scripts/svgsimplify.py

    python-oembed (pip)

node_modules in m should have:
    d3

Javascript build
==============================

Install latest node with Node version manager...

curl -L https://git.io/n-install | bash

(nb: you don't need to be root, but you need to relogin as envvars gets set to point to new node/npm)

### INstall deps
cd /var/www/html/m
npm install

### make
cd /var/www/html
make m/map.js
